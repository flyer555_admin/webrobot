// 监听来自content-script的消息
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse)
{
	console.log('收到来自content-script的消息：');
	console.log(request, sender, sendResponse);
	console.log(oui.cmd[request.cmd]);
	oui.cmd[request.cmd]&&oui.cmd[request.cmd](request.param,sender,request,sendResponse,request.cmd);

	//对于异步的场景， 这里必须返回 true
	return true;
});
//消息传递
oui.cmd = {
	//后台执行ajax请求,一般用于模板加载，路由转向
	loadUrl:function(param,sender,request,sendResponse,cmd){
		if(!param.callback){
			param.callback = function(text){
				sendResponse(text);
			}
		}
		if(param.postParam){
			oui.postData(param.url,param.postParam,function(res){
				sendResponse(res);
			},function(res){
				sendResponse(res)
			});
			return;
		}
		oui.loadUrl(param);
	}
};


// Check whether new version is installed
chrome.runtime.onInstalled.addListener(function(details){
	//监听插件安装
	//if(details.reason == "install" || details.reason == "update" ){
	//	chrome.tabs.create( {url: "option.html"} );
	//}
});

