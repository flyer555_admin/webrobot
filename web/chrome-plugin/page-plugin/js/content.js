$(document).ready(function(){
    var url = location.href;
    var path = location.pathname;
    console.log('当前页面路径:'+path);

    //公共方法在这里 开始
    var me = this;
    var oui = {

    };
    //加载跨域ajax资源
    oui.loadUrl = function(url,subContentType,progressBar,startTag,endTag,callback,postParam){
        if(typeof url=='object'){
            var temp = url;
            subContentType= temp.subContentType,progressBar=temp.progressBar,startTag=temp.startTag,endTag=temp.endTag,callback=temp.callback;
            postParam = temp.postParam;

            var tempUrl=temp.url;
            url=tempUrl;
        }
        (typeof chrome.app.isInstalled !='undefined') && chrome.runtime.sendMessage(
            {
                cmd:'loadUrl',
                param:{
                    url:url,
                    postParam:postParam,
                    subContentType:subContentType,
                    progressBar:progressBar,
                    startTag:startTag,
                    endTag:endTag,
                    callback:callback
                }
            },
            function(response) {
                callback&&callback(response);
            }
        );
    };
    // 公共方法在这里已经结束了

    if(window.top ==window){

        //绑定确定 点击事件
        $(document).on('click','.page-plugin-confirm-button',function(e){
            //这里绑定确定页面 中 确定点击事件
            console.log('绑定 确定按钮事件:');
            var data = {
                name:$('#data-name').val(),
                age:$('#data-age').val(),
                phone:$('#data-phone').val(),
                test:$('#data-test1').val(),
                test2:$('#data-test2').val()
            };

            alert('这里可以提交数据：'+(JSON.stringify(data)));
            //oui.loadUrl({ // ajax 提交
            //    url:'', // TODO 这里指定 服务url
            //    postData:{ //post提交
            //        data:data
            //    },
            //    callback:function(res){ //数据响应
            //        //将页面模板显示到页面中
            //        console.log(res);
            //        $(document.body).append(res);
            //
            //    }
            //});
        });

        //绑定取消 点击事件
            $(document).on('click','.page-plugin-cancel-button',function(e){
            //这里绑定确定页面 中 确定点击事件
            console.log('绑定取消按钮事件:');
            $('#plugin-container').removeClass('active');
        });


        $(document).on('click','.page-plugin-toggle-button',function(e){
            //这里绑定确定页面 中 确定点击事件
            console.log('绑定切换按钮事件:');
            if($('#plugin-container').hasClass('active')){
                $('#plugin-container').removeClass('active');
            }else{
                $('#plugin-container').addClass('active');
            }


        });

        //绑定取消 点击事件

        //顶层页面 对自定义页面元素注入事件绑定


        // 获取当前病人数据， 传输到 右侧内嵌iframe中
        me.cmd4getData = function(){
            var $iframe = $('#plugin-iframe');
            $iframe[0].contentWindow.postMessage({
                cmd:'cmd4loadIframeData',
                param:oui.globalData||{}
            },"*");
        };
        /**显示页面 */
        me.cmd4showPanel = function(data){

            // 拉取 html模板 并显示 注入到dom 中
            var htmlUrl = chrome.extension.getURL("plugin-index.html"); // 拉取页面模板并显示

            //这里在顶层页面显示浮动层
            oui.loadUrl({
                url:htmlUrl, //这里指定 服务url
                callback:function(res){ //数据响应
                    //将页面模板显示到页面中
                    console.log(res);
                    oui.globalData = data;
                    var dom = document.getElementById('plugin-container');
                    if(dom !=null){
                        $(dom).remove();
                    }
                    $(document.body).append(res);

                    var height = $("#plugin-container").height();
                    var $iframe = $('#plugin-iframe');
                    $iframe[0].height=height-2;

                    $('#data-name').val(data.name);
                    $('#data-age').val(data.age);
                    $('#data-phone').val(data.phone)

                }
            });

        };
        //顶层页面绑定事件
        window.addEventListener("message", function(event) {
            var data = event.data;
            if(typeof data =='string'){
                return;
            }
            if(typeof data =='object'){
                //在本类中的方法负责接收消息处理
                if(data.cmd&&data.param){
                    console.log('page-plugin:'+data.cmd);
                    console.log(data);
                    me[data.cmd]&&me[data.cmd](data.param,event);
                }
            }
        }, false);
    }

    //这里是页面注入脚本，判断如果当前页面路径与目标路径匹配 则执行脚本注入绑定事件
    if(path.indexOf('eClinic/WaitingRoom/NurseLeft')>-1){// 这是左侧搜索列表绑定事件
        //病人信息搜索列表,绑定点击事件
        $(document).on('click','#tbPatientList tr',function(e){

            //TODO 这里可以扩展 病人信息的点击事件
            console.log('在这里绑定页面点击事件:');

        })
    }
    //这是中间内容页的绑定事件

    //这里是中间页面内容绑定事件
    if(path.indexOf('eClinic/Patient/PatientDetail')>-1){//

        //病人信息搜索列表,绑定点击事件
        //往顶层页面发送消息，显示弹框信息
        var $parent = $('#divOPInfo');
        var $infos = $parent.find('.patientinfo');
        var info = {
            name:'', //姓名
            age:'', //年龄
            sex:'',  //性别
            phone:'' //电话号码
        };
        try{
            //TODO 这里可以扩展 病人信息

            info.name = $.trim($parent.find('.patientname').text()||"");
            info.age = $.trim($($infos[0]).text()||"");
            info.sex = $.trim($($infos[1]).text()||"");
            var phone = $($infos[2]).text()||"";
            phone = phone.replace('Tel.','');
            phone = $.trim(phone);
            info.phone =phone ;//电话
        }catch(err){
        }
        //病人电话

        window.top.postMessage({
            param: info, //传递相关信息到父页面,发送显示浮动页面的命令
            cmd:'cmd4showPanel' //通过命令触发页面显示浮动层
        },"*");

    }

});