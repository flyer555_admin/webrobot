$(function() {


  var div = '<div style="position: fixed;right: 60px;top:200px;line-height:23px;font-size:21px;color:white;height: 142px;width: 40px;text-align: center; background: #ff464e;padding:3px 2px; display:inline-block; cursor:pointer;border-radius:6px" id="shouto-gift" >从代发兔下单</div>';
  $('#step2>h3').append(div);



  let listDom = $('.consign-detail.batch');
  let list = [];
  listDom.each(function() {
    let address = $.trim($(this).find('.receive-info .logis\\:receInfo').text());
    let order_id = $(this).find('tr.order-title').attr('order_id');
    address = format(address);
    list.push({
      order_id,
      address
    })
  })
  console.log(list);


  $('#shouto-gift').click(function() {
    //发送消息给后台js

    chrome.extension.sendRequest({
      type: "toGift",
      orderType:'taobao',
      list
    }, function(response) {

      console.log(response);
    });
  })
  // test();
})


chrome.extension.onRequest.addListener(
  function(request, sender, sendResponse) {
    if (request.type == "writeDeliveryCode") {//回写运单号

      $('input[name=logisType][value=2]').attr('checked', true);

      $("select[name=sameLogisCompanyId]").find("option:contains('" + request.data.delivery_name + "')").attr("selected",true);


      let list = request.data.dataList;
      console.log(request.data);
      list.forEach((item, index) => {
        let tr = $("tr[order_id=" + item.orderId + "]");
        let tx = tr.parent().parent().find('.shipping-number input[type=text]');
        tx.focus();
        tx.val(item.deliverCode);
      });
      //回写完成后，自动提交批量发货；
      document.querySelector("#btnConsign").click();
      // 自动提交 代码 暂时注释
      // document.querySelector("#main-content > form").submit();

       sendResponse();
    }
  }
)

function format(str) {
  str = str.replace(/，\d{6}，/, '，');
  arr = str.split('，');
  let phone = arr[arr.length - 1];
  let name = arr[arr.length - 2];
  let address = '';
  for (let i = 0; i <= arr.length - 3; i++) {
    address += arr[i];
  }
  //str = address + '，' + name + '，' + phone;
  str = $.trim(name)+','+ $.trim(phone)+','+ $.trim(address);
  //900000000000,李四,13911111111,广东省 东莞市 其他区 江南大道99号

  return str;
}


//自动填运单号测试
function test() {

  let list = [{
    delivery_code: '1100074824932',
    order_id: '658202659343515418'
  }]

  $('input[name=logisType][value=2]').attr('checked', true);

  list.forEach((item, index) => {
    let tr = $("tr[order_id=" + item.order_id + "]");
    let tx = tr.parent().parent().find('.shipping-number input[type=text]');
    tx.focus();
    tx.val(item.delivery_code);
    console.log(tx);
    console.log(tr);
  })



}
