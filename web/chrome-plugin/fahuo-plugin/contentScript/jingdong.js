/**
 * 处理京东的单个出库
 */

setTimeout(function() {
  $(function() {
    let inp;
    let selectLi;

    let span = $('span.ivu-select-selected-value').trigger('click');
    let li = $('li.ivu-select-item:contains("圆通快递")').trigger('click');


    let intval = setInterval(function() {
      let url = window.location.href;
      if (url.indexOf('orderlist/allOrders') != -1 || url.indexOf('orderlist/waitOut') != -1) {
        clearInterval(intval);
        window.location.reload();
      }
    }, 1000);


    var div = '<div style="position: fixed;right: 60px;top:200px;line-height:22px;font-size:19px;color:white;height: 142px;width: 40px;text-align: center; background: #ff464e;padding:3px 2px; display:inline-block; cursor:pointer;border-radius:6px" id="shouto-gift" >从代发兔下单</div>';

    //var div =
    //  '<div style="line-height:20px;color:white;background:green;padding:3px 6px; display:inline-block; cursor:pointer;border-radius:6px" id="shouto-gift">从礼品站下单</div>'
    //var div2 =
    //  '<div style="margin-left:20px;line-height:20px;color:white;background:green;padding:3px 6px; display:inline-block; cursor:pointer;border-radius:6px" id="jingdong-gift">京东回填</div>'
    $('div.out>h3:first').append(div);
    //$('div.out>h3:first').append(div2);
    let list = [];

    let order_id = $('span.order-number a').text();
    let address;

    //京东的要去后台js跨域获取收货信息和手机号
    chrome.extension.sendRequest({
      type: "jingdongGetSource",
      value: {
        order_id,
        cookie: document.cookie
      }
    }, function(response) {
      address = response.address;
      address = format(address);
      console.log(address);
      list.push({
        order_id,
        address
      })
    });



    console.log(list);

    $('#shouto-gift').click(function() {
      //发送消息给后台js
      chrome.extension.sendRequest({
        type: "toGift",
        orderType:'jd',
        list
      }, function(response) {

        console.log(response);
      });
    })

    $('#jingdong-gift').click(function() {
      //发送消息给后台js
      chrome.extension.sendRequest({
        type: "jingdongCallback",
        list
      }, function(response) {
        console.log(response);
        if (response.status == 200) {
          let delivery_name;
          if (delivery_name == '圆通速递' || !delivery_name) {
            delivery_name = '圆通快递'
          }
          let selectLi = $("li.ivu-select-item:contains('" + delivery_name + "')");
          selectLi.trigger('click');
          response.order_list.forEach((item, index) => {
            if ($('span.order-number a').text() == item.order_id) {
              let txt;
              if (!item.delivery_code) {
                txt = '运单号未出，等待商家上传';
              } else {
                txt = item.delivery_code;
              }

              let inp = selectLi.parents('div.ivu-col-span-8').next().find(
                'div.out-delivery-input input');
              inp = inp.get(0);
              var evt = new InputEvent('input', {
                inputType: 'insertText',
                data: txt,
                dataTransfer: null,
                isComposing: false
              });
              inp.value = txt;
              inp.dispatchEvent(evt);

            }
          })
        }

        if (response.status == 400) {
          alert('请先登陆礼品站');
        }
      });
    })
  })
  // showTips();

}, 1000)


chrome.extension.onRequest.addListener(
  function(request, sender, sendResponse) {
    if (request.type == "writeDeliveryCode") { //京东暂时只支持单个发货
      if (request.data.list.length == 0) {
        showTips();
        sendResponse();
        return;
      }
      let delivery_name = request.data.delivery_name;
      if (delivery_name == '圆通速递' || !delivery_name) {
        delivery_name = '圆通快递'
      }
      let list = request.data.list;
      let selectLi = $("li.ivu-select-item:contains('" + delivery_name + "')");
      selectLi.trigger('click');

      console.log(selectLi);
      console.log('页面接收到的是：');
      console.log(request);





      list.forEach((item, index) => {
        if ($('span.order-number a').text() == item.order_id) {
          let txt;
          if (!item.delivery_code) {
            txt = '18:00之前订单当天19:00前出单号';
          } else {
            txt = item.delivery_code;
          }

          let inp = selectLi.parents('div.ivu-col-span-8').next().find('div.out-delivery-input input');
          inp = inp.get(0);
          var evt = new InputEvent('input', {
            inputType: 'insertText',
            data: txt,
            dataTransfer: null,
            isComposing: false
          });
          inp.value = txt;
          inp.dispatchEvent(evt);

        }
      })
      sendResponse();
    }
  }
)

function format(str) {
  str = str.replace('，', '').replace(',', '');

  let arr = str.split(' ');

  let address = arr[0];
  let name = arr[1];
  let phone = arr[2];

  /**
   * 京东的省份不带“省”  除直辖市以外主动填
   */

  let filter = address.trim();
  let key, key1, key2;

  key1 = filter[0];
  key2 = filter[1];
  key = key1 + key2;
  let MCG = '北京,上海,重庆,天津'; //Municipality directly under the Central Government 四个直辖市 不带“省”
  let threeProvince = '黑龙,内蒙';


  if (MCG.indexOf(key) != -1) {

  } else if (threeProvince.indexOf(key) != -1) {
    address = address.slice(0, 3) + '省' + address.slice(3);
  } else {
    address = address.slice(0, 2) + '省' + address.slice(2);
  }


  let newArr = [];
  newArr[0] =name;
  newArr[1] =phone;
  newArr[2] = address;

  str = newArr.join(',');
  return str;
}

function simulateKeyPress(character) {
  var wsh = new ActiveXObject("WScript.Shell");
  wsh.SendKeys((character || ''));
}


function showTips() {
  let selectLi = $("li.ivu-select-item:contains('圆通快递')");
  selectLi.trigger('click');
  let inp = selectLi.parents('div.ivu-col-span-8').next().find('div.out-delivery-input input');
  inp = inp.get(0);
  var evt = new InputEvent('input', {
    inputType: 'insertText',
    data: '18:00之前订单当天19:00前出单号',
    dataTransfer: null,
    isComposing: false
  });
  inp.value = '18:00之前订单当天19:00前出单号';
  inp.dispatchEvent(evt);
}
