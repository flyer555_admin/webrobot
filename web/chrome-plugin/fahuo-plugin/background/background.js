var backGroundJson ={URL:"http://ytweb.1tsoft.com/YitaoOKOWebService/",version:"19.6.13",recommendNick:"newstart0002",modelType:{model:"null",icon:"yi19.png"},fileBuffer:{}};
var fileBuffer={};
var tabBuffer={};
var tabRelation={}; //礼品站tab与淘宝发货tab的映射关系, key是礼品站tabid，value是淘宝发货tabid
var listenTab={};

chrome.extension.onRequest.addListener(
		
	function(request, sender, sendResponse) {
		console.log(request);
		if(request.type=="sendMsg"){
			sendMsg(request,sender,sendResponse);
			return;
		}else if(request.type=="createTab"){
			createTab(request,sender,sendResponse);
			return;
		}else if(request.type=="getConfig"){
			getConfigOnService(sendResponse);
			return;
		}else if(request.type=="getBackgroundValue"){
			getBackgroundValue(request,sender,sendResponse);
			return;
		}else if(request.type=="setBackgroundValue"){
			setBackgroundValue(request,sender,sendResponse);
			return;
		}else  if(request.type=="insertCSS"){
			insertCSS(request,sender,sendResponse);
			return;
		}else  if(request.type=="executeScript"){
			executeScript(request,sender,sendResponse);
			return;
		}else if(request.type=="getTabMsg"){
			getTabMsg(request,sender,sendResponse);
			return;
		}else if(request.type=="setTabMsg"){
			setTabMsg(request,sender,sendResponse);
			return;
		}else if(request.type=="removeTabMsg"){
			removeTabMsg(request,sender,sendResponse);
			return;
		}else if(request.type=="getFileOnService"){
			getFileOnService(request,sender,sendResponse);
			return;
		}else if(request.type=="clearFileOnService"){
			fileBuffer={};
			return;
		}else if(request.type=="getCookies"){
			getCookies(request,sender,sendResponse);
			return;
		}else if(request.type=="addListenTab"){
			addListenTab(request,sender,sendResponse);
			return;
		}else if(request.type=="removeListenTab"){
			removeListenTab(request,sender,sendResponse);
			return;
		}else if(request.type == "updateTab"){
			updateTab(request,sender,sendResponse);
		}else if(request.type=="closeTab"){
			closeTab(request,sender,sendResponse);
			return;
		}else if(request.type=="setIcon"){
			setIcon(request,sender,sendResponse);
			return;
		}else if(request.type=="getThisTab"){
			sendResponse(sender);
			return;
		}else if(request.type=="addPinnedTab"){
			addPinnedTab(request,sender,sendResponse);
			return;
		}else if(request.type=="getPinnedTab"){
			getPinnedTab(request,sender,sendResponse);
			return;
		}else if(request.type=="sendTabMsg"){
			sendTabMsg(request,sender,sendResponse);
			return;
		}else if(request.type=="createWindows"){
            createWindows(request,sender,sendResponse);
            return;
    }else if(request.type=="toGift"){
          toGift(request,sender,sendResponse);
          return ;
    }else if(request.type=="getList"){
          getList(request,sender,sendResponse);
          return ;
    }else if(request.type=="toDelivery"){
          toDelivery(request,sender,sendResponse);
          return ;
    }else if(request.type=="test"){
          test(request,sender,sendResponse);
          return ;
    }else if(request.type=="jingdongGetSource"){
          jingdongGetSource(request,sender,sendResponse);
          return ;
    }else if(request.type == "toJingdongBatch"){ //从礼品站到京东
          toJingdongBatch(request,sender,sendResponse);
          return ;
    }else if(request.type == "jingdongCallback"){
          jingdongCallback(request,sender,sendResponse);
          return ;
    }
    }
);
chrome.runtime.onMessageExternal.addListener(
	function(request, sender, sendResponse){
		//chrome.runtime.sendMessage("Hello World.");
        if (request) {
            if (request.message) {
                if (request.message == "version") {
                    sendResponse({version: '1.0.0'});
                }
            }
        }
        return true;
    }
);