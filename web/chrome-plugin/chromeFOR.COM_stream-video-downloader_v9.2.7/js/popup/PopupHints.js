(function(){
	
	var PopupHints = function(){
		
		var self = this;
		
		const HINTS_HEADER = [ 
					{ 	"name":	  "rate",
						"label":  "If you like us, please rate our product.",		
						"url": 	  "https://chrome.google.com/webstore/detail/flash-video-downloader/imkngaibigegepnlckfcbecjoilcjbhf/reviews",			  
						"icon":   " data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAQCAYAAACC/vbpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkFEQTNDMjY4NUY5NTExRTZBMkIxQzczQzM4MDcwOEVGIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkFEQTNDMjY5NUY5NTExRTZBMkIxQzczQzM4MDcwOEVGIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QURBM0MyNjY1Rjk1MTFFNkEyQjFDNzNDMzgwNzA4RUYiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QURBM0MyNjc1Rjk1MTFFNkEyQjFDNzNDMzgwNzA4RUYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4kv07tAAAA5UlEQVR42mL8//8/w1ADTAxDEAxJR2OA/ws0/w92NSgh/X+53hRkGqshg0ANIx5fMTImXCfk8wFRwwiW+P8byARiRi4kHd+ABCtQjBXKHzxqWL7/Z9nHycjgBBZAiQOIBpA8iB5Mapi4Ei+nP//0axu2NPX8zddtIPnBpgaWphWAyeQ+uiJgGlIEij+ApqVBowZcevxf5VAOFuVhY3j65tc6eDJaqFUOS/yDTQ3D/9UO///N0/yYbMPtCeTKJ9tweoIyKBhDq/nBpgZWtOgDMRvUUyBaH8WgQaSGcbTBNOpo3AAgwABq4nTyCrK95wAAAABJRU5ErkJggg=="	
					},	
					{ 	"name":	  "facebook",
						"label":  "No videos to download or wrong videos displayed? Click to read.",		
						"url": 	  "http://fvdmedia.com/facebook-video/",			  
						"icon":   "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAYCAIAAAB1KUohAAAACXBIWXMAAAsSAAALEgHS3X78AAADJUlEQVQ4jV1STWtdVRRda599815eXknSJrTGoiNTqlbMQASlo4KICoJDHYjg1JEo/oT+g+LYcakjlZZOxFFEpA2IgyYGadXYQtSXvo9779nLwblJ054Ll3P3XeuctddefPOzqyRpRuveoNFoRxWSZjAaDYeYsvemaWlmJhphIkkTzZLDYFDQSADBTNG6vzRR9LbJNNHERB6SU9JoXGeg8kSjaP2e9ypHAAVsJM3bNjpysKuaxtPm9QtrF19cWx72AxjOV99s7t7Y2jux0IvIh2R5mzODZBQxye3huPnkvZc/fON8SEbmHCnZ5u3d8Xg6mK9y6Ei5txEESTJkSeNZ89zZkx9cOgcgAjCBABBS3ea2zTnAQ+Xe5jASIEkHJtPm2dNDT5ZzeLKHk/rGjzv9Of9l9wGJuskSjmzzJofxaAFScTeHUsLX3//66Zc/rJ5adLf5Qb+pG5hZGJIY8rrJJGk0smnz3QcH/4wmAAQA2D+YtUpiEixyKAiDmSiDKS2/8DYAAHWb11aGH7/z0msXnj73zEkCJSCnlxcubZydNc3OnyNPFqEstaEmywVAMuOszk+dWvj8/VclAUjJALxyfm1j/Ywn++3e/esHk96cl6ZhBqNLAikJKqwnF0kAd++PnIwcAZBBAChkQEIy/ncwu765s7o02Fg/EyEz7v7179b2Xn/O79zb77lFziBFAiCYlp5/q9jjbvuj6ZVrP09nzbsX1+s2PNlX393+6PK3N3/6fdqgX7kkAiCLJg/B2AmmcWVx/sSgB5RooFf56tLCyuKwbnPkTDMBFGAU4IBCJAVBUp0jh45G1QUrt1FAEQBAEQbAJZGQCKqzDY/7JoVQHgKIUCcb3ahKD4IK6HEuAKnIFQgBVICAd3VCgtTdjifogWNjJCQyBFk8OlmSJEQIQOQoPaNrpZyqR/iQ4fg3JKmkwpIBSMY4rENS4DjeS8BCIJlD/bm0tb33xZWbk7od9Kpbd/YGfW+7AYiEgjSFYIQfz2RIc5Vt/7F/a/tvM0qY71UL/SpHkCzGkFCARIAuCGJJN8gcqjytLDrJ4m8OGXn8joIm8D8J1xnVhelZ2wAAAABJRU5ErkJggg=="
					},					
					{ 	"name":	  "stream",
						"label":  "Problem downloading Streaming content? Click to read.",		
						"url": 	  "http://fvdmedia.com/streaming-videos/",			  
						"icon":   "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAUCAYAAABF5ffbAAABWElEQVQYV2OMm3fwACe/iD0DFHz/8GYvY/SMPed5RCUNYIKfXz09yxgxdecxfgkZS5jgx6f3jzCGTd68V1BKyQkm+P7J3d2MYRM3bxGUUfKGCb57dGsjY1j/xpWCciphMMG3D24uZwzuXTtfREEjASb4+u61OYzBXauniChrZcMEX92+OJExoGNZp7iqfhlM8MX1M22M/m1L6iTUDRthgs+vna1i9G9eVCihZdwHE3x66WQ+o0/j/FRpHbNZMMHH5w8nMXrWzQ6X07daARN8dOZAKKOeW6yYuJ5pLQPTfxbG//9+3zuxq5kRqILROSjVhF9I1O7z+7cHd6+deZYxJKUmSFhCdiUTMxPL////fz9/dDeYMaGw5wQnH785POg+vTvKGFPYfZGXT0APJvjl3dtzjN5ReSVyqrrdEMF//+/duFgIsojDzjs2WkBIzPHd25d7j2xbsgIAOkiFc78dqlYAAAAASUVORK5CYII="
					},					
					{ 	"name":	  "audio",
						"label":  "Please read information about Audio downloads",		
						"url": 	  "http://fvdmedia.com/audio-download/",			  
						"icon":   "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAUCAYAAABF5ffbAAABWElEQVQYV2OMm3fwACe/iD0DFHz/8GYvY/SMPed5RCUNYIKfXz09yxgxdecxfgkZS5jgx6f3jzCGTd68V1BKyQkm+P7J3d2MYRM3bxGUUfKGCb57dGsjY1j/xpWCciphMMG3D24uZwzuXTtfREEjASb4+u61OYzBXauniChrZcMEX92+OJExoGNZp7iqfhlM8MX1M22M/m1L6iTUDRthgs+vna1i9G9eVCihZdwHE3x66WQ+o0/j/FRpHbNZMMHH5w8nMXrWzQ6X07daARN8dOZAKKOeW6yYuJ5pLQPTfxbG//9+3zuxq5kRqILROSjVhF9I1O7z+7cHd6+deZYxJKUmSFhCdiUTMxPL////fz9/dDeYMaGw5wQnH785POg+vTvKGFPYfZGXT0APJvjl3dtzjN5ReSVyqrrdEMF//+/duFgIsojDzjs2WkBIzPHd25d7j2xbsgIAOkiFc78dqlYAAAAASUVORK5CYII="
					}	
		];

		const HINTS_FOOTER = [ 
					{ "name":	"android",
					  "label":  "Android Version",
					  "url":    "http://fvdmedia.com/to/s/andr_getthemall/",
					  "icon":   "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsSAAALEgHS3X78AAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAA8FJREFUeNq0ljtsHFUUhr87j92Z2Y2dGK+dgjgCYkwByIoU8RCyUBIFCjoUSgpQIBRINFRABQUlEqkAd8EFQUJQUAACmdixEpBiFCXBDzCxDVFM7A1+7O7szL2HYnY3M951JIRypat56J7zz7nn//87SkS4m8MBePuzex9ycvYHtmU9pVC5/5NQkLo2ZiKu6zfee2H5VwfA891TQcE74uRslFKZAIVCmzpKWShlJ0lEI2KwrRxCdgdExI3r+pnKVu0UcDQBCPIjfuCiLKsNQETT6w9RjcrU4jIAvlPCz/WwVpnHUs52AFzHwoiMAFgAjoMSQIxgtMnMSrjOI70vMtz/Mi7duGo3B/eeZLjvJaq1fzAmu15MUpPjoFo9EEFEpK1chSKKQv689QtHht6h4PahlGKw9Czfz7xLParj5YRORJFGMgfAGMGY9kVaQkqFhzlQOsqlpa8Y3v88AD8vjHGgdIyl9SnK4Ry2yrfFNvNZyYNp2xqjDVuVMof2vcL9vSP0dT3YCu7vGmKg5zEO7XuVzUq5Y6wxhlQFt1+AQpuQMN7EUnnOzX3E+dlPydkBYVwBIO8WCKNNjNrCVh6btZvknSK2lYfGNmcAtBa01gBEpsrAnid4fP9r/DD7Pvf1jDCw58kGx5ufkIzF8hS/ryqeHnqTn659wrXyOVzLp5kzVYHGmCQsrFfo8QZ5oHSYi4tn6O96lMG9hzuKqhqX+XtjnsHSMeZvjDNb/w47l6eZ8zaAFrROSoqimEYxSKyI4tqOqq1HNXQsrS+OohhtJ3lMtgLTqsAY06KdMcKdrEpEaJJPRDK9zPQgEViyUrQgjfK00dwZAYyOk1ujEZ3Kk6apNqY1lXK5Xr7ClaVvWF6bxmr4T6eRcwKur11meuEL/lq7jFK5TK5UDwzaSoJcu8BvK5NcWvyauhaeO7hrRwDXCVi4Oc3o+HH8fBeeF7TYaBo9bROajmMcO0d3scQuP2Dy6igb1RUALsyOcWF2DICN6goTV0cp+gFdhXtwbBcdx21Cs9JCa06R5Jp3i0zMnGF1YwGA02dPcPrsCQBWN/5gcuZz8m4xE5OeKRaBaik54ygEHtTiTcKoQnexHxC2wjKV+i0CD3Sjye1elDrRRIxq0nT78NwCX55/C9/dTS3aQCF8/O1xwmgd3y10NMkGbVN2bYwY1RnAVi7LaxcxEuPligDM3RjHUjZ510952DYAIxm7nrIUIzucsbiO1/T4xOycICOmzlskU2kln0SpD9UOIP/54Bd+NEZeB1B3+7fl3wEAGMdn3JuIkD0AAAAASUVORK5CYII="	
					},			 				
					{ "name":	"speeddial",
					  "label":  "3D Speed Dial",
					  "url":    "https://chrome.google.com/webstore/detail/llaficoajjainaijghjlofdfmbjpebpa",
					  "icon":   "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAABCFJREFUeNqUld+LVVUUxz/r7HPPvXfu/Lpjjlpm6qgDkv3GUCkNizF6SBJ8iECcfhH0lBAV/QGFD1FBpZHVS1E0JQhJZjCFmQYhJvlgooI2KI7oTHfm3nPO3mv1MNea0bFsw2ZvFqwPay3WWl8xM/r7+xERVBUR4RpnBXAv8DkwNMkeAda8U4zXc+4BdpnZG8BnQFvTPhf4BnjzSofrAT8A/AicFJHngeXAINACdAAPAo9c6RT/B3QdsBM4BTzaLMHvzSj3A69fy/HfIn4O2A0cBVYBQyJCFmyPGg8Bi4CP/i94C8Y7kemvzkKfMz/sLFDLjGWzY4oxe1Nv64NyYTpnEbmqFLHBu7H5pwT7cty1PXu8felwLZlBSVI+XrCP2W0Rw2mZH06me38Z8n0X6jpQKbDYiTxjsH26Gicg2wuab/qz2PnFUHnBE3+09qaBgBGTu5yeqpAbVEsRG26tsKYnHPnueGPlT6fz7cHYFgnzgVcmg5cC78Wi96Wu5bX9cx7emrniky0+rZpEZkYamR/Ig51SsZuDNB5XFeko4frvlnMLq6UX3j7YCN0tvGwgwKeXwTvNbDEWXhwqz9s6Uun8ILuxq/9cUqZQSghJgXnHfttEiFeV2499WGk5uJYsAg+SF7m9e82ehZ2tfefH3dZybC+Z2cbL4KeBQpB47+m23pmx+BUjQIRhCKFcJu2ozgkUe5272Ot0HALggaxGd/H8or6ehB2Hy6+W4ICZjV0Gf5+EBserd5InFU2yrOHG6kgwclOIHZZlhLLkIZfMGZAB6cRby2Pun5ux73QhPXXJDZTiZrs584wWuzjTOh9FxWuIrZHh05Q4TaGREasncRq5EJw1geRNeFDEjL75dRIHqW+223DrLA7NWctYoYNCyIbTuLBb4/IyJMGTQKqEcX+i0MlRx6JBQn0TqmAGSYVicclRkxZWL1Ryrwye8IiZse22uxiuzEVxgBEiV/S4xxSqYqZiEpxLd13cPHQ2othOVt7gIko5ihMa45YOeGy0EMHZmnJmRCciXnnkEI5D/0wOdAv0GBQFzEF2KZFZqze2nK1LfBO5LCASw0JEsBpINyKjf89ytdnHdZk02UbsKvJW+YZovRMhxigYaI3NqsmKwsyu98sd1VVePZH3aJaTDY9sMB+WM2mXxwAyZUXTRcSS4EAUVASJhSTWdjzzoii6xYkhqogPeDMskhkWCVeBjSmqEdQkCypghllEPPFFBJ+r5mmWI7knyjzO55RUMWOKiExEPFVVMCAXsGginQzIBLE4MjO1KATwnlgD6nNMFTObIk4xgEY2mXqBjE8Y1Tt8E27eyOp2QITDjNW/soZuKaiSakB9IA0MXpH1NAoiIMG+lVH72cSqwURRM4tlBwKS69dWH1+XI3FqhgmpTbPw/xoAsPATugLmhUkAAAAASUVORK5CYII="	
					},			 				
					{ "name":	"capture",
					  "label":  "Capture Screenshots",
					  "url":    "https://chrome.google.com/webstore/detail/bpconcjcammlapcogcnnelfmaeghhagj",
					  "icon":   " data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjMyNTA2ODI4RDQ0RTExRTJBNDY4Q0U4QjM1RUVGNEFEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjMyNTA2ODI5RDQ0RTExRTJBNDY4Q0U4QjM1RUVGNEFEIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MzI1MDY4MjZENDRFMTFFMkE0NjhDRThCMzVFRUY0QUQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzI1MDY4MjdENDRFMTFFMkE0NjhDRThCMzVFRUY0QUQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6qZ25GAAADBElEQVR42qyVTWxMURTHz3lvpu1QjUVTSenCgiJRjaAbBAkiQUIiIRGENkTChk1ZkBBdEkmtCGVjYUFtEAshvvoRMqFoLGiRNJ2UmJq25r13/O99d9pbpjMj7Ul+773zzr333HvuuecSEX0AC8EdMAJShjRopFAegSHLptodMLYr4LdlGwbnjY0YfAHbwAww1wysJAaegzjYBGaBwNii4AnoBivBAuOETP/34FXGQQ/YBZ5qsxMlZ/ZGommVGM6DHqEg9Y3o6/2we0kFcdUW4sCM5xRR0HsX6+sjEk/9WWtW+8J2sBOfz6h8KTlVW/Hp1pDjlpEIvlkC328nfygckV3iaGw+i1SEOifJ97so2Z0OPl5Xf9rBO7BHKRFQpt+YqR5c+RSvhXyvNhNHEmrATC+P6v5IE57bjfaGuHgdvPZTFnHAKtCpNb1sUV+D4xoxHcf/OaFN21OWGW3VUmVsOpaiHPRbGzuRVKPPISpMSkCx7eAeqM3Xy2FuMOmcT9QedGWUiEm/aAEdKxySYwFxvR2CLHLESme9gnSeDmPxZt6H53q0TuVovwHU2Q6cXNMWkdeBSKtRXYelkUnKc3Q5Cw7bIfplLymL4OjzGbhajfdMsAaZPJxnxcP2Cqozp24CURnRIcRXrfISKzCjtAOVQaU52rgIicr0i3j2FDCmmlCR7aAFLMrTyQWfWLi5AAc3wYP/TVOmcKMuoQbtZubF/5jdkozS9HeIRuuDqoxmLDtk+ptRZVHoBrEXp8dnNZeixLAk2pRSAw6C5fYKpusQoDQHva2mmkb2jlZTkSSJ71PfY12qqWzebXIjdfBRhHOBwpdOkpf8IYkOMgXwFGg2J1o7ULfVdz2rRCcFA3F1H8TD+8DXBVA+3yLyhkgPGKsMqGpzGwVpHRZJvCQZiGcmbF86Y7GdpCwBK0wF3mFux4fgmto2NjfQfnPPknUlqpQ8p2oQOGEyyRbf3NlHTViySRCZ5Oxdc0hvgJ+mBi0DbwE2jTyaYjlpMvKCnaZTLSm7tv0RYAA9Q/YLyp4SvwAAAABJRU5ErkJggg=="	
					},			 				
					{ "name":   "wallpapers",
					  "label": "Live Wallpapers",
					  "url":   "http://livestartpage.com/go.php?utm_source=fvd&utm_medium=main_window",
					  "icon":   "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAAKMGlDQ1BJQ0MgcHJvZmlsZQAASImdlndUVNcWh8+9d3qhzTAUKUPvvQ0gvTep0kRhmBlgKAMOMzSxIaICEUVEBBVBgiIGjIYisSKKhYBgwR6QIKDEYBRRUXkzslZ05eW9l5ffH2d9a5+99z1n733WugCQvP25vHRYCoA0noAf4uVKj4yKpmP7AQzwAAPMAGCyMjMCQj3DgEg+Hm70TJET+CIIgDd3xCsAN428g+h08P9JmpXBF4jSBInYgs3JZIm4UMSp2YIMsX1GxNT4FDHDKDHzRQcUsbyYExfZ8LPPIjuLmZ3GY4tYfOYMdhpbzD0i3pol5IgY8RdxURaXky3iWyLWTBWmcUX8VhybxmFmAoAiie0CDitJxKYiJvHDQtxEvBQAHCnxK47/igWcHIH4Um7pGbl8bmKSgK7L0qOb2doy6N6c7FSOQGAUxGSlMPlsult6WgaTlwvA4p0/S0ZcW7qoyNZmttbWRubGZl8V6r9u/k2Je7tIr4I/9wyi9X2x/ZVfej0AjFlRbXZ8scXvBaBjMwDy97/YNA8CICnqW/vAV/ehieclSSDIsDMxyc7ONuZyWMbigv6h/+nwN/TV94zF6f4oD92dk8AUpgro4rqx0lPThXx6ZgaTxaEb/XmI/3HgX5/DMISTwOFzeKKIcNGUcXmJonbz2FwBN51H5/L+UxP/YdiftDjXIlEaPgFqrDGQGqAC5Nc+gKIQARJzQLQD/dE3f3w4EL+8CNWJxbn/LOjfs8Jl4iWTm/g5zi0kjM4S8rMW98TPEqABAUgCKlAAKkAD6AIjYA5sgD1wBh7AFwSCMBAFVgEWSAJpgA+yQT7YCIpACdgBdoNqUAsaQBNoASdABzgNLoDL4Dq4AW6DB2AEjIPnYAa8AfMQBGEhMkSBFCBVSAsygMwhBuQIeUD+UAgUBcVBiRAPEkL50CaoBCqHqqE6qAn6HjoFXYCuQoPQPWgUmoJ+h97DCEyCqbAyrA2bwAzYBfaDw+CVcCK8Gs6DC+HtcBVcDx+D2+EL8HX4NjwCP4dnEYAQERqihhghDMQNCUSikQSEj6xDipFKpB5pQbqQXuQmMoJMI+9QGBQFRUcZoexR3qjlKBZqNWodqhRVjTqCakf1oG6iRlEzqE9oMloJbYC2Q/ugI9GJ6Gx0EboS3YhuQ19C30aPo99gMBgaRgdjg/HGRGGSMWswpZj9mFbMecwgZgwzi8ViFbAGWAdsIJaJFWCLsHuxx7DnsEPYcexbHBGnijPHeeKicTxcAa4SdxR3FjeEm8DN46XwWng7fCCejc/Fl+Eb8F34Afw4fp4gTdAhOBDCCMmEjYQqQgvhEuEh4RWRSFQn2hKDiVziBmIV8TjxCnGU+I4kQ9InuZFiSELSdtJh0nnSPdIrMpmsTXYmR5MF5O3kJvJF8mPyWwmKhLGEjwRbYr1EjUS7xJDEC0m8pJaki+QqyTzJSsmTkgOS01J4KW0pNymm1DqpGqlTUsNSs9IUaTPpQOk06VLpo9JXpSdlsDLaMh4ybJlCmUMyF2XGKAhFg+JGYVE2URoolyjjVAxVh+pDTaaWUL+j9lNnZGVkLWXDZXNka2TPyI7QEJo2zYeWSiujnaDdob2XU5ZzkePIbZNrkRuSm5NfIu8sz5Evlm+Vvy3/XoGu4KGQorBToUPhkSJKUV8xWDFb8YDiJcXpJdQl9ktYS4qXnFhyXwlW0lcKUVqjdEipT2lWWUXZSzlDea/yReVpFZqKs0qySoXKWZUpVYqqoypXtUL1nOozuizdhZ5Kr6L30GfUlNS81YRqdWr9avPqOurL1QvUW9UfaRA0GBoJGhUa3RozmqqaAZr5ms2a97XwWgytJK09Wr1ac9o62hHaW7Q7tCd15HV8dPJ0mnUe6pJ1nXRX69br3tLD6DH0UvT2693Qh/Wt9JP0a/QHDGADawOuwX6DQUO0oa0hz7DecNiIZORilGXUbDRqTDP2Ny4w7jB+YaJpEm2y06TX5JOplWmqaYPpAzMZM1+zArMus9/N9c1Z5jXmtyzIFp4W6y06LV5aGlhyLA9Y3rWiWAVYbbHqtvpobWPNt26xnrLRtImz2WczzKAyghiljCu2aFtX2/W2p23f2VnbCexO2P1mb2SfYn/UfnKpzlLO0oalYw7qDkyHOocRR7pjnONBxxEnNSemU73TE2cNZ7Zzo/OEi55Lsssxlxeupq581zbXOTc7t7Vu590Rdy/3Yvd+DxmP5R7VHo891T0TPZs9Z7ysvNZ4nfdGe/t57/Qe9lH2Yfk0+cz42viu9e3xI/mF+lX7PfHX9+f7dwXAAb4BuwIeLtNaxlvWEQgCfQJ3BT4K0glaHfRjMCY4KLgm+GmIWUh+SG8oJTQ29GjomzDXsLKwB8t1lwuXd4dLhseEN4XPRbhHlEeMRJpEro28HqUYxY3qjMZGh0c3Rs+u8Fixe8V4jFVMUcydlTorc1ZeXaW4KnXVmVjJWGbsyTh0XETc0bgPzEBmPXM23id+X/wMy421h/Wc7cyuYE9xHDjlnIkEh4TyhMlEh8RdiVNJTkmVSdNcN24192Wyd3Jt8lxKYMrhlIXUiNTWNFxaXNopngwvhdeTrpKekz6YYZBRlDGy2m717tUzfD9+YyaUuTKzU0AV/Uz1CXWFm4WjWY5ZNVlvs8OzT+ZI5/By+nL1c7flTuR55n27BrWGtaY7Xy1/Y/7oWpe1deugdfHrutdrrC9cP77Ba8ORjYSNKRt/KjAtKC94vSliU1ehcuGGwrHNXpubiySK+EXDW+y31G5FbeVu7d9msW3vtk/F7OJrJaYllSUfSlml174x+6bqm4XtCdv7y6zLDuzA7ODtuLPTaeeRcunyvPKxXQG72ivoFcUVr3fH7r5aaVlZu4ewR7hnpMq/qnOv5t4dez9UJ1XfrnGtad2ntG/bvrn97P1DB5wPtNQq15bUvj/IPXi3zquuvV67vvIQ5lDWoacN4Q293zK+bWpUbCxp/HiYd3jkSMiRniabpqajSkfLmuFmYfPUsZhjN75z/66zxailrpXWWnIcHBcef/Z93Pd3Tvid6D7JONnyg9YP+9oobcXtUHtu+0xHUsdIZ1Tn4CnfU91d9l1tPxr/ePi02umaM7Jnys4SzhaeXTiXd272fMb56QuJF8a6Y7sfXIy8eKsnuKf/kt+lK5c9L1/sdek9d8XhyumrdldPXWNc67hufb29z6qv7Sern9r6rfvbB2wGOm/Y3ugaXDp4dshp6MJN95uXb/ncun572e3BO8vv3B2OGR65y747eS/13sv7WffnH2x4iH5Y/EjqUeVjpcf1P+v93DpiPXJm1H2070nokwdjrLHnv2T+8mG88Cn5aeWE6kTTpPnk6SnPqRvPVjwbf57xfH666FfpX/e90H3xw2/Ov/XNRM6Mv+S/XPi99JXCq8OvLV93zwbNPn6T9mZ+rvitwtsj7xjvet9HvJ+Yz/6A/VD1Ue9j1ye/Tw8X0hYW/gUDmPP8FDdFOwAAAAlwSFlzAAALEgAACxIB0t1+/AAABEtJREFUOI2FlV2IVVUUx39rn3PvnTu3O045OTkO2Exq5lc9JNkUTZCEEUVQEFFBD5ZR9BRWilHUU0UvBhVaFEFQERYRBH1A0cdAQ1l+VIYjaprK6ORc79xzzt5n79XDHG2Y0fzDYsE+//Vnsb6OqAbOB1VEBD0vcQpk28+bpr8ZIACLReQFm+V1NXLHBXHUuOF4O2UVFLYB7cB6YOeUmDOIjzVGziV8pzFye5rkjP2TrunrqX9gj9eIVPoC3AzUgO3AhrMJG9QwzbTwu0IuVNvKnDjmHsoSBRF84Kqg1IJCUHYWfobF1qUzSlr4nRrUVmtxOU3tTSN7TtwczSl/nme6EgNADuwouDMaFTtnzyX8lyqHTOr7653CL9+NbcwGZ31eylnqAYEjwP5pMf8JnyNjAzhVjvgg/R0d6g6ftIPv7z627oGFnV2pCxhhFGieUzibKTwVXjyoYOo1w4f7Gy/dekks7ZGQKyqTHDl7xjab/naaWAF6AUQQE6Ntlna1qckNeKVHoANonE08zmw6VdAAEWCBG4F+QE0kGkrxqAQ/B9sKbpI7F7gJ+AgoAZ7JJupkxi41hWBefPDAFcCrihARNLNxNK+v/MSpCfv4iWa+oruiPg1gYDOwD/h1StYxEIx1mVqX5dZlFeuya63LXrPODmfO9fu8pWjT7DzQTT1Otz/cc2r10Hj4OU0nIrGtkNu0N7fJUG6TrblNrsttUsltkuc20di67EpgHXALYuaDgM+IjaPhL9TfDi2Rv8e7/ly89MieBel4bGz9ua9Odr66qnqyp12aecBUPGYtsFbQg8BnwOty/8tdE0A7KPhE8VZCR9+uw43LG3/8XhtIUkPvNVd/eeny5QdmHRy597K9w209+3coBumuNJkno9QkRRF8VCoqQkvueN7sAl1qTGy1vvyHZtctWyY6Bz/e+8mn3xjfWtm9ajDU5i8kqJg8Lqlo0L6RYbPyx4/yUto6SSkqzZbxfK6OVmZlR9sIPgZ2x4vmDWwsaXb9aKl/69CsR0eiaj00hr94rFKLV1503V0+bqvRap6KAIwGUUR39a7wBzp7pZo212fl6ttezcVBMZv2v9O1YmLf/S1TGZI8BIlEdOjYCXP3l9+HajI+Pxs9+lPbgmWzCSEPPo9F5DiTd6ETWCYayt6UVI3xouGRSNjazIP5YPVAGJgzG68q8eQCSey9cxOnGnUVeYtLLp3dHB93oCWQV4CngfFi3hcCzwjJvcWYbjEi/YlzG9S5CDCo+lgmi+1c7ruTpPV1XKksDhNNVwz9g8AbhYAp5nQvcF/R/TeBshF5KrWuK6iuAxwiEqMoAj7PXXJ8rJJVygRVgHuA96ZtFcVmGuBd4CCwzYh0BWvXOOc6gTEU4uJfZubU2sfWLFl0W7UUPxtUNwPfFlvkpt0SX1hccAaNyJOJy1/saq+OAUaEIKpnbkdxrM4ck6gQ+D+cLo9Oj/8X0IM+FbwOydIAAAAASUVORK5CYII="	
					}			 				
		];
		
		const HINTS_MESSAGE = [ 
					{ "name":	"youtube",
					  "html":   '<div class="message-wrapped">'+
					  			'  <span><%popup_noyoutube_message1%></span><br>'+
								'  <span><%popup_noyoutube_message2%></span><br>'+
								'  <span><%popup_noyoutube_message3%></span>'+
								'</div>',
					  "button": false			
					},			 				
					{ "name":	"stream",
					  "html":   '<div class="message-wrapped streams">'+
					  			'  <div class="title"><%popup_streams_message1%></div><br>'+
								'  <div class="text"><%popup_streams_message2%></div>'+
								'  <div class="text"><%popup_streams_message3%></div><br><br>'+
								'  <div class="image">'+
								'     <img src="/images/popup/message_converter.png">'+
								'  </div><br>'+
								'  <div class="button">'+
								'     <a href="https://chrome.google.com/webstore/detail/llaficoajjainaijghjlofdfmbjpebpa" target="_blank">'+
								'        <div><%popup_streams_button%></div>'+
								'     </a>'+ 
								'  </div>'+
								'</div>',
					  "button": true			
					}			 				
		];
		
		const INTERVAL_TO_DISPLAY_RATE = 2 * 24 * 3600; // 2 days

		var hints_header = null, hints_footer = null;
		
		// ---------------------------------------------- INIT ---------------------------
		this.init = function(){		
		
			fvdDownloader.Utils.getActiveTab( function( tab ){
					if( tab )	{
	
						var rate = buildHints( tab );
							
						buildHeader( rate.header );

						buildFooter( rate.footer );

					
			
					}
			});			
			
		}

		// ----------------------------------------------------- 
		function buildHints( tab ){

			var url = tab.url;
			var isStream = false;
			var media = fvdDownloader.Media.getMedia( tab.id );
			if (media) {
				for (var i=0; i<media.length; i++) { 
				  if (media[i].type == 'stream' || media[i].type == 'record') isStream = true; 
				}    
			}	

			var text = fvdDownloader.Prefs.get( "hints_disabled" );			
			var flagDisabled = text ? JSON.parse(text) : [];

			var flagRate = false;
			var flagProblem = false;
			var flagPriority = false;
			var hh = null;
			var installTime = fvdDownloader.Prefs.get( "install_time" );

			// показ сообщения об facebook
			if (flagDisabled.indexOf('facebook') == -1 && /^https?:\/\/www\.facebook\.com\//.test(url))  {
			  for (var i=0; i<HINTS_HEADER.length; i++) {
				  if (HINTS_HEADER[i].name == 'facebook') {
					  flagPriority = true;
					  hh = HINTS_HEADER[i];
				  }
			  }
			}  

			// показ сообщения об soundcloud
			if (flagDisabled.indexOf('audio') == -1 && /soundcloud\.com/.test(url))  {
			  for (var i=0; i<HINTS_HEADER.length; i++) {
				  if (HINTS_HEADER[i].name == 'audio') {
					  flagPriority = true;
					  hh = HINTS_HEADER[i];
				  }
			  }
			}  

			// показ сообщения об stream
			if (!flagPriority && flagDisabled.indexOf('stream') == -1 && isStream )  {
			  for (var i=0; i<HINTS_HEADER.length; i++) {
				  if (HINTS_HEADER[i].name == 'stream') {
					  flagPriority = true;
					  hh = HINTS_HEADER[i];
				  }
			  }
			}  

			// показ сообщения об ошибке
			if (!flagPriority) {
			  if (flagDisabled.indexOf('problem') == -1)  {
				for (var i=0; i<HINTS_HEADER.length; i++) {
					if (HINTS_HEADER[i].name == 'problem') {
						flagProblem = true;
						hh = HINTS_HEADER[i];
					}
				}
			  }  
			} 

			// показ рейтинга
			var now = parseInt(new Date().getTime() / 1000);
			if (!flagProblem && !flagPriority) {
				if (flagDisabled.indexOf('rate') == -1)  {
				  for (var i=0; i<HINTS_HEADER.length; i++) {
					  if (HINTS_HEADER[i].name == 'rate') {
						  flagRate = true;
						  hh = HINTS_HEADER[i];
					  }
				  }
				}  
				if (flagRate) {
					if( now - installTime < INTERVAL_TO_DISPLAY_RATE )       flagRate = false;
				}

				if (!flagRate) {      // случайный выбор
					// выбор сообщений
					var h = [];
					for (var i=0; i<HINTS_HEADER.length; i++) {
						if (flagDisabled.indexOf(HINTS_HEADER[i].name) == -1 && 
							['rate', 'facebook', 'stream', 'audio'].indexOf(HINTS_HEADER[i].name) == -1) {
										h.push( HINTS_HEADER[i] );
						}
					}
					var k = h.length;
					if (k>0) {             // случайность    
						var ind = Math.floor(Math.random() * k );
						hh = h[ind];
					}
				}
			}    
    
			// нижние сообщения
			if( now - installTime > INTERVAL_TO_DISPLAY_RATE )       {
				flagRate = true;
			}
			if (flagDisabled.indexOf('rate') != -1)  flagRate = false; 
			if (flagRate && HINTS_FOOTER.length>0 && HINTS_FOOTER[0].name != 'rate')  flagRate = false; 

			var f = [];
			for (var i=0; i<HINTS_FOOTER.length && f.length<4; i++) {
				if (!flagRate && HINTS_FOOTER[i].name == 'rate') continue;
				f.push( HINTS_FOOTER[i] );
			}
		
			return {header: hh, footer: f};
		}
		
		// ----------------------------------------------------- 
		function closeHints( name ){
		
			console.log(name);
			
			var text = fvdDownloader.Prefs.get( "hints_disabled" );			
			var flagDisabled = text ? JSON.parse(text) : [];
			
			flagDisabled.push(name);
			text = JSON.stringify(flagDisabled);
			fvdDownloader.Prefs.set( "hints_disabled", text);

		}
		
		// ----------------------------------------------------- 
		function buildHeader( rr ){

			var container = document.getElementById("header_container");
			while( container.firstChild )  {
				container.removeChild( container.firstChild );
			}

			if (rr) {
				container.removeAttribute('style');

				if (rr.name == 'rate') {
					var d = document.createElement("div");
					d.setAttribute("class", "header-rate");
					var d1 = document.createElement("div");
					d1.setAttribute("class", "header-rate-text");
					d1.textContent = rr.label;
					var d2 = document.createElement("div");
					d2.setAttribute("class", "header-rate-img");
					d.appendChild(d1);
					d.appendChild(d2);

					var a = document.createElement("a");
					a.setAttribute("target", "_blank");
					a.appendChild(d);
					container.appendChild(a);
					
					a.addEventListener( "click", function( event ){
							self.navigate_url( rr.url );
							event.stopPropagation();
						}, false );

					var div = document.createElement("div");
					div.setAttribute("class", "close");
					container.appendChild(div);
					
					div.addEventListener( "click", function( event ){
							closeHints( rr.name );
							container.setAttribute('style', 'opacity: 0');
							a.removeAttribute('href');
							event.stopPropagation();
						}, false );

				}
				else {
					var a = document.createElement("a");
					a.setAttribute("class", "text");
					a.setAttribute("target", "_blank");
					a.setAttribute("style", "background: url('"+rr.icon+"') 6px center no-repeat;");
					a.setAttribute("href", rr.url);
					a.textContent = rr.label;
					container.appendChild(a);
					var div = document.createElement("div");
					div.setAttribute("class", "close");
					container.appendChild(div);

					div.addEventListener( "click", function( event ){
							closeHints( rr.name );
							a.setAttribute('style', 'opacity: 0');
							a.removeAttribute('href');
							div.setAttribute('style', 'opacity: 0');
							event.stopPropagation();
						}, false );
				}	
			}	
			else {
				container.setAttribute('class', 'headerHint');
			}

		}
		
		// ----------------------------------------------------- 
		function buildFooter( rr ){

			var container = document.getElementById("footer_container");
			while( container.firstChild )  {
				container.removeChild( container.firstChild );
			}

			for (var i=0; i<rr.length; i++) {

				if (rr[i].name == 'rate') {
					var d = document.createElement("div");
					d.setAttribute("class", "footer-rate");
					var d1 = document.createElement("div");
					d1.setAttribute("class", "footer-rate-text");
					var d2 = document.createElement("div");
					d2.setAttribute("class", "footer-rate-img");
					d.appendChild(d1);
					d.appendChild(d2);

					var a = document.createElement("a");
					a.setAttribute("target", "_blank");
					a.setAttribute("href", rr[i].url);
					a.appendChild(d);
					container.appendChild(a);

				}
				else {
					var a = document.createElement("a");
					a.setAttribute("class", "footer-item");
					a.setAttribute("target", "_blank");
					a.setAttribute("style", "background-image: url('"+rr[i].icon+"'); background-position: 0px center;");
					a.setAttribute("href", rr[i].url);
					a.textContent = rr[i].label;
					container.appendChild(a);
				}	
			}
		

		}	

		// ---------------------------------------------- INIT ---------------------------
		this.message = function(type){		

			var xx = null;

			for (var i=0; i<HINTS_MESSAGE.length; i++) {
				if (HINTS_MESSAGE[i].name == type) {
					  var xx = HINTS_MESSAGE[i];
					  break;
				}
			}
			
			if (xx) {
				xx.html = xx.html.replace(/\<\%(.+?)\%\>/gm, function(m) { 
					var mm = m.match(/\<\%(.+?)\%\>/);
					if (mm) {
						return _(mm[1]);	
					}	
					return m;
				});

				document.getElementById("download_item_container").setAttribute('hidden', true);

				var container = document.getElementById("containerMessage");
				container.removeAttribute('hidden');

				while( container.firstChild )	{
					container.removeChild( container.firstChild );
				}

				container.innerHTML = xx.html;

				if (xx.button) {
					var div = document.createElement("div");
					div.setAttribute("class", "message-disabled");
					container.appendChild(div);

					var div1 = document.createElement("div");
					div1.setAttribute("class", "message-disabled-wrapped");
					div.appendChild(div1);

					var btn1 = document.createElement("button");
					btn1.setAttribute("class", "message-button-dont-show");
					btn1.setAttribute("type", "button");
					btn1.textContent = _("popup_disabled_message");
					div1.appendChild(btn1);
					
					btn1.addEventListener( "click", function( event ){
							_close();
							event.stopPropagation();
						}, false );
					

					var btn2 = document.createElement("button");
					btn2.setAttribute("class", "message-button-closed");
					btn2.setAttribute("type", "button");
					btn2.textContent = _("popup_closed_message");
					div1.appendChild(btn2);
					
					btn2.addEventListener( "click", function( event ){
							_close();
							event.stopPropagation();
						}, false );
					
				}
		
			}			

			function _close() {
				document.getElementById("download_item_container").removeAttribute('hidden');
				container.setAttribute('hidden', true);
			}	


			return null;
		}

		// ----------------------------------------------
		this.navigate_url = function( url ){
			chrome.tabs.query( 	{
							url:  url 
						}, function( tabs ){

									if( tabs.length > 0 )
									{
										foundTabId = tabs[0].id;
										chrome.tabs.update( foundTabId, {
																		active: true
																		} );
									}
									else
									{
										chrome.tabs.create( {	active: true,
																url: url
															}, function( tab ){ }
														);
									}
					} );
		}
		
	}
	
	this.PopupHints = new PopupHints();
	
}).apply( fvdDownloader );
