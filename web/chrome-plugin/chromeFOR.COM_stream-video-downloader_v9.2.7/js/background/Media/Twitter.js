(function(){
    
    var Twitter = function(){     

        const DEBUG = false;
    
        const TITLE_MAX_LENGTH  = 96;
        const EXT_PATTERN = new RegExp("\\.([a-z0-9]{1,5})(?:\\?|#|$)", "i");
        const NAME_PATTERN = new RegExp("/([^/]+?)(?:\\.([a-z0-9]{1,5}))?(?:\\?|#|$)", "i");

        var tw_video_thumb = [];


        // --------------------------------------------------------------------------------
        this.detectMedia = function( data, callback ){

            var url = data.url.toLowerCase();

            if( /^https?:\/\/video\.twimg\.com\/[^\?]*\.m3u8/.test(url) )  {
                detectTwitterVideo( data, callback )
                return;
            }   
            else if( /^https?:\/\/pbs\.twimg\.com\/ext_tw_video_thumb\/(.+?)\.jpg/.test(url) )  {
                var m = url.match(/ext_tw_video_thumb\/([^\/]*)/i);
                if (m) {
                    tw_video_thumb[m[1]] = data.url;    
                }   
                callback(null, true);
                return;
            }    
            else if( /https?:\/\/video\.twimg\.com\/(.*)\.ts/.test(url) )  {
                callback(null, true);
                return;
            } 

            callback(null);
        }
        
        // --------------------------------------------------------------------------------
        function detectTwitterVideo( data, callback ){

            if (DEBUG) console.log( data );

            var url = data.url,
                hh = hex_md5(url);

            var parsedMedia = [];
            var mediaFound = false;


            var domain = null, k, tt, host = "", prot = "";
            var x = fvdDownloader.Utils.parse_URL(url);
            
            host = x.protocol + '//' + x.hostname + (x.port ? ':'+x.port : '') + x.path+'/';
            domain = x.protocol + '//' + x.hostname + (x.port ? ':'+x.port : '');
            search = x.search || "";
            var groupMedia;
            
            fvdDownloader.Utils.getAJAX( url, null, function(content){
                
                    _parse(content);

                    if (mediaFound) {
                        data.foundMedia = "Twitter"; 
                        callback(parsedMedia, true);
                    }   
                    else {
                        callback(null);
                    }

                
            });

            // ---------------------
            function _parse( content ){
                
                var lines = content.split('\n');

                if ( lines.length<2 ) return;
                if ( lines[0].replace(/\r/, '') != '#EXTM3U' ) return;

                var flag = 0;
                var file_name = null, ext = null;
                groupMedia = fvdDownloader.Storage.nextGroupId(); 

                for (var i=0; i<lines.length; i++) {

                    var line = lines[i].trim().replace(/\r/g,'');
                    if (line.indexOf('#') == 0)  continue;
                    
                    if (!flag) {
                        var k = line.indexOf('.m3u8');
                        if ( k != -1 )  flag = 1;           // playlist
                        else            flag = 2;           // fragment
                    }
                    
                    if (flag == 1)  {

                        var w = NAME_PATTERN.exec(line);
                        if (w)  file_name = w[1];
                        var l = EXT_PATTERN.exec(line);
                        if (l) ext = l[1];

                        if (line.indexOf('http') != 0) {
                            if (line.indexOf('/') == 0)  line = domain + line;
                            else    line = host + line;
                        }   
                        if (line.indexOf('?') == -1 && search) {
                            line = line + search;
                        }    

                        _add(line, file_name);    
                    }
                    else if (flag == 2)  {
                        var w = NAME_PATTERN.exec(url);
                        if (w)  file_name = w[1];
                        var l = EXT_PATTERN.exec(url);
                        if (l) ext = l[1];
                        break;
                    }

                } 

            }   

            // ---------------------
            function _add( url, file_name ){

                if (DEBUG) console.log('addMedia', url, file_name);
                
                var n = url.match(/\/([0-9]+)\//i);
                var videoId = n ? n[1] : file_name;
                n = url.match(/\/([0-9]+)x([0-9]+)\//i);
                var quality = n ? { width: parseInt(n[1]), height: parseInt(n[2]) } : null;
                var hash = videoId + '_' + (quality ? quality.height : file_name);
                var ext = 'mp4';
                
                if (!quality) return;

                var thumbnail = tw_video_thumb[videoId] ? tw_video_thumb[videoId] : data.thumbnail;

                var ft = [];
                ft.push({tag: 'span', content: '['+(quality ? quality.width+'x'+quality.height : '')+', ' });
                ft.push({tag: 'b',    content: fvdDownloader.Utils.upperFirst(ext) });
                ft.push({tag: 'span', content: '] ' });

                var title = data.tabTitle ? data.tabTitle : file_name;

                var q = quality ? quality.height : null;

                var pp = {  video: { url:  url, 
                                     ext:  ext,
                                     hash: hash   },
                         };

                parsedMedia.push( {
                        url:        url,
                        tabId:      data.tabId,
                        tabUrl:     data.tabUrl,
                        frameId:    data.frameId,
                        
                        hash:           hash,
                        thumbnail:      thumbnail,
                        
                        ext:            ext,
                        title:          title,
                        format:         "",
                        
                        downloadName:   title,
                        displayName:    title,
                        displayLabel:   ft,
                        filename:       file_name,

                        playlist:      {  video: { url:  url,  ext:  ext, hash: hash   },    },
                        
                        size:           0,
                        type:           "video",
                        metod:          "playlist",
                        source:         "Twitter",
                        quality:        q,
                        
                        group:          groupMedia,
                        order:          q,
                    });    

                mediaFound = true;
        
            }   


        }


        // ==================================================================== 
        this.getMedia = function( media ){
            
            return media;
        }

    };
    
    this.Twitter = new Twitter();
    
}).apply( fvdDownloader.Media );
