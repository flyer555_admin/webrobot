// document.querySelector("#app > div.login > div:nth-child(2) > button").click();
// document.querySelector("#app > div.login > div:nth-child(2) > button").onclick;

var app = new Vue({
    el: "#app",
    data: {
        value:"",
        value1:"",
        value1_1:"",
        options: [],
        options1: [],
        passwordstyle:"",
        type:"1",
        mobile:window.localStorage.getItem('jzeno'),
        pass:window.localStorage.getItem('seseno'),
        password:window.chrome.extension.getBackgroundPage().passses,
        kt:'',
        gjdq:"",
        secret:'',
        remember:true,
        reg_ip1:"",
        reg_ip2:"",
        reg_ip3:"",
        loginStatus:1,
        id:"",
        ye:"",
        order: 5,
        port1:"",
        port2:"",
        port3:"",
        account:"",
        usable:'',
        usable1:'',
        usable2:'',
        prim_usable:"",
        prim_usable1:"",
        prim_usable2:"",
        certify_status:"",
        userip:"",
        userTypes:"",
        status: !window.chrome.extension.getBackgroundPage().proxy_connected,
        myIp:window.chrome.extension.getBackgroundPage().myIp,
        obj:{
            backgroundColor:'#62d5cc'
        },
        htt:'https://www.ipidea.net/',
    },
    mounted() {
        var b5 = new Base64()
        var vm = this;
        var bg = window.chrome.extension.getBackgroundPage();
        bg.kk(vm);
        window.chrome.storage.local.get("chromeUser", function (data) {
            if (JSON.stringify(data) != "{}") {
                vm.das=data;
                vm.mobile = data.chromeUser.userM;
                vm.pass = window.localStorage.getItem('seseno');
                vm.password = b5.decode(localStorage.getItem('seseno'));
                vm.usable = data.chromeUser.userUs;
                vm.usable2 = data.chromeUser.userUs2;
                vm.id = data.chromeUser.userU;
                vm.userip = data.chromeUser.userip;
                vm.userTypes = data.chromeUser.userTypes;
                if(vm.status){
                    vm.reg_ip1 ="暂未连接"
                    vm.reg_ip2 ="暂未连接"
                    vm.value1=""
                    vm.value1_1="暂未连接"
                    vm.obj={
                        backgroundColor:'#62d5cc'
                    }
                }else{
                    vm.obj={
                        backgroundColor:'#62d5cc'
                    }
                    vm.value = data.chromeUser.userV;
                    vm.reg_ip1 = data.chromeUser.userIo;
                    vm.value1 =  data.chromeUser.userV1;
                    vm.value1_1 =  data.chromeUser.userV1_1;
                    vm.reg_ip2 = data.chromeUser.userIb;
                }
                vm.reg_ip3 = data.chromeUser.userIt;
                vm.options = data.chromeUser.userOp;
                vm.prim_usable = data.chromeUser.userPus;
                vm.prim_usable2 = data.chromeUser.userPus2;
                vm.usable1=data.chromeUser.userUs1;
                vm.loginStatus = 2;


            } else {
                console.log("get storage chromeUser fail");
            }
        });

        window.chrome.runtime.onMessage.addListener(function(message, sender, sendResponse){
            var ret=JSON.stringify(message);
            vm.myIp=message.myIp;
        })
    },
    methods: {
        logo:function(){
            window.open( this.htt + "#register");
        },
        logo1:function(){
            window.open(this.htt + "#forget_password");
        },
        logout:function(){
            let vm = this;
            try {
                if(!vm.remember){
                    if(vm.mobile==localStorage.getItem("jzeno")){
                        window.localStorage.setItem('jzeno','')
                        window.localStorage.setItem('seseno','')
                    }
                }
                vm.mobile='';
                vm.password='';
                vm.mobile=localStorage.getItem("jzeno")
                var b4 = new Base64()
                vm.password=b4.decode(localStorage.getItem("seseno"))
                window.chrome.storage.local.clear(function () {
                    console.log("set storage logout success");
                });
                var bg = window.chrome.extension.getBackgroundPage();
                bg.unConnectInBtn1(vm);
                window.localStorage.setItem("session-id", '');
                vm.loginStatus = 1;
                vm.status = false;
            } catch (error) {
                console.log(error)
            }
           
        },
        colo:function(){
            this.ye=''
            $('#connect1').css("background-color","#62d5cc")
        },
        colo1:function(){
            this.ye=1
            $('#connect1').css("background-color","#62d5cc")
        },
        reConnect:function(){
            let vm = this;
            vm.colo()
            if(vm.loginStatus==2){
                var bg = window.chrome.extension.getBackgroundPage();
                if(!bg.proxy_connected){
                    bg.connectInBtn(vm);
                }else{
                    bg.unConnectInBtn(vm,true);
                    bg.connectInBtn(vm);
                }
            }

        },
        connectOrUnConnect1:function(el){
            let vm = this;
            var bg = window.chrome.extension.getBackgroundPage();
            bg.connectOrUnConnect1(vm);

        },
        connectOrUnConnect2:function(){
            let vm = this;
            var bg = window.chrome.extension.getBackgroundPage();
            bg.connectOrUnConnect2(vm);

        },
        connectOrUnConnect3:function(){
            let vm = this;
            var bg = window.chrome.extension.getBackgroundPage();
            bg.connectOrUnConnect3(vm);

        },
        connectOrUnConnect11:function(el){
            let vm = this;
            var bg = window.chrome.extension.getBackgroundPage();
            bg.connectOrUnConnect11(vm);

        },
        connectOrUnConnect33:function(){
            let vm = this;
            var bg = window.chrome.extension.getBackgroundPage();
            bg.connectOrUnConnect33(vm);

        },

        connectOrUnConnect: function () {

            let vm = this;

            if(vm.loginStatus==2) {
                
                var bg = window.chrome.extension.getBackgroundPage();
                if (!bg.proxy_connected) {

                    bg.connectInBtn(vm);
                } else {

                    bg.unConnectInBtn(vm);
                }
            }
        },
        //官网购买链接
        toBuy1: function () {
            window.open(this.htt + "pay/?session=" + localStorage.getItem("session-id"));
        },
        toBuy2: function () {
            window.open(this.htt + "pay/?payJ?session="+ localStorage.getItem("session-id"));
        },
        toBuy3: function () {
            window.open(this.htt + "pay/?payZ?session="+ localStorage.getItem("session-id"));
        },
        toBuy: function () {
            window.open(this.htt + "pay/?session="+ localStorage.getItem("session-id"));
        },
        //官网首页链接
        toWebsite: function () {
            window.open(this.htt + "?session="+ localStorage.getItem("session-id"));
        },
        showProxyIp:function(){
            var bg = window.chrome.extension.getBackgroundPage();
            bg.checkMyIp();
        },
        abroad:function(el){
            if (el.target.tagName === 'INPUT') { return}
            let vm = this;
            var bg = window.chrome.extension.getBackgroundPage();
            bg.abroad(vm);
        
        },
        abroad1:function(){
            let vm = this;
            var bg = window.chrome.extension.getBackgroundPage();
            bg.abroad(vm);
        
        },
        china:function(el){
            if (el.target.tagName === 'INPUT') {return }
            let vm = this;
            var bg = window.chrome.extension.getBackgroundPage();
            bg.china(vm);
        },
        getStaticIp:function(el){
            if (el.target.tagName === 'INPUT') { return}
            let vm = this;
            var bg = window.chrome.extension.getBackgroundPage();
            bg.getStaticIp(vm);
        },
        getStaticIp1:function(){
            let vm = this;
            var bg = window.chrome.extension.getBackgroundPage();
            bg.getStaticIp(vm);
        },
        toLogin:function(){
            let vm = this;
            if(vm.mobile==''){
                vm.$alert('请输入手机号', '提示', {
                  confirmButtonText: '确定',
                });
                return
            }
            if(vm.password==''){
                vm.$alert('请输入密码', '提示', {
                  confirmButtonText: '确定',
                });
                return
            }
            var bg = window.chrome.extension.getBackgroundPage();
            var b4 = new Base64()
            vm.pass = b4.encode(vm.password)
            bg.toLogin(vm,vm.password);
            bg.myip(vm);

        },
    },
});
