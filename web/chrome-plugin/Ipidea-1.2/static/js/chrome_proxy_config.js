function getConfig(proxy_host,proxy_port){
    var config={
        mode :"fixed_servers",
        rules: {
            singleProxy: {
                scheme: "http",
                host: proxy_host,
                port: proxy_port
            },
            bypassList: ["*.ipidea.net","xiaoer.321174.com:2333","tiqu.linksocket.com:81"]
        }
    }

    return config;
}
