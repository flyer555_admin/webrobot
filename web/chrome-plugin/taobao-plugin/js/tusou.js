!
    function() {
        var a = "https://tusou.vvic.com",
            r = "https://www.vvic.com";
        chrome.contextMenus.create({
            title: "通过搜款网搜索图片",
            contexts: ["image"],
            onclick: function(r, e) {
                var t = o(r.srcUrl),
                    c = "/api/uploadImage?ck=plug&url=" + encodeURIComponent(t),
                    n = new XMLHttpRequest;
                n.open("POST", a + c, !0),
                    n.onreadystatechange = function() {
                        if (4 == n.readyState) if (200 == n.status) {
                            var e = JSON.parse(n.responseText),
                                t = e.code;
                            if (200 == t) {
                                var c = e.city || "gz",
                                    o = a + "/list/" + e.appMd5 + "?f=plug&searchCity=" + c;
                                s(o, r.pageUrl),
                                    chrome.tabs.create({
                                        url: o
                                    })
                            } else 202 == t || 203 == t ? alert("您的操作太过频繁，请稍后重试") : alert("操作失败，请重试")
                        } else {
                            if (504 == n.status) return alert("您操作的频率过快，请稍后重试。"),
                                !1;
                            if (403 == n.status) return alert("您操作的频率过快，请联系客服。"),
                                !1;
                            if (509 == n.status) return alert("您操作的频率过快，请稍后重试。"),
                                !1;
                            if (406 == n.status) return alert("您操作的频率过快，请登录搜款网再进行操作。"),
                                chrome.tabs.create({
                                    url: "https://www.vvic.com/login.html?f=tusou"
                                }),
                                !1;
                            alert("操作失败，请重试")
                        }
                    },
                    n.send()
            }
        }),
            chrome.contextMenus.create({
                title: "通过搜款网搜索内容",
                contexts: ["selection"],
                onclick: function(e, t) {
                    var c = e.selectionText,
                        o = r + "/redirect/1/search/index.html?q=" + encodeURIComponent(c);
                    chrome.tabs.create({
                        url: o
                    })
                }
            }),
        chrome.cookies || (chrome.cookies = chrome.experimental.cookies),
            chrome.cookies.get({
                    url: r,
                    name: "uid"
                },
                function(e) {
                    null == e || console.log(e)
                });
        var o = function(e) {
                var t = e;
                if (/^(https?:)?\/\/\S+\.alicdn.com.*\.(jpe?g|png).*/i.test(t)) {
                    var c = (t = (t = t.replace(/_\.webp/g, "")).replace(/q\d+\.jpg/, ".jpg")).match(/_[0-9]+x[0-9]+\.jpg/) || [];
                    if (0 < c.length)~~c[0].replace(/_|(.jpg)/g, "").split("x")[0] < 300 && (t = t.replace(/_[0-9]+x[0-9]+\.jpg/, ""))
                }
                return 0 == t.indexOf("//") && (t = location.protocol + t),
                    t
            },
            s = function(e, t) {
                e = encodeURIComponent(e);
                var c = t,
                    o = (new Date).getTime(),
                    r = (new Image(1, 1), "https://vda.vvic.com/collect/?s=plug&t=ck&db=tusou&di=0&da=tusou_btn&p=" + c + "&ts=" + o + "&l=" + e),
                    n = new XMLHttpRequest;
                n.open("GET", r, !0),
                    n.send()
            }
    } ();
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    debugger;
    if(request.action=='event2getImgFile'){//获取图片接口
        event2getImgFile(request.param.uuid,sendResponse);
    }else if(request.action =='imgurl'){// 上传图片接口
        soutbByUrl(request.imgurl);
    }


    return true;
});
var oui ={};
ppt = function(sid) {
    i = "https://s.taobao.com/search?app=imgsearch&tfsid=" + sid,
        window.open(i, '_blank')
},
    soutbByUrl = function(url, callback) {
        var newImg = document.createElement('img');
        newImg.setAttribute('crossOrigin', "Anonymous");
        newImg.src = url;
        newImg.onload = function() {
            var imgName = newImg.src.match(/.*\/(.*?.(jpg|jpeg|gif|png))/);
            if (imgName) {
                imgName = imgName[1]
            } else {
                imgName = newImg.src
            }
            imgToBlob(newImg, 'image/jpeg',
                function(blob) {
                    blob.name = imgName;
                    soutbByFile(blob, callback)
                })
        }
    },
    callbacks={},
    (function (_) {
        // Private array of chars to use
        var CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');

        _.uuid = function (len, radix) {
            var chars = CHARS, uuid = [], i;
            radix = radix || chars.length;

            if (len) {
                // Compact form
                for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random() * radix];
            } else {
                // rfc4122, version 4 form
                var r;

                // rfc4122 requires these characters
                uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
                uuid[14] = '4';

                // Fill in random data.  At i==19 set the high bits of clock sequence as
                // per rfc4122, sec. 4.1.5
                for (i = 0; i < 36; i++) {
                    if (!uuid[i]) {
                        r = 0 | Math.random() * 16;
                        uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
                    }
                }
            }

            return uuid.join('');
        };

        _.uuidFast = function () {
            var chars = CHARS, uuid = new Array(36), rnd = 0, r;
            for (var i = 0; i < 36; i++) {
                if (i == 8 || i == 13 || i == 18 || i == 23) {
                    uuid[i] = '-';
                } else if (i == 14) {
                    uuid[i] = '4';
                } else {
                    if (rnd <= 0x02) rnd = 0x2000000 + (Math.random() * 0x1000000) | 0;
                    r = rnd & 0xf;
                    rnd = rnd >> 4;
                    uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
                }
            }
            return uuid.join('');
        };


        /**
         * 获取整型uuid
         */
        _.getUUIDLong = function () {
            //18 位防止后台转为long 超过 long 的最大值
            return oui.uuid(18, 10);
        };

        /**
         * 获取字符串类型的uuid
         */
        _.getUUIDString = function () {
            return oui.uuidFast();
        };
    })(oui);
    soutbByFile = function(imgFile, callback) {
        var uuid =oui.getUUIDLong();
        callbacks[uuid]=imgFile;
        window.open('https://s.taobao.com/?win_uuid='+uuid, '_blank');

    },
    event2getImgFile = function(uuid, callback) {
        var f= new FileReader();
        f.onload = function(e){
            callback&&callback(e.target.result);
            delete  callbacks[uuid];
        };
        f.readAsDataURL(callbacks[uuid]);
    },
    imgToBlob = function(img, mime, callback) {
        var canvas = document.getElementById("canvas-soutu");
        if (!canvas) {
            canvas = document.createElement('canvas');
            canvas.id = 'canvas-soutu';
            canvas.style.display = 'none';
            document.body.appendChild(canvas)
        }
        var ctx = canvas.getContext('2d');
        canvas.width = img.naturalWidth;
        canvas.height = img.naturalHeight;
        ctx.drawImage(img, 0, 0);
        canvas.toBlob(function(blob) {
                callback(blob)
            },
            mime, 1)
    },
    getMimeFromUrl = function(url) {
        var imgName = getImgnameFromUrl(url);
        var ext = getExtFromName(imgName);
        if (ext) {
            return getMimeFromExt(ext)
        } else {
            return ''
        }
    },
    getImgnameFromUrl = function(url) {
        return url.match(/.*\/(.*?)(\?|$)/)[1]
    },
    getExtFromName = function(name) {
        var rtn = name.match(/.*\.(.*)/);
        if (rtn) {
            return rtn[1]
        } else {
            return ''
        }
    },
    getMimeFromExt = function(ext) {
        var mime = 'image/';
        if (ext == 'jpg') {
            ext = 'jpeg'
        }
        return mime + ext
    };