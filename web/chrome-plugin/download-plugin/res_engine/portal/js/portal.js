!(function (win, $) {
    /**
     * com.oui.portal.PortalController
     * 首页PortalController
     * **/
    var PortalController = {
        "package": "com.oui.portal",
        "class": "PortalController",
        data:{},
        getCurrentUrl:function(){
            var contextPath = oui.getContextPath();
            var loadUrl = oui.router.getCurrentLoadUrl();
            if(loadUrl.indexOf(contextPath)>=0){
                loadUrl = loadUrl.substring(contextPath.length);
            }
            return loadUrl;
        },
        inIframe:function(){
            var flag = false;
            if(window.top && (window.top !=window)){
                flag = true;
            }
            return flag;

        },
        /***
         * 登录中国知网
         */
        login4cnki:function(){//&lbb_userId=***&lbb_tokenId=****
            var me = this;
            me.showPluginLoading();
            me.getWebsiteUser(1,function(user,pwd,msg){
                if(!user){ //登录失败
                    me.hidePluginLoading();
                    alert(msg||"登录失败");

                    return;
                }
                me.getUserInfo(function(userInfo){
                    var params = $.param({
                        lbb_userId:userInfo.userId,
                        lbb_tokenId:userInfo.tokenId,_t:new Date().getTime()
                    });
                    var targetUrl ='http://login.gduf.vpn358.com/index.php?r=front/index&go_to_login=cnki&' + params;
                    var tempParam ={
                        url:targetUrl
                    };

                    var url = me.data.serviceAddress+'iframe4website.html?'+ $.param(tempParam);
                    window.open(url,'_blank');
                    setTimeout(function(){
                        me.hidePluginLoading();
                    },1000);
                });

            });


            //document.getElementById('location4websitediv').innerHTML='<iframe src="'+url+'"></iframe>';
        },
        /***
         * 登录北大法宝
         */
        login4pkulaw:function(){
            //北大法宝
            var me = this;
            me.showPluginLoading();
            me.getWebsiteUser(2,function(user,pwd,msg){
                if(!user){ //登录失败

                    me.hidePluginLoading();
                    alert(msg||"登录失败");
                    return;
                }
                me.getUserInfo(function(userInfo){
                    var params = $.param({
                        lbb_userId:userInfo.userId,
                        lbb_tokenId:userInfo.tokenId,
                        _t:new Date().getTime()
                    });
                    var targetUrl = 'https://www.pkulaw.com/?go_to_login=pkulaw&' + params;
                    var tempParam ={
                        url:targetUrl
                    };
                    var url = me.data.serviceAddress+ 'iframe4website.html?'+ $.param(tempParam);
                    setTimeout(function(){
                        me.hidePluginLoading();
                    },1000);
                    window.open(url,'_blank');
                });

            });


            //document.getElementById('location4websitediv').innerHTML='<iframe src="'+url+'"></iframe>';
        },
        /***
         * 登录威科先行
         */
        login4wkxx:function(){
            //威科先行

            var me = this;
            me.showPluginLoading();
            me.getWebsiteUser(3,function(user,pwd,msg){
                if(!user){ //登录失败

                    me.hidePluginLoading();
                    alert(msg||"登录失败");
                    return ;
                }
                me.getUserInfo(function(userInfo){
                    var params = $.param({
                        lbb_userId:userInfo.userId,
                        lbb_tokenId:userInfo.tokenId,
                        _t:new Date().getTime()
                    });
                    me.showPluginLoading();
                    var targetUrl = 'https://law.wkinfo.com.cn/?go_to_login=wkxx&' + params;

                    var tempParam ={
                        url:targetUrl
                    };
                    var url = me.data.serviceAddress+'iframe4website.html?'+ $.param(tempParam);
                    setTimeout(function(){
                        me.hidePluginLoading();
                    },1000);
                    window.open(url,'_blank');
                });
            });
            //document.getElementById('location4websitediv').innerHTML='<iframe src="'+url+'"></iframe>';

        },
        getWebsiteUser:function(webNum,callback)
        {
            this.getUserInfo(function(userInfo){
                var urls = userInfo.urls || {};
                var url = urls.queryWebsiteOne ||"";
                if(!url){
                    return ;
                }
                oui.ajax({
                    url: url,
                    postParam:{
                        webNum:webNum,
                        userId:userInfo.userId||'',
                        cellphone:userInfo.cellphone||''
                    },
                    callback:function(res)
                    {
                        if(res&&res.success){
                            var user = res.data||{};
                            var name = user.webUser||"";
                            var pwd = user.webPwd||"";
                            callback&&callback(name,pwd);
                        }else{
                            callback&&callback(null,null,res.msg);
                            //提醒错误信息
                        }
                    }
                });
            });
        },

        /**
         * 绑定中国知网的事件
         */
        bindEvents4cnki:function(){
            var me = this;
            //登录中国知网的事件绑定
            //登录
            //http://login.gduf.vpn358.com/index.php?r=front/index&toLogin=cnki
            var toLogin = oui.getParam('go_to_login'); //去登录页
            var href = location.href;
            //lbb_userId=***&lbb_tokenId=****
            var userId = oui.getParam('lbb_userId');
            var tokenId = oui.getParam('lbb_tokenId');
            // 调用api 获取 当前官网的 用户账号信息;
            var flag = false;
            if(href.indexOf('login.gduf.vpn358.com')>-1 ){ // 中国知网vpn 登录入口路径
                if(toLogin=='cnki'){ // 中国知网
                   flag = true;
                }
            }else{
                //如果中国知网则跳转
                if(href.indexOf("cnki.com")>-1 || (href.indexOf("cnki.net")>-1 && (href.indexOf("cnki.net.")<0))){
                    window.location.href = 'http://h-s.www.cnki.net.gduf.vpn358.com/';//跳转到代理官网
                    return;
                }
            }
            if(!flag){
                return ;
            }
            this.getWebsiteUser(1,function(user,pwd)
            {
               me.getWebsiteUser4cnki(user,pwd);
            });

        },
        showPluginLoading:function(){
          $('.webrobot-plugin-sidebar').addClass('plugin-loading');
        },
        hidePluginLoading:function(){
            $('.webrobot-plugin-sidebar').removeClass('plugin-loading');
        },
        getWebsiteUser4cnki:function(user,pwd)
        {
            //需要登出再登入
            //默认用户密码写死
            var toLogin = oui.getParam('go_to_login'); //去登录页
            var href = location.href;
            //lbb_userId=***&lbb_tokenId=****
            var userId = oui.getParam('lbb_userId');
            var tokenId = oui.getParam('lbb_tokenId');
            // 调用api 获取 当前官网的 用户账号信息;
            var flag = false;
            if(href.indexOf('login.gduf.vpn358.com')>-1 ){ // 中国知网vpn 登录入口路径
                if(toLogin=='cnki'){ // 中国知网
                    flag = true;
                }
            }
            if(!flag){
                return ;
            }
            var me =this;
            var $user= $('.J_username');
            var $pwd = $('.J_password');
            if($user&&$user.length){
                me.showPluginLoading();
                $user.val(user);
                $pwd.val(pwd);
                //autocomplete="off"
                $pwd.attr('readonly','readonly');
                $pwd.attr('autocomplete','off');
                $pwd.css('display','none');
                $pwd.attr('type','text');
                var href = $('.J_login_button').attr('href');
                href = href +'&go_to_login='+toLogin;

                oui.storage.set('download.file.plugin.lastLogin',new Date().getTime()+"");
                //用户名密码输入
                $('.J_login_button')[0].click();

            }else{
                var lastLogin = oui.storage.get('download.file.plugin.lastLogin')||"0";
                var num = Number(lastLogin);
                var curr = new Date().getTime();
                if(curr-num>1000*10){//十秒钟之后退出
                    me.showPluginLoading();
                    oui.loadUrl({url:'http://login.gduf.vpn358.com/index.php?r=front/logout',callback:function(){
                        //回调后转向登录
                        location.href='http://login.gduf.vpn358.com/index.php?r=front/index&go_to_login=cnki';
                    }});
                }else{//进入登录页面
                    me.hidePluginLoading();
                    //进入中国知网
                    //location.href ='http://h-s.www.cnki.net.gduf.vpn358.com';
                    window.top.postMessage({
                        cmd:'cmd4locationPage',
                        param:{
                            url:'http://h-s.www.cnki.net.gduf.vpn358.com'
                        }
                    },'*');
                }
            }
        },
        /**
         * 绑定北大法宝事件
         */
        bindEvents4pkulaw:function(){
            var toLogin = oui.getParam('go_to_login'); //去登录页
            var href = location.href;

            var flag = false;
            if((href.indexOf('https://www.pkulaw')>-1 ) || (href.indexOf('https://login.pkulaw')>-1)){
                if(toLogin=='pkulaw'){ // 北大法宝
                    flag = true;
                }
            }
            if(!flag){
                return;
            }
            var me = this;
            this.getWebsiteUser(2,function(user,pwd)
            {
                me.getWebsiteuser4pkulaw(user,pwd);
            });
        },
        getWebsiteuser4pkulaw:function(user,pwd)
        {var toLogin = oui.getParam('go_to_login'); //去登录页
            var href = location.href;

            var flag = false;
            if((href.indexOf('https://www.pkulaw')>-1 ) || (href.indexOf('https://login.pkulaw')>-1)){
                if(toLogin=='pkulaw'){ // 北大法宝
                    flag = true;
                }
            }
            if(!flag){
                return;
            }

            var me = this;
            if(href.indexOf('https://login.pkulaw')>-1){ //登录页面
                var $user= $('#inputUserName');
                var $pwd = $('#inputPwd');
                if($user&&$user.length){
                    me.showPluginLoading();
                    $user.val(user);
                    $pwd.attr('type','text');
                    $pwd.attr('readonly','readonly');
                    $pwd.attr('autocomplete','off');
                    $pwd.css('display','none');
                    $pwd.val(pwd);
                    var href = $('.J_login_button').attr('href');
                    href = href +'&go_to_login='+toLogin;
                    oui.storage.set('download.file.plugin.lastLogin',new Date().getTime()+"");
                    //用户名密码输入
                    $('#loginByUserName')[0].click();

                    setTimeout(function(){
                        window.top.postMessage({
                            cmd:'cmd4locationPage',
                            param:{
                                url:'https://www.pkulaw.com'
                            }
                        },'*');
                    },1000);

                }
            }else{//非登录页面 处理
                //需要登出再登入
                //logout/?ReturnUrl=https://www.pkulaw.com/
                var $logout = $('.exit');
                if($logout&&$logout.length){ //已经登录，需要通过判断做出退出登录逻辑
                    var url ='https://www.pkulaw.com/logout/?ReturnUrl=https://www.pkulaw.com/';
                    var lastLogin = oui.storage.get('download.file.plugin.lastLogin')||"0";
                    var num = Number(lastLogin);
                    var curr = new Date().getTime();
                    if(curr-num>1000*10){//十秒钟之后退出
                        me.showPluginLoading();
                        //退出重新登录
                        oui.loadUrl({url:url,callback:function(){
                            //回调后转向登录
                            location.href='https://login.pkulaw.com/?ReturnUrl=https%3a%2f%2fwww.pkulaw.com%2f&go_to_login=pkulaw';
                        }});
                    }else{
                        me.hidePluginLoading();
                        window.top.postMessage({
                            cmd:'cmd4locationPage',
                            param:{
                                url:'https://www.pkulaw.com'
                            }
                        },'*');
                    }
                }else{ //未登录，进入登录页面
                    //手动登录
                    location.href='https://login.pkulaw.com/?ReturnUrl=https%3a%2f%2fwww.pkulaw.com%2f&go_to_login=pkulaw';
                }
            }

        },
        /**
         * 绑定威科先行事件
         */
        bindEvents4wkxx:function(){
            var toLogin = oui.getParam('go_to_login'); //去登录页
            var href = location.href;
            var pathName = location.pathname;

            var flag = false;
            if((href.indexOf('https://law.wkinfo.com.cn/')>-1 ) || (href.indexOf('www.wkinfo.com.cn')>-1)){ // 威科先行
                if(toLogin=='wkxx'){ // 威科先行
                    // 当前首页就是 登录所在页面
                    if(pathName =='/'){
                        flag = true;
                    }
                }

            }
            if(!flag){
                return ;
            }
            var me = this;
            this.getWebsiteUser(3,function(user,pwd)
            {
                me.getWebsiteUser4wkxx(user,pwd);
            });
        },
        //监听 窗体命令
        cmd4testNext:function(param){
            /**
             *  go_to_login:'wkxx4test',
             test_user:nextUser.user,
             test_pwd:nextUser.pass,
             test_index:nextIdx
             */
            if(param.go_to_login =='wkxx4test'){//测试威科先行的下一个登录账户
                location.href="https://law.wkinfo.com.cn/?"+ $.param(param);
            }

        },

        /** 测试威科先行页面 */
        test4wkxx:function(){
            var user = oui.getParam('test_user'),pwd=oui.getParam('test_pwd');
            var test_index = oui.getParam('test_index')||"";
            if(test_index){
                test_index = Number(test_index);
            }
            var toLogin = oui.getParam('go_to_login'); //去登录页
            var href = location.href;
            var pathName = location.pathname;
            var flag = false;
            if((href.indexOf('https://law.wkinfo.com.cn/')>-1 ) || (href.indexOf('www.wkinfo.com.cn')>-1)){ // 威科先行
                if(toLogin=='wkxx4test' || toLogin=='wkxx4testCheck' || toLogin =='wkxx4toTestCheck'){ // 威科先行
                    // 当前首页就是 登录所在页面
                    if(pathName =='/'){
                        flag = true;
                    }
                }
            }
            if(!flag){
                return ;
            }


            //登录页面
            var me = this;
            var $user= $('#login-username');
            var $pwd = $('#login-password');
            var userParam = $.param({
                test_user:user,
                test_pwd:pwd,
                test_index:test_index
            });
            if(toLogin =='wkxx4toTestCheck'){//前往测试校验页面
                window.setTimeout(function(){
                    window.top.postMessage({
                        cmd:'cmd4locationPage',
                        param:{
                            url:'https://law.wkinfo.com.cn/?go_to_login=wkxx4testCheck&'+userParam
                        }
                    },'*');
                },500);
                return;
            }
            if(toLogin=='wkxx4test'){ //登录入口
                if($user&&$user.length){
                    //判断是否已经登录
                    var isLogin = $('.rite').text().indexOf('个人中心')>-1;
                    //已经登录
                    if(!isLogin){//没有登录，去登录 //登录TODO  存在问题
                        $user.val(user);
                        $pwd.val(pwd);
                        var $button =$('.btn.btn-block.btn-submit') ;
                        //用户名密码输入
                        $button[0].click();

                        //登录成功判断

                        window.setTimeout(function(){
                            window.top.postMessage({
                                cmd:'cmd4locationPage',
                                param:{
                                    url:'https://law.wkinfo.com.cn/?go_to_login=wkxx4toTestCheck&'+userParam
                                }
                            },'*');
                        },500);
                    }else{//当前已经登录需要先退出再进入
                        var $exit = $('.btn-link'); //btn-link
                        $exit[0].click(); //退出登录
                        setTimeout(function(){
                            location.href="https://law.wkinfo.com.cn/?go_to_login=wkxx4test&"+userParam;
                        },500);
                    }
                }
            }else{ //登录成功校验
                if($user&&$user.length){
                    //判断是否已经登录
                    var isLogin = $('.rite').text().indexOf('个人中心')>-1;
                    //已经登录
                    window.opener&&window.opener.postMessage({
                        cmd:'cmd4testCallback',
                        param:{
                            go_to_login:'wkxx4test',
                            test_user:user,
                            test_pwd:pwd,
                            canUse:isLogin,//登录成功则为可用
                            test_index:test_index
                        }
                    },'*');
                }
            }
        },
        getWebsiteUser4wkxx:function(user,pwd)
        {
            var toLogin = oui.getParam('go_to_login'); //去登录页
            var href = location.href;
            var pathName = location.pathname;

            var flag = false;
            if((href.indexOf('https://law.wkinfo.com.cn/')>-1 ) || (href.indexOf('www.wkinfo.com.cn')>-1)){ // 威科先行
                if(toLogin=='wkxx'){ // 威科先行
                    // 当前首页就是 登录所在页面
                    if(pathName =='/'){
                        flag = true;
                    }
                }

            }
            if(!flag){
                return ;
            }

            //登录页面

            var me = this;
            var $user= $('#login-username');
            var $pwd = $('#login-password');
            if($user&&$user.length){
                //判断是否已经登录
                var isLogin = $('.rite').text().indexOf('个人中心')>-1;
                //已经登录
                if(!isLogin){//没有登录，去登录 //登录TODO  存在问题
                    me.showPluginLoading();

                    //$pwd.attr('type','text');
                    $pwd.attr('readonly','readonly');
                    $pwd.attr('autocomplete','off');
                    $pwd.css('display','none');
                    $user.val(user);
                    $pwd.val(pwd);
                    var $button =$('.btn.btn-block.btn-submit') ;
                    oui.storage.set('download.file.plugin.lastLogin',new Date().getTime()+"");
                    //用户名密码输入
                    $button[0].click();

                    window.setTimeout(function(){
                        window.top.postMessage({
                            cmd:'cmd4locationPage',
                            param:{
                                url:'https://law.wkinfo.com.cn/'
                            }
                        },'*');
                    },500);
                }else{
                    me.showPluginLoading();
                    //已经登录，判断交互时间
                    var $exit = $('.btn-link'); //btn-link
                    var lastLogin = oui.storage.get('download.file.plugin.lastLogin')||"0";
                    var num = Number(lastLogin);
                    var curr = new Date().getTime();
                    if(curr-num>1000*10){//十秒钟之后退出
                        //退出重新登录
                        $exit[0].click(); //退出登录
                        setTimeout(function(){
                            location.href="https://law.wkinfo.com.cn/?go_to_login=wkxx";
                        },1000);
                        //再重新登录
                    }else{
                        me.hidePluginLoading();
                        window.top.postMessage({
                            cmd:'cmd4locationPage',
                            param:{
                                url:'https://law.wkinfo.com.cn/'
                            }
                        },'*');
                    }
                }
            }
        },

        init:function(){
            var me = this;
            me.initRouter(); //这是 插件开发模式的router机制
            me.data.clickName=oui.browser.mobile?'tap':'click';
            this.refs = {};
            template.helper('PortalController',this);
            template.helper('oui',oui);
            oui.loadUrl = oui.loadUrl4ChromeExt; //插件开发 加载url的机制 需要通过插件的加载方式实现

            //获取当前人信息

            window.addEventListener("message", function(event) {
                var data = event.data;
                if(typeof data =='string'){
                    return;
                }
                if(typeof data =='object'){
                    //在本类中的方法负责接收消息处理

                    //处理api请求
                    if(data.cmd&&data.param&& data.callbackId&&(data.cmd=='cmd4loadUrlTopChromePlugin')){
                        var param = data.param;
                        var callbackId = data.callbackId;
                        var iframeId = data.iframeId;
                        if(!iframeId){
                            return ;
                        }
                        param.callback = function(res)
                        {
                            // 对目标window 发送消息
                            var contentWin = document.getElementById(iframeId).contentWindow;
                            contentWin&&contentWin.postMessage({
                                cmd:'cmd4loadUrlTopChromePluginResponse',
                                param:res,
                                callbackId:callbackId
                            },"*")
                        };
                        oui.loadUrl(param);
                    }else if(data.cmd&&data.param&&data.callbackId&&(data.cmd=='cmd4storage')){//获取localstorage，设置localstorage
                        // 对目标window 发送消息
                        var iframeId = data.iframeId;
                        if(!iframeId){
                            return ;
                        }
                        var contentWin = document.getElementById(iframeId).contentWindow;
                        var param = data.param;

                        if(param.key && (typeof param.value !='undefined')){//设置值
                            oui.storage4chromeExt.set(param.key,param.value,function(res){
                                contentWin&&contentWin.postMessage({
                                    cmd:'cmd4storageResponse',
                                    param:res,
                                    callbackId:data.callbackId
                                },"*")
                            });

                        }else if(param.key){//获取值
                            if(param.remove){
                                //删除值
                                oui.storage4chromeExt.remove(param.key,function(res){
                                    contentWin&&contentWin.postMessage({
                                        cmd:'cmd4storageResponse',
                                        param:res,
                                        callbackId:data.callbackId
                                    },"*")
                                });
                            }else{
                                //获取值并返回

                                oui.storage4chromeExt.get(param.key,function(res){
                                    contentWin&&contentWin.postMessage({
                                        cmd:'cmd4storageResponse',
                                        param:res,
                                        callbackId:data.callbackId
                                    },"*")
                                });

                            }
                        }else if(param.clear){ //清除数据
                            oui.storage4chromeExt.clear(function(res){
                                contentWin&&contentWin.postMessage({
                                    cmd:'cmd4storageResponse',
                                    param:res,
                                    callbackId:data.callbackId
                                },"*")
                            });
                        }




                    }else if(data.cmd&&data.param){ //处理当前登录人信息 //cmd4updateUserInfo
                        me[data.cmd]&&me[data.cmd](data.param);
                    }

                }
            }, false);
            if(me.data.webrobotState =="edit" || me.data.webrobotState =="runtime4preview" ){ //非运行态不执行 WebRobot自动任务
                if(me.data.webrobotState =="runtime4preview" ){
                    //var url = me.data.webAddress+'/webrobot-components/index.js';
                    var url = chrome.extension.getURL("webrobot-components/index.js");

                    url = oui.setParam(url,'sysId',me.data.sysId);
                    url = oui.setParam(url,'sysName',me.data.sysName);
                    url = oui.setParam(url,'_t',new Date().getTime());
                    oui.require([url]); //加载运行态资源
                }else{
                    //
                    me.loadAll(function(){
                        if(location.href.indexOf('iframe4website.html')>-1){
                            me.showPluginLoading();
                        }
                        //事件绑定
                        me.bindEvents4cnki();//绑定中国知网的登录事件
                        me.bindEvents4pkulaw();//绑定北大法宝登录事件
                        me.bindEvents4wkxx();//绑定威科先行
                        me.test4wkxx();//测试威科先行的事件监听
                    });
                }
            }

        },
        /*
         window.top.postMessage({
         cmd:'cmd4locationPage',
         param:{
         url:'https://law.wkinfo.com.cn/'
         }
         },'*');
         */
        cmd4locationPage:function(data,event){
            window.location.href= data.url;
        },
        //更新当前登录人信息
        cmd4updateUserInfo:function(data, event)
        {
            if(data && data.userId)
            {
                if(data.personRole ==1){ //普通会员宽度减半
                    $('#webrobot-plugin').css('width','60%')
                }else{
                    $('#webrobot-plugin').css('width','88%')
                }
                oui.storage4chromeExt.set('userInfo',oui.parseString(data));
                $('.webrobot-plugin-toggle').hide();
                $('.webrobot-plugin-toggle-loged').show();
            } else
            {
                oui.storage4chromeExt.remove('userInfo');
                $('.webrobot-plugin-toggle').show();
                $('.webrobot-plugin-toggle-loged').hide();
            }

        },

        loadAll:function(callback){
            var me = this;
            me.bindEvents();

            //追加元素到页面上
            var apiConfigUrl = chrome.extension.getURL("res_engine/portal/js/api-config.json");
            //加载api配置
            me.loadApiConfig(apiConfigUrl,function(){
                //加载portal页面
                var htmlUrl = chrome.extension.getURL("plugin-index.html");
                me.loadPortalHtml(htmlUrl,function(){
                    oui.router.push('tpl/welcome.vue.html');

                    //侧边按钮显示隐藏

                    me.getUserInfo(function(userInfo){
                        console.log('用户信息：');
                        console.log(userInfo);
                        if(userInfo && userInfo.userId)
                        {

                            if(userInfo.personRole ==1){ //普通会员宽度减半
                                $('#webrobot-plugin').css('width','50%')
                            }else{
                                $('#webrobot-plugin').css('width','88%')
                            }
                            $('.webrobot-plugin-toggle').hide();
                            $('.webrobot-plugin-toggle-loged').show();
                        } else
                        {
                            $('#webrobot-plugin').css('width','50%');
                            $('.webrobot-plugin-toggle').show();
                            $('.webrobot-plugin-toggle-loged').hide();
                        }
                        if(me.inIframe()){
                            $('.webrobot-plugin-toggle').hide();
                            $('.webrobot-plugin-toggle-loged').hide();
                        }
                        var $sysPanel = $('#webrobot-plugin .webrobot-sys');
                        var $eidt = $('#webrobot-plugin .webrobot-page-edit');
                        var $iframe = $('#webrobot-plugin-iframe');

                        if(userInfo && userInfo.userId)
                        {//已登录状态
                            $sysPanel.addClass('current');
                            $eidt.removeClass('current');

                            //personRole 1普通用户，2管理员，3推广员，4超级管理员
                            var personRole = userInfo.personRole;
                            if([1, 3].includes(personRole))
                            {
                                $iframe[0].src = me.data.webrobotPluginUserIndexUrl;
                            } else if([2, 4].includes(personRole))
                            {
                                $iframe[0].src = me.data.webrobotPluginAdminIndexUrl;
                            }

                        } else
                        {
                            $sysPanel.addClass('current');
                            $eidt.removeClass('current');

                            if(!$iframe[0].src){
                                $iframe[0].src = me.data.webrobotPluginUrl;
                            }
                        }

                        callback&&callback();
                    });
                })
            });
        },

        getUserInfo:function(callback){
            oui.storage4chromeExt.get('userInfo',function(value){
                callback&&callback(oui.parseJson(value||"{}"));
            })
        },

        /****
         * 加载portal页面
         * @param htmlUrl
         */
        loadPortalHtml:function(htmlUrl,callback){
            var me = this;
            oui.loadUrl({
                url: htmlUrl,
                subContentType:1, //取body中内容
                callback:function(html){
                    var render4html = template.compile(html);
                    var content = render4html(me.data);
                    $(document.body).append(content);
                    //$('#plugin-logo').attr('src', oui.getContextPath() + 'images/web-logo.png');

                    oui.parse({
                        container:'#webrobot-plugin',
                        callback:function(){
                            callback&&callback();
                        }
                    });
                }
            });
        },
        /**加载api配置 **/
        loadApiConfig:function(url,callback){
            var me = this;
            oui.loadUrl({
                url: url,
                subContentType:2, //取整个json内容
                callback:function(json){
                    var config = oui.util.eval(json);//获取配置对象
                    me.apiConfig  = config; //获取api配置
                    if(config.useLocalDB && config.dbConfigPath){//使用本地数据库, 并且要进行数据库配置
                        me.buildLocalApi();
                        //加载数据库配置，并 初始化数据库配置
                        me.loadDBConfig(config.dbConfigPath,function(){
                            //加载完db配置之后， 扩展构建api方法
                            callback&&callback();
                        });
                    }else{//api是调用后台服务
                        me.buildServerApi();
                        callback&&callback();
                    }
                }
            });

        },
        //构造本地api调用
        buildLocalApi:function(){
            var me = this;
            me.api = function(name,param,success,error){
                var path = me.apiConfig.api4LocalDB[name];
                me.api.run(path,param,success,error);
            };
            me.api.run = function(path,param,success,error){
                if(path){
                    var storePath= path.substring(0,path.lastIndexOf('.'));
                    var method = path.substring(path.lastIndexOf('.')+1);
                    var store = oui.util.eval(storePath);
                    store[method](param,function(res){ //回调返回
                        success&&success({
                            success:true,
                            data:res
                        });
                        if(NProgress){
                            NProgress.done();
                        }
                    },function(){
                        error&&error();
                        if(NProgress){
                            NProgress.done();
                        }
                    }); //固定三个参数
                }
            };
            oui.api = me.api;
        },
        //构造 远程api调用
        buildServerApi:function(){
            var me = this;
            //根据命名处理
            me.api = function(name,param,success,error){
                var path = me.apiConfig.api[name]||"";
                me.api.run(path,param,success,error);
            };
            //运行任意url
            me.api.run = function(path,param,success,error){
                if(path){
                    oui.postData(path,param,success,error);
                }
            };
            oui.api = me.api;
        },
        /**加载数据库配置 **/
        loadDBConfig:function(url,callback){
            var me = this;
            oui.loadUrl({
                url: url,
                subContentType:2, //取整个json内容
                callback:function(json){
                    var config = oui.util.eval(json);//获取配置对象
                    me.db = oui.buildDB(config ,callback);
                }
            });
        },
        getView:function(){
            return this;
        },
        //显示左侧布局面板
        event2showLayoutPanel:function(cfg){
            //webrobot-plugin-show
            var me = this;

            var $webrobotPlugin = $('#webrobot-plugin');
            if($webrobotPlugin.hasClass('webrobot-plugin-show')){
                me.hidePlugin();
            }else{
                me.showPlugin();
            }
        },

        //隐藏左侧布局面板
        event2hidelayoutPanel:function(cfg){
            var me = this;
            me.hidePlugin();
        },
        // 编辑机器学习模板与WebRobot设置切换
        event2toggle:function(cfg){
            var me = this;
            var $sysPanel = $('#webrobot-plugin .webrobot-sys');
            var $eidt = $('#webrobot-plugin .webrobot-page-edit');
            if($sysPanel.hasClass('current')){
                $sysPanel.removeClass('current');
                $eidt.addClass('current');
            }else{
                $sysPanel.addClass('current');
                $eidt.removeClass('current');
                var $iframe = $('#webrobot-plugin-iframe');
                if(!$iframe[0].src){
                    $iframe[0].src = me.data.webrobotPluginUrl;
                }
            }
        },
        //显示插件
        showPlugin:function(){
            $('#webrobot-plugin').addClass('webrobot-plugin-show');
            var me = this;
            me.changeIframeHeight();
        },

        //**** 显示登录状态侧边按钮 *****


        //iframe高度自适应
        changeIframeHeight:function(){
            var $iframe = $('#webrobot-plugin-iframe');
            if($iframe && $iframe.length){

                var height = $('#webrobot-plugin .webrobot-plugin-view.current').height();
                $iframe[0].height=height-2;
            }
        },
        //隐藏插件
        hidePlugin:function(){
            $('#webrobot-plugin').removeClass('webrobot-plugin-show');
        },
        _data:function(){
          return this;
        },
        /****
         * 绑定事件
         */
        bindEvents:function(){
            var me = this;
            //禁止元素被删掉
            var ids = (
            'webrobot-plugin-runtime,' +
            'webrobot-sys-runtime-header-info-tpl,' +
            'webrobot-page-runtime-content-tpl,' +
            'router-view-webrobot-comp-inner-tpl,' +
            'webrobot-plugin,' +
            'webrobot-sys-header-info-tpl,' +
            'webrobot-page-edit-content-tpl,' +
            'router-view-webrobot-plugin-inner-tpl').split(',');
            //绑定dom被删除事件
            $('body').on('DOMNodeRemoved', function(e) {
                if(!e.target){
                    return ;
                }
                if((!e.target.getAttribute) || (!e.target.getAttribute('id'))){
                    return;
                }
                var id = e.target.getAttribute('id');
                if(id&&ids.indexOf(id)>-1){
                    var outerHtml = e.target.outerHTML;
                    //被删除时又自动给它追加上，禁止被删除
                    $('body').append(outerHtml);
                    me.refershRouter();
                    console.log('元素[#'+id+']被删除了，已经被恢复了');
                }
            });
            window.addEventListener("message", function(event) {
                var data = event.data;
                if(typeof data =='string'){
                    return;
                }
                if(typeof data =='object'){
                    //在本类中的方法负责接收消息处理
                    if(data.cmd&&data.param){

                        console.log('webrobot-plugin:'+data.cmd);
                        console.log(data);
                        me[data.cmd]&&me[data.cmd](data.param,event);
                    }
                }
            }, false);
        },
        refershRouter :function(){
            var timer4router = this.timer4router;
            if(timer4router){
                clearTimeout(timer4router);
            }
            var me = this;
            this.timer4router = setTimeout(function(){
                oui.router.refresh();
                me.timer4router =null;
            });

        },

        //删除向导的命令
        cmd4removeGuide:function(data,event){
            oui.api("removeGuide",{id:data.guideId},function(){
                 //删除成功后转向
                oui.router.push('tpl/welcome.vue.html');
            },function(err){
                console.log(err);
            })
        },
        //保存自动任务与设计器关系
        cmd4savePageDesign:function(data,event){
            var guideId = data.guideId;
            var page =data.page;
            var me = this;
            if(this.data.guide.id ==guideId){
                this.data.guide.pageDesignId = page.id;
                this.data.guide.pageDesign = page;
                oui.api("saveGuide",this.data.guide, function(res){
                    if(res.success){ //保存成功

                        var iframeId = me.data.iframeId4pageDesign; //获取当前设计器所在iframeID
                        document.getElementById(iframeId).contentWindow.postMessage({
                            cmd:'cmd4saveCallbackSuccess',//前去触发 dom拾取
                            param:{
                                message:'保存成功'
                            }
                        },'*');//跨域处理

                    }else{
                    }
                },function(res){
                });
            }
        },
        //监听 来自模型设计器中前去拾取元素选择器的方法
        cmd4go2pickSelector:function(data,event){
            var me = this;

            me.data.pageDesigner = {
                controlId:data.controlId
            };
            me.hidePlugin();

            //发送命令，执行开始拾取元素操作
            (typeof chrome.app.isInstalled !='undefined') && (typeof chrome.app.isInstalled !='undefined') && chrome.runtime.sendMessage(
                {
                    cmd:'pickDom4control',
                    param:{ //传入当前参数
                        controlId:data.controlId //将当前控件id通过参数传递过去；等待选择回调处理该控件绑定选择器
                    }
                },
                function(response) {

                }
            );
        },
        //回填表单模型中控件绑定的选择器
        cmd2pickSelector4control:function(data,event){
            var me = this;
            var controlId = me.data.pageDesigner.controlId;
            var s =data.selector; //获取当前拾取到的元素
            //回填到当前控件上,通过iframe发送消息

            var iframeId = this.data.iframeId4pageDesign; //获取当前设计器所在iframeID
            document.getElementById(iframeId).contentWindow.postMessage({
                cmd:'cmd4pickDomFillBack',//前去触发 dom拾取
                param:{
                    controlId:controlId,
                    selector:s
                }
            },'*');//跨域处理

            me.showPlugin(); //显示插件

        },
        //通过消息传递 处理 元素定位器的拾取回填
        cmd4pickSelector:function(data,event){
            //监听元素拾取的消息
            var me = this;
            var selector = me.data.selector;
            var s=data.selector;
            for(var k in s){
                selector[k] =s[k];
            }
            //保存定位器
            if(selector.id){
                //保存定位器
                oui.api("saveSelector",selector,function(res){
                    if(res.success){ //保存成功
                        //保存成功
                        console.log('定位器保存成功');
                        //刷新页面
                        var url  = oui.router.getCurrentLoadUrl();
                        console.log(url);
                        oui.router.push(url,oui.router.query||{});
                    }else{
                        //保存失败
                        console.log('定位器保存失败');
                    }
                },function(res){
                    console.log('定位器保存失败');
                });
            }
            me.showPlugin();
        },
        /**
         * 处理接收的消息数据，根据cmd 处理相关消息
         * @param data
         * @param event
         */
        cmd4load:function(data,event){
            //加载 WebRobot中 系统列表、菜单列表 用于回填页面信息
            var me = this;
            var sysId = data.sysId; //系统id
            var menus =data.menus; //系统菜单列表
            var sys = data.sys; //系统列表
            var sysName ='';
            oui.findOneFromArrayBy(sys,function(item){
                if(item.id == sysId){
                    sysName = item.display;
                    return true;
                }
            });

            var lastSysId = me.data.sysId;
            var hasChange = false;
            if(lastSysId !=sysId){
                //系统切换
                hasChange = true;
            }
            me.data.sysId = sysId;
            me.data.menus = menus;
            me.data.sys = sys;
            me.data.sysName = sysName;
            oui.storage.set('webrobot.plugin.sysId',sysId);
            oui.storage.set('webrobot.plugin.sysName',sysName);
            //加载完成后，重新渲染 指定区域
            me.renderByLoadSys(hasChange);
            console.log(data);
        },
        renderByLoadSys:function(hasChange){
            oui.getById('webrobot-page-edit-content').render(); //编辑页面渲染
            oui.getById('webrobot-sys-header-info').render(); //系统信息头部渲染
            if(hasChange){
                oui.router.push('tpl/welcome.vue.html');
            }
        },
        /***
         * 获取当前数据
         * @returns {PortalController.data|{}}
         */
        getData:function(){
            var me = this;
            return me.data;
        },
        getIncludeData:function(){
            return oui.router.query;
        },
        /***
         * 删除某个script标签
         * @param urls
         */
        clearScriptTag:function(urls){
            if((!urls)||(!urls.length)){
                return ;
            }
            var scripts = $('script[data-url]');
            for(var i = 0; i < scripts.length; i++){//是否已加载
                var src =scripts[i].getAttribute('data-url') || scripts[i].src;
                src = src.substring(0,src.indexOf(oui_context.js_version));
                if(urls.indexOf(src)>-1){
                    scripts[i].parentNode.removeChild(scripts[i]);
                    scripts[i]=null;
                }
            }
        },
        /***
         * 初始化路由
         */
        initRouter:function(){
            var util= oui.util;
            function Router() {
                this.routes = {};
            }
            //获取当前加载的url
            Router.prototype.getCurrentLoadUrl = function(){
                return this.url4load||"tpl/welcome.vue.html"; //默认开始页面
            };
            Router.prototype.route = function (path, callback) {
                this.routes[path] = callback || function () { };
            };
            /**
             * 处理 url参数
             * @param obj
             * @returns {*}
             */
            Router.prototype.param =function(data){
                if ( typeof data != 'object') {
                    return( ( data == null ) ? "" : data.toString() );
                }
                var buffer = [];
                // Serialize each key in the object.
                for ( var name in data ) {
                    if ( ! data.hasOwnProperty( name ) ) {
                        continue;
                    }
                    var value = data[ name ];
                    buffer.push(
                        encodeURIComponent( name ) + "=" + encodeURIComponent( ( value == null ) ? "" : value )
                    );
                }
                // Serialize the buffer and clean it up for transportation.
                var source = buffer.join( "&" ).replace( /%20/g, "+" );
                return( source );
            };
            //放入路由路径
            Router.prototype.push = function(path,query,includeEl){

                //console.log('path :', path)
                //var paramsStr=this.param(query);

                var paramsStr = this.param(query||{});
                if(paramsStr){
                    paramsStr= '&'+paramsStr;
                }
                this.includeEl = includeEl;
                if (path.indexOf("?") !== -1) {
                    this.currentUrl =  path + '&key=' + util.genKey()+paramsStr;
                } else {
                    this.currentUrl =  path + '?key=' + util.genKey()+paramsStr;
                }
                if(this.currentUrl.indexOf('#')<0){
                    this.currentUrl='#'+this.currentUrl;
                }
                this.refresh();
            };
            Router.prototype.refresh = function () {
                //console.log('url change');
                //console.log(location.hash);
                var params = util.getParamsUrl(this.currentUrl);
                this.query = params.query;
                this.path = params.path;
                this.params = params.params;

                if(!this.path){
                    //当前没有路由，则不做处理
                    return;
                }
                var me = this;
                //如果存在自定义路由配置，则执行自定义路由逻辑，否则执行自动路由
                if(this.refreshBefore){
                    var flag = this.refreshBefore();
                    if(!flag){
                        return ;
                    }
                }
                var url = this.path;
                var url4load = url;
                var contextPath = oui.getContextPath();
                if(url.indexOf('/')==0){ //以斜杠开头
                    if(url.indexOf(contextPath)!=0){
                        url4load= contextPath+url.substring(1);
                    }
                }else{//以相对路径 或者http路径开头
                    if(url.indexOf('http')!=0){//以相对路径开头
                        url4load = contextPath+url;
                    }
                }
                if(this.currentUrl.indexOf('?')>0){
                    url4load = url4load + this.currentUrl.substring(this.currentUrl.indexOf('?'));
                }
                //刷新页面
                this.url4load = url4load;
                if(this.routes[this.path]){
                    this.routes[this.path]();
                }else{
                    this.startTime = new Date(); // router 改变时间
                    //存在点，则就说明是后缀名类型的模板
                    var pathSuffix = this.path.substring(0,this.path.indexOf('.html'));
                    if(pathSuffix.indexOf('.')>0){ // 指定模板类型的后缀
                        //尝试通过ajax获取视图模板
                        //跟上文件名后缀
                        NProgress.start();

                        util.loadComponent(this.path,{
                            $router:params
                        },this.includeEl,function(options){
                            NProgress.set(0.5);
                            NProgress.done();
                            me.endTime = new Date();
                            //成功回调
                        },function(error){
                            //失败回调
                            NProgress.set(0.5);
                            NProgress.done();
                            console.log(error);
                            console.log('路由['+me.currentUrl+']资源加载异常!!!');
                        });
                    }else{//模拟原生html页面跳转，无模板类型的页面自动转向

                        com.oui.portal.PortalController.renderByLoadSys();
                    }
                }
            };
            //默认文件全路径自动路由机制
            win.Router = Router;
            win.oui.router = new Router();
        }

    };
    PortalController = oui.biz.Tool.crateOrUpdateClass(PortalController);
})(window, window.$$||window.$);


