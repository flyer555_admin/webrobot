let domain = 'https://www.daifatu.com'; //https://www.daifatu.com/user/send_order
//let domain ='http://www.daifaniao.com';

function toGift(request, sender, sendResponse) {
  try {
    chrome.tabs.create({
      url: domain + '/user/send_order',  //正式
      // url: 'http://192.168.10.43:8084/index/gifts/batch_ordering',//小布丁的
	  //url:'http://www.gift.com/index/gifts/batch_ordering', //测试
	  //url:'http://gift.sayii.com/index/gifts/batch_ordering', //测试
      selected: true,
      pinned: false
    }, function(tab) {
      tabBuffer[tab.id] = {};
      tabBuffer[tab.id] = {
        orderType:request.orderType,
        order_list: request.list
      };
      tabRelation[tab.id] = sender.tab.id;
      // let obj = {order_list:[
      //   {
      //     order_id: 12345,
      //     address: '收货地址1'
      //   },
      //   {
      //     order_id: 54321,
      //     address: '收货地址2'
      //   }
      // ]};
      console.log(tabBuffer);

      sendResponse(request);
    });
  } catch (e) {
    console.log(e.message);

  }
}

function getList(request, sender, sendResponse) {
  try {
    let tab_id = sender.tab.id;
    sendResponse(tabBuffer[tab_id]);
  } catch (e) {
    console.log(e.message);

  }
}

function toDelivery(request, sender, sendResponse) {
  try {
    
    let giftTabId = sender.tab.id;
    let tabId = tabRelation[giftTabId]; //获得对应的淘宝发货tabid
    console.log(giftTabId);
    console.log(tabId);
    console.log(tabRelation);
    chrome.tabs.sendRequest(tabId, {
      type: 'writeDeliveryCode',
      data: request.data
    }, function(response) {
      let tabIndex;
      let windowId;
      chrome.tabs.query({
          // active: true,
          // currentWindow: true
        },
        tabs => {
          console.log(tabs);
          tabs.forEach((item,index) => {
            if(item.id == tabId){
             tabIndex = item.index;
             windowId = item.windowId;
            }
          })
          console.log(tabIndex);
          chrome.tabs.highlight({
            windowId:windowId,
            tabs: tabIndex
          }, function(response) {
            console.log(response);
          })
          console.log('执行选中标签');
        }
      );

    });

    sendResponse({
      tab_id: tabId
    });

  } catch (e) {
    console.log(e.message);

  }
}

function test(request, sender, sendResponse) {
  try {
    let giftTabId = sender.tab.id;
    let tabId = tabRelation[giftTabId]; //获得对应的淘宝发货tabid
    let tabIndex;
    let windowId;
    sendResponse({
      tab_id: sender.tab.id
    });
    chrome.tabs.query({
        // active: true,
        // currentWindow: true
      },
      tabs => {
        console.log(tabs);
        tabs.forEach((item,index) => {
          if(item.id == tabId){
           tabIndex = item.index;
           windowId = item.windowId;
          }
        })
        
        console.log(tabIndex);
        chrome.tabs.highlight({
          windowId:windowId,
          tabs: tabIndex
        }, function(response) {
          console.log(response);
        })
        console.log('执行选中标签');
      }
    );
  } catch (e) {
    console.log(e.message);

  }
}
function jingdongGetSource(request, sender, sendResponse) {
  try {
    console.log(request.value.order_id);
    let address;
    let phone;
    let addressPromise = new Promise((resolve, reject) => {
    			 $.ajax({
    			    type:"GET",
    			    url:"https://porder.shop.jd.com/order/global/getConsigneeInfo?orderId=" + request.value.order_id,
    			    dataType:"json",
    				data:{
    				
    				},
    			    success: function (data) {
                address = data.model.consigneeInfo.address + ' ' + data.model.consigneeInfo.name;
    				  resolve({
    				  	address
    				  });
    			    }
    			})
    		});
        
    let phonePromise = new Promise((resolve, reject) => {
    			 $.ajax({
    			    type:"GET",
    			    url:"https://porder.shop.jd.com/order/global/viewOrderMobile?orderId=" + request.value.order_id,
    			    dataType:"json",
    				data:{
    				
    				},
    			    success: function (data) {
                phone = data.phone;
    				  resolve({
    				  	phone
    				  });
    			    }
    			})
    		});
        
       Promise.all([addressPromise, phonePromise]).then((result) => {
          address = result[0].address + ' ' +  result[1].phone;
          sendResponse({address});
       })
    
  } catch (e) {
    console.log(e.message);
  }
}

function toJingdongBatch(request, sender, sendResponse){
  let data = {
    delivery_name:'圆通快递',
    list: request.list
  }
  try {
    chrome.tabs.create({
      url: request.url,  //正式
      selected: true,
      pinned: false
    }, function(tab) {
      
      setTimeout(function(){
         chrome.tabs.sendRequest(tab.id, {
           type: 'writeDeliveryCode',
           data: data
         }, function(response) {
         
         });
      },5000)

       
      sendResponse(request);
    });
  } catch (e) {
    console.log(e.message);
  
  }
}

function jingdongCallback(request,sender,sendResponse){
    $.ajax({
        type:"POST",
        url: domain + "/index/extendapi/query_delivery_code",
        dataType:"JSON",
        data:{
           plat_type: 2,
           order_ids: JSON.stringify(request.list)
        },
        success: function (data) {
          sendResponse(data);
        }
    })
}
function sendTabMsg(request, sender, sendResponse) {
  try {
    chrome.tabs.sendRequest(request.tabId, request, function(response) {
      sendResponse(response);
    });

  } catch (e) {
    console.log(e.message);

  }
}

function getBackgroundValue(request, sender, sendResponse) {

  if (request.key == "URL") {
    console.log(request);
    console.log(backGroundJson);
  }
  sendResponse(backGroundJson[request.key]);
}

function setBackgroundValue(request, sender, sendResponse) { //
  backGroundJson[request.key] = request.value;
  if (sendResponse != null) {
    sendResponse();
  }
}

function getTabMsg(request, sender, sendResponse) {
  if (request.tabId == null) {
    sendResponse(tabBuffer[sender.tab.id]);
  } else if (request.tabId == "all") {
    sendResponse(tabBuffer);
  } else {
    sendResponse(tabBuffer[request.tabId]);
  }
}

function setTabMsg(request, sender, sendResponse) {
  var tabId = request.tabId;
  if (tabId == null) {
    tabId = sender.tab.id;
  }
  tabBuffer[tabId] = {};
  tabBuffer[tabId].OrderMsg = request.value;
  sendResponse(tabBuffer);
}

function removeTabMsg(request, sender, sendResponse) {
  if (request.tabId != null) {
    delete tabBuffer[request.tabId];
  } else {
    delete tabBuffer[sender.tab.id];
  }
}

function insertCSS(request, sender, sendResponse) {

  $.ajax({
    url: backGroundJson.URL + request.inputCss,
    dataType: "text",
    success: function(xhr) {
      chrome.tabs.insertCSS(sender.tab.id, {
        code: xhr
      });
      sendResponse({});
    },
    error: function(xhr) {
      chrome.tabs.insertCSS(sender.tab.id, {
        code: xhr.responseText
      });
      sendResponse({});
    }
  });

}


function executeScript(request, sender, sendResponse) {
  $.ajax({
    url: backGroundJson.URL + request.inputScript,
    dataType: "text",
    success: function(xhr) {
      chrome.tabs.executeScript(sender.tab.id, {
        code: xhr
      });
      sendResponse({});
    },
    error: function(xhr) {

      chrome.tabs.executeScript(sender.tab.id, {
        code: xhr.responseText
      });
      sendResponse({});
    }
  });
}


