/**
 * 处理京东的批量出库
 */

$(function() {
  let jd = new JingdongBatch();

    //order/orderlist/waitOut
    let intval = setInterval(function(){
      let url = window.location.href;
      if(url.indexOf('orderlist/allOrders') != -1 || url.indexOf('orderlist/waitOut') != -1 ){
        clearInterval(intval);
        window.location.reload();
      }
    },1000);
    
  setTimeout(function() {
    jd.getOrderList();
    jd.getOrderAddress();
    
  }, 1000)
})



class JingdongBatch {
  constructor() {
    this.listPre = []; //临时数组
    this.list = [];
  }
  /**
   * 获取页面所有订单
   */
  getOrderList() {
    let objList = $('form table.out-lost-table');
    objList.each((index, item) => {
      let td = $(item).children('tr:first').children('td:first');
      let order_id = $(td).find('a').text();
      this.listPre.push({
        order_id
      })
    })

    return this.listPre;
  }

  getOrderAddress() {
    let promiseList = [];
    let prom;
    let that = this;
    this.listPre.forEach((item, index) => {
      prom = new Promise((resolve, reject) => {
        //京东的要去后台js跨域获取收货信息和手机号
        chrome.extension.sendRequest({
          type: "jingdongGetSource",
          value:{
            order_id:item.order_id,
            cookie: document.cookie
          }
        }, function(response) {
          console.log(response);
            let address = response.address;
            address = format(address);
            that.list.push({
              order_id:item.order_id,
              address
            })
            
            resolve();
        });
            
      })
    })
    promiseList.push(prom);
    Promise.all(promiseList).then((result) => {
      console.log(this.list);
      this.addButton();
      this.toGift();
      this.jingdongCallback();
    })
    
  }
  
  toGift(){
    let that = this;
    $('#daifatu-gift').click(function() {
      //发送消息给后台js
    
      chrome.extension.sendRequest({
        type: "toGift",
        orderType:'jd',
        list:that.list
      }, function(response) {
        console.log(that.list);
        console.log(response);
      });
    })
  }
  jingdongCallback(){
    let that = this;
    $('#jingdong-gift').click(function() {
      //发送消息给后台js
      chrome.extension.sendRequest({
        type: "jingdongCallback",
        list:that.list
      }, function(response) {
          console.log(response);
          if(response.status == 200){
            response.order_list.forEach((item, index) => {
                let order_id = item.order_id;
                let a = $("a:contains('" + order_id + "')");
                let tr = a.parents('tr');
                let inp = tr.find(".ivu-input");
                
                tr.find("li.ivu-select-item:contains('圆通快递')").trigger('click');
                
                inp = inp.get(0);
                
                let txt;
                if(!item.delivery_code){
                  txt = '运单号未出，等待商家上传';
                }else{
                  txt = item.delivery_code;
                }
                var evt = new InputEvent('input', {
                	inputType: 'insertText',
                	data: txt,
                	dataTransfer: null,
                	isComposing: false
                });
                inp.value = txt;
                inp.dispatchEvent(evt);
                
            })
          }
          if(response.status == 400){
            alert('请先登陆礼品站');
          }
      });
    })
  }
  
  
  
  
  


  /**
   * 页面控制 选择厂家自送
   */

  deliveryWay() {
    $('li.ivu-select-item:contains("厂家自送")').trigger('click');
  }

  /**
   * 添加物流信息
   */
  addInfo() {
    $('a:contains("添加物流信息")').trigger('click');
  }
  /**
   * 添加按钮
   */
  addButton(){
    //<div style="position: fixed;right: 60px;top: 200px;text-align: center;width: 18px;height: 120px;float: left;color:white;background: #ff464e;padding:3px 6px;display:inline-block;cursor:pointer;border-radius:6px;" id="daifatu-gift" data-spm-anchor-id="a1z0f.6.0.i0.691f2e91UXUz0z">从代发兔下单</div>
     var div = '<div style="position: fixed;right: 60px;top:200px;line-height:22px;font-size:19px;color:white;height: 142px;width: 40px;text-align: center; background: #ff464e;padding:3px 2px; display:inline-block; cursor:pointer;border-radius:6px" id="daifatu-gift" >从代发兔下单</div>';
      $('h3.out-title-first:first').append(div);
      //var div2 = '<div style="margin-left:20px;line-height:20px;color:white;background:green;padding:3px 6px; display:inline-block; cursor:pointer;border-radius:6px" id="jingdong-gift">京东单号回填</div>'
      //$('h3.out-title-first:first').append(div2);
  }
}


function format(str) {
  str = str.replace('，','').replace(',','');
  
  let arr = str.split(' ');
  
  let name = arr[arr.length-2];
  let phone = arr[arr.length-1];
  arr.length=arr.length-2;
  let address = arr.join(' ');

  /**
   * 京东的省份不带“省”  除直辖市以外主动填
   */
  
  let filter = address.trim();
  let key,key1,key2;
  
  key1 = filter[0];
  key2 = filter[1];
  key = key1 + key2;
  let MCG = '北京,上海,重庆,天津'; //Municipality directly under the Central Government 四个直辖市 不带“省”
  let threeProvince = '黑龙,内蒙';
  
  
  if(MCG.indexOf(key) != -1){
    
  }else if(threeProvince.indexOf(key) != -1){
       address = address.slice(0,3) + '省' + address.slice(3);
  }else{
       address = address.slice(0,2) + '省' + address.slice(2);
  }

 
  let newArr = [];
  newArr[0] =name;
  newArr[1] =phone ;
  newArr[2] =address ;
  
  str = newArr.join(',');
  return str;
}

chrome.extension.onRequest.addListener(
  function(request, sender, sendResponse) {

    if (request.type == "writeDeliveryCode") {
      let list = request.data.dataList;
      if(list.length == 0){
        showTips();
        sendResponse();
        return ;
      }
      console.log('回来的单号');
      console.log(request.data);
      var delivery_name = request.data.delivery_name;
      list.forEach((item, index) => {
          
          let order_id = item.orderId;
          let a = $("a:contains('" + order_id + "')");
          let tr = a.parents('tr');
          let inp = tr.find(".ivu-input");
          
          tr.find("li.ivu-select-item:contains('"+delivery_name+"')").trigger('click');
          
          inp = inp.get(0);
          
          let txt;
          if(!item.deliverCode){
            txt = '18:00之前订单当天19:00前出单号';
          }else{
            txt = item.deliverCode;
          }
          var evt = new InputEvent('input', {
          	inputType: 'insertText',
          	data: txt,
          	dataTransfer: null,
          	isComposing: false
          });
          inp.value = txt;
          inp.dispatchEvent(evt);
          
      })
      //document.querySelector("#app > p > button.btn-so.ivu-btn.ivu-btn-primary");//回填完成后自动点击
      //暂时不做订单处理
      //let btnDom = document.querySelector("#app > p > button.btn-so.ivu-btn.ivu-btn-primary");
      //btnDom&&btnDom.click();

       sendResponse();
    }
  }
)

function showTips(){
  let objList = $('form table.out-lost-table');
  objList.each((index, item) => {
    let tr = $(item).children('tr:first');
    let inp = tr.find(".ivu-input");
    
    tr.find("li.ivu-select-item:contains('圆通快递')").trigger('click');
    
    inp = inp.get(0);
    var evt = new InputEvent('input', {
    	inputType: 'insertText',
    	data: '18:00之前订单当天19:00前出单号',
    	dataTransfer: null,
    	isComposing: false
    });
    inp.value = '18:00之前订单当天19:00前出单号';
    inp.dispatchEvent(evt);
  })
}
