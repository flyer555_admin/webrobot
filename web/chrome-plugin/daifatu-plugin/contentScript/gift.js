// chrome.extension.onRequest.addListener(
//    function(request, sender, sendResponse) {
// 
//      console.log(request);
// 
//      sendResponse({
//        farewell: "goodbye"
//      });
//    }
// );
$(function() {
  var triggerInput = function (dom){
    var evt = document.createEvent("Event");
    evt.initEvent("input", false, true);
    dom.dispatchEvent(evt);
  };
  function getParam(key){
    var cfg = {};
    var url = location.search; //获取url中"?"符后的字串
    if (url.indexOf("?") != -1) {
      var str = url.substr(url.indexOf("?")+1);
      var strs = str.split("&");
      for (var i = 0; i < strs.length; i++) {
        cfg[strs[i].split("=")[0]] = decodeURIComponent(strs[i].split("=")[1]);
      }
    }
    return key?cfg[key]:cfg;
  }

  var me = {
    /**获取发货单号 cmd:getDeliveryCode **/
    getDeliveryCode:function(param,event){
      //批量传入 数据格式  {orderType:'taobao',dataList:[{orderId,deliverCode}] } //订单id，发货单号,支持批量回填
      chrome.extension.sendRequest({type:'toDelivery',data:param},function(response){

      })
    }

  };
 /**需要原有页面改造 ,提供批量回填发货单号事件绑定   */
 //接口处理调用:
 /* window.postMessage({
     cmd:'getDeliveryCode',
     param:{
       delivery_name:'',//物流公司名称
       orderType:'taobao', //订单类型，taobao,jd
       dataList:[]//订单列表 数据格式 [{orderId,deliverCode}] //订单号,发货单号
     }
   },'*');
  */

  //绑定更新物流送货单号事件
  window.addEventListener("message", function(event) {
    var data = event.data;
    if(typeof data =='string'){
      return;
    }
    if(typeof data =='object'){
      //在本类中的方法负责接收消息处理
      if(data.cmd&&data.param){
        me[data.cmd]&&me[data.cmd](data.param,event);
      }
    }
  }, false);
  var orderId = getParam('order_id');
  if(orderId){//用于发送消息的处理
    var message = localStorage.getItem('daifatu_google_data');
    if(message){
      message = JSON.parse(message);
      window.postMessage(message,'*');
    }
  }else{
      setTimeout(function() {
        chrome.extension.sendRequest({
          type: "getList"
        }, function(response) {

          console.log(response);
          let obj = response;

          let orderType = obj.orderType;
          if(orderType){
            if(orderType =='taobao'){
              let tabaoDom = document.querySelector("#apps > div > div.user_main > div > div.subnav_content > div > div:nth-child(1) > div.list > ul > li");
              tabaoDom.click();
            }else if(orderType=='jd'){
              let jindongDom =document.querySelector("#apps > div > div.user_main > div > div.subnav_content > div > div:nth-child(1) > div.list > ul > li:nth-child(2)");
              jindongDom.click();
            }else if(orderType=='pdd'){
              let pddDom =document.querySelector("#apps > div > div.user_main > div > div.subnav_content > div > div:nth-child(1) > div.list > ul > li:nth-child(3)");
              pddDom.click();
            }

          }

          let val = '';
          $('#app').find('textarea').val(val);
          obj.order_list.forEach((item, index) => {
            val += item.order_id + '，' + item.address;
          val += '\n';
        });
        $('#app').find('textarea')[0].value=val;
        $('#app').find('textarea').val(val);
        triggerInput($('#app').find('textarea')[0]);
        //默认不添加到列表
        //setTimeout(function(){
        //  var dom = document.querySelector("#apps > div > div.user_main > div > div.subnav_content > div > div:nth-child(3) > div.btn");
        //  dom.click();
        //});

      });
    }, 500);//默认进入后回填
  }

})
