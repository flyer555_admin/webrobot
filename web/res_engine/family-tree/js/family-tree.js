!(function(win, $)
{
    var repeatCount = 0;
    var cTimeout;
    var timeoutIntervals = new Array();
    var timeoutIntervalSpeed;
    function elementPosition(obj) {
        var curleft = 0, curtop = 0;
        if (obj.offsetParent) {
            curleft = obj.offsetLeft;
            curtop = obj.offsetTop;
            while (obj = obj.offsetParent) {
                curleft += obj.offsetLeft;
                curtop += obj.offsetTop;
            }
        }
        return { x: curleft, y: curtop };
    }
    function ScrollToControl(dom)
    {
        var elem =dom;
        var scrollPos = elementPosition(elem).y;
        scrollPos = scrollPos - document.documentElement.scrollTop;
        var remainder = scrollPos % 50;
        var repeatTimes = (scrollPos - remainder) / 50;
        ScrollSmoothly(scrollPos,repeatTimes);
        window.scrollBy(0,remainder);
    }

    function ScrollSmoothly(scrollPos,repeatTimes)
    {
        if(repeatCount < repeatTimes)
        {
            window.scrollBy(0,50);
        }
        else
        {
            repeatCount = 0;
            clearTimeout(cTimeout);
            return;
        }
        repeatCount++;
        cTimeout = setTimeout("oui.ScrollSmoothly('"+scrollPos+"','"+repeatTimes+"')",10);
    }
    oui.ScrollSmoothly = ScrollSmoothly;
    var FamilyTree = {
        "package": "com.oui.models.menu.web",//com.oui.models.menu.web.FamilyTree
        "class": "FamilyTree",
        data: {},

        initData: function()
        {

            var me = this;
            this.saved = true;
            me.data.clickName = oui.os.mobile ? 'tap' : 'click';
            var urlParams = oui.getPageParam('urlParams') || {};
            //var id = urlParams.id || '';

            me.urlParams = urlParams;
            oui.setPageParam('_menu_page_' + 'family-tree', oui.parseJson(oui.parseString(urlParams)));

            template.helper("FamilyTree", this);
            me.data.clickName = oui.os.mobile ? 'tap' : 'click';

            me.data.apiMap = {};
            me.data.menu = {};
            me.nodeTypeEnum = {
                root: {
                    name: 'root'
                },
                menu: {
                    name: 'menu'
                },
                spouse: {
                    name: 'spouse'
                }

            };

            me.menuActionEnum = {
                designPage: {//设计页面
                    action: function(node, treeMap, orgGraph)
                    {
                        var id = node.id;
                        orgGraph.showPageDesign(id);
                    }
                },

                addSpouse: {
                    api: 'addSpouse',
                    display: '添加爱人',
                    getApiParams: function(node, treeMap, orgGraph)
                    {
                        var ids = treeMap.ids || [];
                        var params = {
                            menu: {
                                spouseId: node.id,
                                id: treeMap.newId(),
                                name: '新的节点' + (ids.length + 1),
                                nodeType: me.nodeTypeEnum.spouse.name
                            }
                        };
                        return params;
                    },

                    action: function(node, treeMap, orgGraph, targetNode, result)
                    {
                        me.saved = false;
                        var newNode = result.menu;
                        var spouse = node.node.spouse || [];
                        spouse.push(newNode);
                        node.node.spouse = spouse;
                        //刷新当前节点和下面的节点列表
                        orgGraph.refreshByNodeId(node.parentId || node.id, treeMap);
                    }
                },
                addMenu: {
                    api: 'addMenu',
                    display: '添加节点',
                    getApiParams: function(node, treeMap, orgGraph)
                    {
                        var ids = treeMap.ids || [];
                        var params = {
                            menu: {
                                id: treeMap.newId(),
                                parentId: node.id,
                                name: '新的节点' + (ids.length + 1),
                                nodeType: me.nodeTypeEnum.menu.name
                            }
                        };
                        return params;
                    },

                    action: function(node, treeMap, orgGraph, targetNode, result)
                    {
                        me.saved = false;
                        var newNode = result.menu;
                        treeMap.addNode(newNode); //添加节点
                        //刷新当前节点和下面的节点列表
                        orgGraph.refreshByNodeId(node.id, treeMap);
                    }
                },
                designPage: {
                    //设计页面
                    action: function(node, treeMap, orgGraph, targetNode, result)
                    {
                        var tempNode = node.node || {};
                        tempNode.page = tempNode.page || {};
                        var page = tempNode.page;
                        var controls = page.controls || [];
                        controls = oui.parseJson(controls);//处理控件字符串为数组
                        page.events = oui.parseJson(page.events || {});//处理事件的字符串为对象
                        page.controls = controls;
                        page.style = oui.parseJson(page.style);
                        page.innerStyle = oui.parseJson(page.innerStyle);
                        page.pageDesignType = 'normalForm';

                        //TODO 处理 页面url 加载表单设计器

                        //转换后台返回的数据格式，浮点数被转字符串问题修复 fix定位位置问题
                        //page = me.transPage4Front(page);
                        /*


                         pageBizPropsUrl:'res_engine/page_design/pc/page-biz-tpl-robot.art.html', //页面业务属性扩展url
                         controlBizPropsUrl:'res_engine/page_design/pc/components-biz-prop-art-tpl-robot', //控件业务属性扩展url
                         buttons:'preview,save,merge,split,insertColumn4prev,insertRow4prev,removeColumn,removeRow',
                         bizJs:'res_engine/page_design/pc/js/page-plugin-robot.js',
                         saveCallBack:'saveCallback'
                         */
                        //保存节点页面回调命令
                        var dialog = oui.showPageDesign({
                            //pageBizPropsUrl:'res_engine/page_design/pc/page-biz-tpl-robot.art.html', //页面业务属性扩展url
                            //controlBizPropsUrl:'res_engine/page_design/pc/components-biz-prop-art-tpl-robot', //控件业务属性扩展url
                            buttons: 'preview,save,merge,split,insertColumn4prev,insertRow4prev,removeColumn,removeRow',
                            bizJs: 'res_engine/family-tree/js/page-design-plugin.js',

                            uploadUrl: oui.uploadURL,//跨静态页面传值，用于上传组件
                            /**必须参数*/
                            controls: [],//已有的控件列表
                            mainTemplate: '',//页面的业务属性面板模板获取方法
                            saveCallBack: 'saveCallback',
                            /**非必须参数*/
                            viewType: 'urlDialog',
                            useControls: false,//使用传入控件列表作为 待设计控件区域,否则待选区域的控件全控件列表
                            page: page,//页面设计对象，如果是打印则是打印模板设计对象
                            scriptPkg: "com.oui.DesignBiz", //指定扩展脚本包,为了避免命名空间与页面设计的命名冲突，指定命名空间，实现业务与设计的代码命名隔离，api调用不冲突
                            bizCss: [],
                            canCloneControl: true,//是否可以复制控件
                            //其它业务参数调用传递,重要提示：cfg.params的参数最好使用简单map对象，不能包含任何dom对象或window对象等
                            params: {}
                        });


                    }
                },
                editEnName: { //编辑英文名，一般用于导出项目代码的目录名
                    action: function(node, treeMap, orgGraph, targetNode, result)
                    {
                        var name = node.node.enName || node.id;
                        oui.getTop().oui.showInputDialog('编辑英文名称', function(v)
                        {
                            if(!v)
                            {
                                oui.getTop().oui.alert('英文名称不能为空');
                                return;
                            }
                            var url = me.data.apiMap['updateNodeEnName'];
                            if(url)
                            {
                                oui.postData(url, {
                                    nodeId: node.id,
                                    nodeType: node.node.nodeType,
                                    oldEnName: name,
                                    newEnName: v
                                }, function(res)
                                {
                                    if(res.success)
                                    {
                                        node.node.enName = v;
                                        orgGraph.refreshByNodeId(node.id, treeMap);
                                        me.saveDesign();
                                    } else
                                    {
                                        oui.getTop().oui.alert(res.msg);
                                    }
                                }, '保存中');
                            } else
                            {
                                node.node.enName = v;
                                orgGraph.refreshByNodeId(node.id, treeMap);
                                me.saveDesign();
                            }

                        }, [{type: "text", value: name}]);
                    }
                },
                editName: {
                    action: function(node, treeMap, orgGraph, targetNode, result)
                    {
                        var name = node.node.name;

                        nodeEditModal.show();   //编辑弹窗显示
                        return;

                        oui.getTop().oui.showInputDialog('编辑名称', function(v)
                        {
                            if(!v)
                            {
                                oui.getTop().oui.alert('节点名称不能为空');
                                return;
                            }
                            var url = me.data.apiMap['updateNodeName'];
                            if(url)
                            {
                                oui.postData(url, {
                                    nodeId: node.id,
                                    nodeType: node.node.nodeType,
                                    oldName: name,
                                    newName: v
                                }, function(res)
                                {
                                    if(res.success)
                                    {
                                        node.node.name = v;
                                        orgGraph.refreshByNodeId(node.id, treeMap);
                                        me.saveDesign();
                                    } else
                                    {
                                        oui.getTop().oui.alert(res.msg);
                                    }
                                }, '保存中');
                            } else
                            {
                                node.node.name = v;
                                orgGraph.refreshByNodeId(node.id, treeMap);
                                me.saveDesign();
                            }

                        }, [{type: "text", value: name}]);
                    }
                },
                edit: {
                    /** 业务编辑，如配置页面url，指定自定义页面等 ***/
                    action: function(node, treeMap, orgGraph, targetNode, result)
                    {
                        var name = node.node.name;
                        var view = oui.getById("family-tree");
                        me.editOldNode = oui.parseJson(oui.parseString(node.node));

                        var html = view.getHtmlByTplId('node-edit-options-tpl', {
                            treeMap: me.treeMap,
                            nodeId: node.id
                        });
                        document.getElementById('node-edit-modal').outerHTML = html;

                        //初始化日期选择
                        /* $('#birthDate').fdatepicker({
                             format: 'yyyy-mm-dd hh:ii',
                         });
                         $('#dateOrDeath').fdatepicker({
                             format: 'yyyy-mm-dd hh:ii',
                         });*/

                        //初始化日期选择
                        new Rolldate({
                            el: '#birthDate',
                            format: 'YYYY-MM-DD hh:mm',
                            beginYear: 1900,
                        });

                        new Rolldate({
                            el: '#dateOrDeath',
                            format: 'YYYY-MM-DD hh:mm',
                            beginYear: 1900,
                        });

                    }
                },

                //编辑爱人
                editSpouse: {
                    /** 业务编辑，如配置页面url，指定自定义页面等 ***/
                    action: function(node, treeMap, orgGraph, targetNode, params, result, spouseId)
                    {
                        me.editOldNode = oui.parseJson(oui.parseString(node.node));

                        var view = oui.getById("family-tree");
                        var html = view.getHtmlByTplId('node-edit-spouse-options-tpl', {
                            treeMap: me.treeMap,
                            spouseId: spouseId,
                            nodeId: node.id
                        });

                        document.getElementById('node-edit-modal').outerHTML = html;

                        //初始化日期选择
                        /*$('#birthDate').fdatepicker({
                            format: 'yyyy-mm-dd',
                        });
                        $('#dateOrDeath').fdatepicker({
                            format: 'yyyy-mm-dd',
                        });*/


                        //初始化日期选择
                        new Rolldate({
                            el: '#birthDate',
                            format: 'YYYY-MM-DD hh:mm',
                            beginYear: 1900,
                        });

                        new Rolldate({
                            el: '#dateOrDeath',
                            format: 'YYYY-MM-DD hh:mm',
                            beginYear: 1900,
                        });

                    }
                },
                //删除爱人
                removeSpouse: {
                    action: function(node, treeMap, orgGraph, targetNode, params, result, spouseId)
                    {
                        var spouses = me.findSpouse(node.id, treeMap);
                        oui.removeFromArrayBy(spouses, function(curr)
                        {
                            if(curr && curr.id == spouseId)
                            {
                                return true;
                            }
                        });
                        orgGraph.refreshByNodeId(node.id, treeMap);
                    }
                },

                addApp: {
                    api: 'addApp',
                    display: '添加应用',
                    getApiParams: function(node, treeMap, orgGraph)
                    {
                        var ids = treeMap.ids || [];
                        var newNode = {
                            id: treeMap.newId(),
                            parentId: node.id,
                            name: '新的应用' + (ids.length + 1),
                            nodeType: me.nodeTypeEnum.app.name,
                            menuId: me.data.menu.id,
                            circleId: me.data.menu.circleId,
                            moduleId: node.id
                        };
                        var params = {
                            app: newNode
                        };
                        return params;
                    },
                    action: function(node, treeMap, orgGraph, target, result)
                    {
                        var newNode = result.app;
                        treeMap.addNode(newNode); //添加节点
                        //刷新当前节点和下面的节点列表
                        orgGraph.refreshByNodeId(node.id, treeMap);
                    }
                },
                addPage: {
                    api: 'addPage',
                    display: '添加页面',
                    getApiParams: function(node, treeMap, orgGraph)
                    {
                        var params = {};
                        var ids = treeMap.ids || [];
                        var moduleId = node.parentId;
                        var newNode = {
                            id: treeMap.newId(),
                            parentId: node.id,
                            name: '新的页面' + (ids.length + 1),
                            nodeType: me.nodeTypeEnum.page.name,
                            menuId: me.data.menu.id,
                            circleId: me.data.menu.circleId,
                            appId: node.id,
                            moduleId: moduleId
                        };
                        params.page = newNode;
                        return params;
                    },
                    action: function(node, treeMap, orgGraph, target, result)
                    {
                        var newNode = result.page;
                        treeMap.addNode(newNode); //添加节点
                        //刷新当前节点和下面的节点列表
                        orgGraph.refreshByNodeId(node.id, treeMap);
                    }
                },
                addLogic: {
                    api: 'addLogic',
                    display: '添加逻辑',
                    getApiParams: function(node, treeMap, orgGraph)
                    {
                        var params = {};
                        var ids = treeMap.ids || [];
                        var moduleId = node.parentId;
                        var newNode = {
                            id: treeMap.newId(),
                            parentId: node.id,
                            name: '新的逻辑' + (ids.length + 1),
                            nodeType: me.nodeTypeEnum.logic.name,
                            menuId: me.data.menu.id,
                            circleId: me.data.menu.circleId,
                            appId: node.id,
                            moduleId: moduleId
                        };
                        params.logic = newNode;
                        return params;
                    },
                    action: function(node, treeMap, orgGraph, target, result, res)
                    {
                        var newNode = result.logic;
                        treeMap.addNode(newNode); //添加节点
                        //刷新当前节点和下面的节点列表
                        orgGraph.refreshByNodeId(node.id, treeMap);
                    }
                },
                removeAll4ajax: { //删除当前和所有子孙,并执行后台api删除
                    api: 'removeAllNode',
                    display: '删除节点',
                    getApiParams: function(node, treeMap, orgGraph)
                    {
                        var params = {
                            nodeId: node.id,
                            nodeType: node.node.nodeType,
                            menuId: me.data.menu.id,
                            circleId: me.data.menu.circleId
                        };
                        return params;
                    },
                    action: function(node, treeMap, orgGraph, target, result)
                    {
                        if(!node.parentId)
                        {
                            return;
                        }
                        me.saved = false;
                        var parentId = node.parentId;
                        treeMap.removeNodeAll(node);
                        orgGraph.refreshByNodeId(parentId, treeMap);

                    }
                },
                add: { //添加子节点
                    action: function(node, treeMap, orgGraph)
                    {
                        var newNode = {
                            id: treeMap.newId(),
                            parentId: node.id,
                            name: '新的节点'
                        };
                        me.saved = false;
                        treeMap.addNode(newNode); //添加节点
                        //刷新当前节点和下面的节点列表
                        orgGraph.refreshByNodeId(node.id, treeMap);
                    }
                },
                addParent: {//添加一个新节点 作为当前节点的父节点
                    action: function(node, treeMap, orgGraph)
                    {
                        var newNode = {
                            id: treeMap.newId(),
                            name: '新的节点',
                            parentId: node.parentId || "",
                            nodeType: me.nodeTypeEnum.root.name
                        };
                        node.node.nodeType = 'menu';
                        me.saved = false;
                        var isRefreshRoot = !node.parentId;
                        treeMap.addParentNode(newNode, node); //添加节点
                        if(isRefreshRoot)
                        {
                            orgGraph.refreshByRoot(treeMap);
                        } else
                        {
                            orgGraph.refreshByNodeId(newNode.parentId, treeMap);
                        }
                    }
                },
                addBrother: { //在当前节点后面添加一个兄弟节点,考虑顺序调整
                    action: function(node, treeMap, orgGraph)
                    {
                        if(!node.parentId)
                        { //根节点不能添加兄弟节点
                            return;
                        }
                        me.saved = false;
                        var newNode = {
                            id: treeMap.newId(),
                            name: '新的节点',
                            parentId: node.parentId,
                            nodeType: me.nodeTypeEnum.menu.name
                        };

                        treeMap.addBrotherNode(newNode, node); //添加节点

                        orgGraph.refreshByNodeId(node.parentId, treeMap);
                    }
                },
                remove: {//删除当前节点
                    action: function(node, treeMap, orgGraph)
                    {
                        me.saved = false;
                        //根节点不能删除
                        if(!node.parentId)
                        {
                            if(node.childIds && node.childIds.length == 1)
                            {
                                //根节点只有一个子节点时，才能删除，将子节点作为根节点
                                treeMap.removeRoot(node);
                                orgGraph.refreshByRoot(treeMap);
                            }
                        } else
                        {
                            var parentId = node.parentId;
                            //删除时需要判断是否存在页面设计
                            //删除页面设计,再删除节点
                            treeMap.removeNode(node);
                            orgGraph.refreshByNodeId(parentId, treeMap);
                        }
                    }
                },
                removeAll: { //删除当前和所有子孙
                    action: function(node, treeMap, orgGraph)
                    {
                        if(!node.parentId)
                        {
                            return;
                        }
                        me.saved = false;
                        var parentId = node.parentId;
                        //删除当前和所有孙子节点时，需要遍历删除所有表单设计
                        treeMap.removeNodeAll(node);
                        orgGraph.refreshByNodeId(parentId, treeMap);

                    }
                },
                hideMenu: {
                    action: function(node, treeMap, orgGraph)
                    {

                    }
                },

                //祭祀
                handleJiSi: {
                    action: function(node, treeMap, orgGraph)
                    {
                        console.log(node, treeMap, orgGraph);
                        var name = node.node.name;
                        var avatar = node.node.avatar ? node.node.avatar : '';
                        var param = $.param({
                            id: node.node.id,
                            name: name,
                            avatar: avatar
                        });

                        location.href = 'detail.html?' + param;
                    }
                },

                /** 对于拖拽处理的两个节点位置交换逻辑 *****/

                //            <!--swap,addParentByTarget,addBrotherByTarget,addChildByTarget,hideMenu4DragEnd -->
                swapSort: {
                    ///交换两个节点的顺序
                    action: function(node, treeMap, orgGraph, targetNode)
                    {
                        if((!node) || (!targetNode))
                        {
                            return;
                        }
                        if(node.parentId == targetNode.parentId)
                        {
                            var parentNode = treeMap.findNode(node.parentId);
                            if(parentNode)
                            {
                                var childIds = parentNode.childIds || [];
                                var nodeIdx = childIds.indexOf(node.id);
                                var targetIdx = childIds.indexOf(targetNode.id);
                                childIds[nodeIdx] = targetNode.id;
                                childIds[targetIdx] = node.id;
                            }
                            orgGraph.refreshByNodeId(node.parentId, treeMap);
                        }

                    }
                },
                swap: {
                    /***
                     * 交换算法， 只变更更 交换 两个节点 的名称而已
                     * @param node
                     * @param treeMap
                     * @param orgGraph
                     * @param targetNode
                     */
                    action: function(node, treeMap, orgGraph, targetNode)
                    {
                        var id = node.id;
                        var targetId = targetNode.id;


                        var nodeName = treeMap.findNodeName(node.id);
                        var targetNodeName = treeMap.findNodeName(targetNode.id);
                        treeMap.updateNodeName(node.id, targetNodeName);
                        treeMap.updateNodeName(targetNode.id, nodeName);
                        //刷新 分别刷新两个节点的父节点即可
                        if(treeMap.isRoot(node.id) || treeMap.isRoot(targetNode.id))
                        {
                            orgGraph.refreshByRoot(treeMap);
                        } else
                        {
                            orgGraph.refreshByNodeId(node.parentId, treeMap);
                            orgGraph.refreshByNodeId(targetNode.parentId, treeMap);
                        }

                    }
                },
                addChildByTarget: {//将节点添加到目标节点下，作为目标节点的子节点
                    action: function(node, treeMap, orgGraph, targetNode)
                    {
                        //将当前节点的父亲节点中的 子节点列表中移除 当前节点
                        //目标节点的子节点列表中增加当前节点
                        //刷新目标节点
                        if(!node.parentId)
                        { //根节点不能作为目标节点的子节点
                            return;
                        }
                        /****
                         * 祖先节点不能作为 目标节点的子节点
                         */
                        if(treeMap.hasParents(node.id, targetNode.id))
                        {
                            return;
                        }
                        /*****
                         * 剔除当前节点在父节点的子节点列表中位置
                         */
                        var lastParentId = node.parentId;
                        var nodeId = node.id;
                        var parentNode = treeMap.findNode(lastParentId);
                        if(parentNode && parentNode.childIds)
                        {
                            var idx = parentNode.childIds.indexOf(nodeId);
                            if(idx > -1)
                            {
                                parentNode.childIds.splice(idx, 1);
                            }
                        }
                        /**
                         * 目标节点的子节点列表中增加当前节点
                         */
                        var childIds = targetNode.childIds || [];
                        childIds.push(nodeId);
                        targetNode.childIds = childIds;

                        node.parentIds = [targetNode.id];
                        node.parentId = targetNode.id;

                        orgGraph.refreshByNodeId(targetNode.id, treeMap);
                        orgGraph.refreshByNodeId(lastParentId, treeMap);
                    }
                },
                hideMenu4DragEnd: {
                    action: function(node, treeMap, orgGraph, targetNode)
                    {

                    }
                }
            };

            if(me.userInfo.familyCulturePath)
            {
                oui.ajax({
                    progressBar: '加载中...',
                    url: $Api.readFile,
                    postParam: {
                        userId: me.userInfo.userId,
                        tokenId: me.userInfo.tokenId,
                        targetUserId: me.userInfo.userId,
                        path: me.userInfo.familyCulturePath
                    },
                    callback: function(res)
                    {
                        if(res.success)
                        {
                            var tree = oui.parseJson(res.content);
                            var ids = tree.ids || [];
                            oui.eachArray(ids, function(item)
                            {
                                var curr = tree.map[item];
                                var node = curr.node;
                                if(node.avatar)
                                {
                                    var tempUrl = oui.setParam(node.avatar, 'tokenId', me.userInfo.tokenId);
                                    node.avatar = tempUrl;
                                }
                                if(node.spouse)
                                {
                                    if(node.spouse.avatar)
                                    {
                                        var tempUrl = oui.setParam(node.spouse.avatar, 'tokenId', me.userInfo.tokenId);
                                        node.spouse.avatar = tempUrl;

                                    }
                                }

                            });

                            me.treeMap = me.newTreeMap(tree, 'id', 'parentId', 'name');
                            oui.parse({
                                callback: function()
                                {
                                    me.bindEvents();
                                }
                            });
                        } else
                        {
                            me.data.menu.circleId = urlParams.circleId || '';
                            me.init4default();
                            oui.parse({
                                callback: function()
                                {
                                    me.bindEvents();
                                }
                            });
                        }

                    }
                });

            } else
            {
                me.data.menu.circleId = urlParams.circleId || '';
                me.init4default();
                oui.parse({
                    callback: function()
                    {
                        me.bindEvents();
                    }
                });
                //var sysId = me.getSysId();
                //oui.db.sys_menu.selectOne(sysId,function(res){
                //    if(!res){
                //        me.init4default();
                //    }else{
                //        var lastNode =res;
                //        lastNode.map['root'].node.name = me.getSysName(); //根节点的名称修改
                //        me.treeMap = me.newTreeMap(res);
                //    }
                //    //新增

                //},function(){
                //});
                //创建一个新节点
            }

            window.onbeforeunload = function(e)
            {
                if(me.hasSaveData())
                {
                    return;
                }
                return (e || window.event).returnValue = '有信息未保存';
                // return confirm("确定退出吗");
            };
        },
        init: function()
        {
            var me = this;
            oui.getUserInfo(function(userInfo)
            {
                if(userInfo)
                {
                    me.userInfo = userInfo;
                    me.initData();
                } else
                {
                    //登录页面
                    location.href = 'logReg.html';
                }
            });
        },
        init4default: function()
        {
            var me = this;
            //var sysId = me.getSysId();
            var rootId = oui.getUUIDLong();
            var map = {};
            map[rootId] = {
                "id": rootId, "joinId": "", "prevId": "", "parentId": "", "parentIds": [], "childIds": [],
                "node": {
                    "id": rootId,
                    "name": "根节点",
                    "parentId": "",
                    "nodeType": "root",
                    "prevId": "",
                    "joinId": ""
                }, "unExpand": false
            };
            me.treeMap = me.newTreeMap({
                id: rootId,
                "clickName": "click",
                "idKey": "id",
                "parentIdKey": "parentId",
                "nameKey": "name",
                "ids": [rootId],
                "rootIds": [rootId],
                "map": map, "direction": ""
            });
        },
        /**获取节点的最小宽度 **/
        findNodeMinWidth: function(nodeId)
        {
            //节点+爱人节点+爱人间距+节点间距+兄弟节点
            var nodeWidth = 100;
            var spousePadding = 30;
            var brotherPadding = 48;
            var spousePaddingBefore = 20;
            var spousePaddingAfter = 20;
            var result = 0;
            var me = this;
            var ids = me.treeMap.findChildIds(nodeId);
            var isExpand = false;
            if(me.treeMap.isExpand(nodeId))
            {
                isExpand = true;
                for(var i = 0, len = ids.length; i < len; i++)
                {
                    var spouse = me.findSpouse(ids[i], me.treeMap);
                    if(spouse.length)
                    { //爱人间距
                        result += (spouse.length * (nodeWidth + spousePadding));
                    }
                    result += brotherPadding;//兄弟节点间距
                    result += nodeWidth; //增加节点距离
                }
            }

            /**判断爱人节点数是否 大于子节点数 */
            var spouse = me.findSpouse(nodeId, me.treeMap);

            if((spouse.length >= ids.length) || (spouse.length && (!isExpand)))
            { //爱人节点数大于子节点数
                var temp = (nodeWidth + spousePaddingBefore + spousePaddingAfter) + (spouse.length * (nodeWidth + spousePadding));
                if(result < temp)
                {
                    result = temp;
                }
            }
            if(result == 0)
            {
                result = (nodeWidth + brotherPadding);
            }
            return result;
        },
        findSpouseNodeName: function(nodeId, spouseId)
        {
            var me = this;
            var spouse = me.findSpouse(nodeId, me.treeMap);
            var one = oui.findOneFromArrayBy(spouse, function(item)
            {
                if(item.id == spouseId)
                {
                    return true;
                }
            });
            if(one)
            {
                return one.name || '';
            }
            return '';
        },
        /**根据父节点 获取 族谱中没行第一个节点 到 中点的位置，用于绘制横线 **/
        findSpouseLength: function(nodeId)
        {
            //根据节点id获取 爱人列表，取数据的中线
            var me = this;
            var spouse = me.findSpouse(nodeId, me.treeMap) || [];
            return spouse.length;
        },
        findCenterLineStyle: function(nodeId, width, padding)
        {
            var me = this;
            var len = me.findSpouseLength(nodeId) + 1;
            var tempWidth = (width + padding) * len / 2.0 - (width / 2.0) - padding / 2.0;
            tempWidth = tempWidth.toFixed(2);
            return "width:" + tempWidth + "px";
        },
        findSourceNodeSpouse: function(nodeId, spouseId)
        {
            var me = this;
            var spouse = me.findSpouse(nodeId, me.treeMap);
            var one = oui.findOneFromArrayBy(spouse, function(item)
            {
                if(item.id == spouseId)
                {
                    return true;
                }
            });
            return one || {};
        },
        findSpouse: function(nodeId, treeMap)
        {
            var sourceNode = treeMap.findSourceNode(nodeId);
            return sourceNode.spouse || [];
        },
        findSpouseLeft: function(nodeId, treeMap)
        {
            var sourceNode = treeMap.findSourceNode(nodeId);
            var arr = sourceNode.spouse || [];
            var temp = [];
            if(arr.length)
            {
                for(var i = 0, len = arr.length; i < len; i++)
                {
                    if(i % 2 == 1)
                    {
                        temp.push(arr[i]);
                    }
                }
            }
            return temp;
        },
        findSpouseRight: function(nodeId, treeMap)
        {
            var sourceNode = treeMap.findSourceNode(nodeId);
            var arr = sourceNode.spouse || [];
            var temp = [];
            if(arr.length)
            {
                for(var i = 0, len = arr.length; i < len; i++)
                {
                    if(i % 2 == 0)
                    {
                        temp.push(arr[i]);
                    }
                }
            }
            return temp;
        },
        //获取奇数 标记
        findSpouseOdd: function(nodeId, treeMap)
        {
            var me = this;
            var ids = me.treeMap.findChildIds(nodeId);
            if(ids.length == 1)
            {
                var sourceNode = treeMap.findSourceNode(nodeId);
                var arr = sourceNode.spouse || [];
                return (arr.length % 2) == 1 ? 'spouse-odd' : 'spouse-even';//奇数 偶数标记
            }
            return '';
        },
        isSingle: function(nodeId, treeMap)
        {
            var me = this;
            var hs = me.findSpouse(nodeId, treeMap);
            var flag = true;
            if(hs && hs.length)
            {
                flag = false;
                return flag;
            }
            return flag;

        },

        //获取当前编辑弹窗中的信息
        getCurrNodeEdit: function()
        {
            var $modal = $('#node-edit-modal');
            var name = $modal.find('[name="name"]').val();
            var sex = $modal.find('[name="sex"]').val();
            var age = $modal.find('[name="age"]').val();
            var birthDate = $modal.find('[name="birthDate"]').val();
            var dateOrDeath = $modal.find('[name="dateOrDeath"]').val();
            var personState = $modal.find('[name="personState"]').val();
            var cd = $modal.find('[name="cd"]').val();
            var lssj = $modal.find('[name="lssj"]').val();
            var bz = $modal.find('[name="bz"]').val();

            return {
                name: name,
                age: age,
                sex: sex,
                birthDate: birthDate,
                dateOrDeath: dateOrDeath,
                personState: personState,
                cd: cd,
                lssj: lssj,
                bz: bz
            };
        },

        clearCurrNodeEdit: function()
        {

        },

        //普通节点，选择图片
        event2ChooseAvatar: function(cfg)
        {
            var me = this;
            var nodeId = cfg.el.getAttribute('node-id');
            var files = cfg.el.files;
            var imgFile = files[0];
            me.tempAvatar = imgFile;

            var sourceNode = me.treeMap.findSourceNode(nodeId);
            var editNode = me.getCurrNodeEdit();
            for(var k in editNode)
            {
                sourceNode[k] = editNode[k];
            }

            var imgUrl = window.URL.createObjectURL(imgFile);
            me.treeMap.findSourceNode(nodeId).avatar = imgUrl;
            var view = oui.getById("family-tree");
            var html = view.getHtmlByTplId('node-edit-options-tpl', {
                treeMap: me.treeMap,
                nodeId: nodeId
            });
            document.getElementById('node-edit-modal').outerHTML = html;

        },

        //添加爱人节点，选择图片
        event2ChooseAvatar4Spouse: function(cfg)
        {
            console.log(arguments);
            var me = this;
            var nodeId = cfg.el.getAttribute('node-id');
            var spouseId = cfg.el.getAttribute('spouse-id');

            var files = cfg.el.files;
            var imgFile = files[0];
            me.tempAvatar = imgFile;
            console.log(files);

            var sourceNode = me.findSourceNodeSpouse(nodeId, spouseId);
            var editNode = me.getCurrNodeEdit();
            for(var k in editNode)
            {
                sourceNode[k] = editNode[k];
            }

            var imgUrl = window.URL.createObjectURL(imgFile);
            me.findSourceNodeSpouse(nodeId, spouseId).avatar = imgUrl;

            var view = oui.getById("family-tree");
            var html = view.getHtmlByTplId('node-edit-spouse-options-tpl', {
                treeMap: me.treeMap,
                nodeId: nodeId,
                spouseId: spouseId
            });

            document.getElementById('node-edit-modal').outerHTML = html;
        },

        /** 节点属性弹窗-保存  */
        event2saveNodeOptions: function(cfg)
        {
            var me = this;
            var el = cfg.el;
            var nodeId = el.getAttribute('node-id');
            console.log(nodeId);
            console.log(me.treeMap);
            //var $file = $('#node-edit-modal input[name=avatar]');

            //if($file[0].files && ($file[0].files.length > 0))
            if(me.tempAvatar)
            {
                var userInfo = oui.storage.get('userInfo') || {};
                userInfo = oui.parseJson(userInfo);
                if(!userInfo.userId || !userInfo.tokenId)
                {
                    alert('当前登录已失效，请重新登录');
                    return;
                }
                //var avatarFile = $file[0].files[0];
                var avatarFile = me.tempAvatar;
                var formData = new FormData();
                formData.append('userId', userInfo.userId);
                formData.append('tokenId', userInfo.tokenId);
                formData.append('targetUserId', userInfo.userId);
                formData.append('file', avatarFile);

                $.ajax({
                    url: $Api.uploadFile,
                    type: 'POST',
                    mimeType: "multipart/form-data",
                    cache: false,         //不设置缓存
                    processData: false,  // 不处理数据
                    dataType: 'json',
                    contentType: false,   // 不设置内容类型
                    data: formData,
                    success: function(res)
                    {
                        console.log(res);
                        res = oui.parseJson(res);
                        var imgParam = {
                            userId: userInfo.userId,
                            tokenId: userInfo.tokenId,
                            targetUserId: userInfo.userId,
                            path: res.data[0]
                        };
                        var imgParamStr = $.param(imgParam);
                        var imgUrl = $Api.viewImage + '?' + imgParamStr;

                        var $modal = $('#node-edit-modal');
                        var avatar = imgUrl;
                        $modal.hide();

                        me.tempAvatar = null;
                        delete me.tempAvatar;

                        var sourceNode = me.treeMap.findSourceNode(nodeId);
                        var editNode = me.getCurrNodeEdit();

                        sourceNode.name = editNode.name;
                        sourceNode.sex = editNode.sex;
                        sourceNode.age = editNode.age;
                        sourceNode.avatar = avatar;
                        sourceNode.birthDate = editNode.birthDate;
                        sourceNode.dateOrDeath = editNode.dateOrDeath;
                        sourceNode.personState = editNode.personState;
                        sourceNode.cd = editNode.cd;
                        sourceNode.lssj = editNode.lssj;
                        sourceNode.bz = editNode.bz;
                        if(sourceNode.jf)
                        {
                            sourceNode.jf += 1;
                        } else
                        {
                            sourceNode.jf = 1;
                        }

                        me.editOldNode = null;
                        delete me.editOldNode;


                        me.refreshByNodeId(nodeId, me.treeMap);

                    }
                });

            } else
            {
                var $modal = $('#node-edit-modal');
                $modal.hide();

                var sourceNode = me.treeMap.findSourceNode(nodeId);
                var editNode = me.getCurrNodeEdit();

                sourceNode.name = editNode.name;
                sourceNode.sex = editNode.sex;
                sourceNode.age = editNode.age;
                sourceNode.birthDate = editNode.birthDate;
                sourceNode.dateOrDeath = editNode.dateOrDeath;
                sourceNode.personState = editNode.personState;
                sourceNode.cd = editNode.cd;
                sourceNode.lssj = editNode.lssj;
                sourceNode.bz = editNode.bz;
                if(sourceNode.jf)
                {
                    sourceNode.jf += 1;
                } else
                {
                    sourceNode.jf = 1;
                }

                me.editOldNode = null;
                delete me.editOldNode;
                me.refreshByNodeId(nodeId, me.treeMap);
            }

        },

        //节点属性弹窗-保存爱人信息
        event2saveSpouseNodeOptions: function(cfg)
        {
            var me = this;
            var el = cfg.el;
            var nodeId = el.getAttribute('node-id');
            console.log(nodeId);
            console.log(me.treeMap);
            var spouseId = el.getAttribute('node-hw-id');

            if(me.tempAvatar)
            {
                var userInfo = oui.storage.get('userInfo') || {};
                userInfo = oui.parseJson(userInfo);
                if(!userInfo.userId || !userInfo.tokenId)
                {
                    alert('当前登录已失效，请重新登录');
                    return;
                }
                //var avatarFile = $file[0].files[0];
                var avatarFile = me.tempAvatar;
                var formData = new FormData();
                formData.append('userId', userInfo.userId);
                formData.append('tokenId', userInfo.tokenId);
                formData.append('targetUserId', userInfo.userId);
                formData.append('file', avatarFile);

                $.ajax({
                    url: $Api.uploadFile,
                    type: 'POST',
                    mimeType: "multipart/form-data",
                    cache: false,         //不设置缓存
                    processData: false,  // 不处理数据
                    dataType: 'json',
                    contentType: false,   // 不设置内容类型
                    data: formData,
                    success: function(res)
                    {
                        console.log(res);
                        res = oui.parseJson(res);
                        var imgParam = {
                            userId: userInfo.userId,
                            tokenId: userInfo.tokenId,
                            targetUserId: userInfo.userId,
                            path: res.data[0]
                        };
                        var imgParamStr = $.param(imgParam);
                        var imgUrl = $Api.viewImage + '?' + imgParamStr;

                        var $modal = $('#node-edit-modal');
                        $modal.hide();

                        me.tempAvatar = null;
                        delete me.tempAvatar;

                        var sourceNode = me.findSourceNodeSpouse(nodeId, spouseId);
                        var editNode = me.getCurrNodeEdit();

                        sourceNode.name = editNode.name;
                        sourceNode.sex = editNode.sex;
                        sourceNode.age = editNode.age;
                        sourceNode.avatar = imgUrl;
                        sourceNode.birthDate = editNode.birthDate;
                        sourceNode.dateOrDeath = editNode.dateOrDeath;
                        sourceNode.personState = editNode.personState;
                        sourceNode.cd = editNode.cd;
                        sourceNode.lssj = editNode.lssj;
                        sourceNode.bz = editNode.bz;

                        me.refreshByNodeId(nodeId, me.treeMap);

                    }
                });

            } else
            {
                var $modal = $('#node-edit-modal');
                $modal.hide();

                var sourceNode = me.findSourceNodeSpouse(nodeId, spouseId);
                var editNode = me.getCurrNodeEdit();

                sourceNode.name = editNode.name;
                sourceNode.sex = editNode.sex;
                sourceNode.age = editNode.age;
                sourceNode.birthDate = editNode.birthDate;
                sourceNode.dateOrDeath = editNode.dateOrDeath;
                sourceNode.personState = editNode.personState;
                sourceNode.cd = editNode.cd;
                sourceNode.lssj = editNode.lssj;
                sourceNode.bz = editNode.bz;

                me.refreshByNodeId(nodeId, me.treeMap);

            }


        },

        /** 节点属性弹窗-隐藏  */
        event2hideNodeModal: function()
        {
            var me = this;
            if(this.tempAvatar)
            {
                delete this.tempAvatar;
            }
            if(me.editOldNode)
            {
                var sourceNode = me.treeMap.findSourceNode(me.editOldNode.id);
                for(var k in me.editOldNode)
                {
                    sourceNode[k] = me.editOldNode[k];
                }
                me.editOldNode = null;
                delete me.editOldNode;
                me.refreshByNodeId(sourceNode.id, me.treeMap);
            }
            var $modal = $('#node-edit-modal');
            $modal.hide();
        },

        destroy: function()
        {
            //页面销毁触发
            var me = this;
            template.helper('FamilyTree', null);
            me.data = null;
            oui.biz.Tool.Clear(me);
        },
        /** 根据输入变量获取控件类型***/
        findControlType4param: function(propDefine)
        {
            var dataTypeEnum = oui.dataTypeEnum[propDefine.dataType]
            return dataTypeEnum.controlType || "textfield";
        },
        /** 根据输入变量获取显示类型***/
        findShowType4param: function(propDefine)
        {
            var dataTypeEnum = oui.dataTypeEnum[propDefine.dataType]
            var showType = dataTypeEnum.showType;
            if(typeof showType != 'undefined')
            {
                return showType;
            }
            showType = 0;
            return showType;
        },
        /** 根据输入变量获取输入校验*****/
        findValidate4param: function(propDefine)
        {
            return {};
        },
        /** 绑定拖拽事件****/
        bindEvents: function()
        {
            /*var dragEventsMap = {
             'dragstart': oui.os.mobile ? 'touchstart' : 'dragstart',
             'dragover': oui.os.mobile ? 'touchmove' : 'dragover',
             'dragend': oui.os.mobile ? 'touchend' : 'dragend',
             'drop': oui.os.mobile ? 'touchend' : 'drop',
             'mousedown': oui.os.mobile ? 'touchstart' : 'mousedown',
             'mousemove': oui.os.mobile ? 'touchmove' : 'mousemove',
             'mouseup': oui.os.mobile ? 'touchend' : 'mouseup'
             };*/

            var dragEventsMap = {
                'dragstart': 'dragstart',
                'dragover': 'dragover',
                'dragend': 'dragend',
                'drop': 'drop',
                'mousedown': 'mousedown',
                'mousemove': 'mousemove',
                'mouseup': 'mouseup'
            };

            var me = this;
            me.dragData = {};
            $(document).on(dragEventsMap.dragstart, '#control_family-tree .family-tree .node', function(e)
            {
                me.dragData.fromNodeId = '';
                me.dragData.toNodeId = '';
                me.dragData.timer4dragend = null;
                var nodeId = $(e.target).attr('node-id');
                if(!nodeId)
                {
                    return;
                }
                $('.second-menu').remove();
                me.dragData.fromNodeId = nodeId;
                $('.node', '.family-tree').removeClass('allowedDrop');
            });
            $(document).on(dragEventsMap.dragover, '#control_family-tree .family-tree .node', function(e)
            {
                e.preventDefault();
                var nodeId = $(e.target).attr('node-id');
                if(!nodeId)
                {
                    return;
                }
                //console.log('dragover:'+nodeId);

                //$(e.target).addClass('');
            });
            $(document).on(dragEventsMap.dragend, '#control_family-tree .family-tree .node', function(e)
            {
                var nodeId = $(e.target).attr('node-id');
                if(!nodeId)
                {
                    return;
                }
                if(me.dragData.timer4dragend)
                {
                    try
                    {
                        clearTimeout(me.dragData.timer4dragend);
                    } catch(err)
                    {
                    }
                    me.dragData.timer4dragend = null;
                }
                me.dragData.timer4dragend = setTimeout(function()
                {
                    console.log('drag end,from:' + me.dragData.fromNodeId + ',to:' + me.dragData.toNodeId);

                    $('.node', '.family-tree').removeClass('allowedDrop');
                    if(me.dragData.fromNodeId == me.dragData.toNodeId)
                    {
                        return;
                    }
                    if(me.dragData.fromNodeId && me.dragData.toNodeId)
                    {
                        $('.node[node-id=' + me.dragData.fromNodeId + '],.node[node-id=' + me.dragData.toNodeId + ']').addClass('allowedDrop');
                    }
                    me.dragEnd(me.dragData.fromNodeId, me.dragData.toNodeId);

                    me.dragData.timer4dragend = null;
                }, 1);

            });
            $(document).on(dragEventsMap.drop, function(e)
            {
                var target = e.target;
                if(!$(target).hasClass('node'))
                {
                    try
                    {
                        target = $(target).closest('.node')[0];
                    } catch(err)
                    {
                    }
                    if(!target)
                    {
                        return;
                    }
                }
                var nodeId = $(target).attr('node-id');
                if(!nodeId)
                {
                    return;
                }
                me.dragData.toNodeId = nodeId;
                //console.log(e);
                //console.log('drop:'+nodeId);

            });

            $(document).on(dragEventsMap.mousedown, function(e)
            {

                if($(e.target).closest('.node').length)
                {
                    return;
                }
                if($(e.target).closest('.second-menu').length || ($(e.target).is('.second-menu')))
                {
                    return;
                }
                me.hideMenu && me.hideMenu();
            });
            try
            {
                $(document).off(dragEventsMap.mousedown, '#control_family-tree .family-tree-graph');
            } catch(err)
            {
            }
            /*** 拖拽事件处理***/
            $(document).on(dragEventsMap.mousedown, '#control_family-tree .family-tree-graph', function(e)
            {
                var $this = $('.family-tree');
                if($(e.target).closest('.node').length)
                {
                    $this.data('panning', false);
                    //e.preventDefault();
                    return;
                } else
                {
                    $this.css('cursor', 'move').data('panning', true);

                }
                var lastX = 0;
                var lastY = 0;
                var lastTf = $this.css('transform') || "none";
                if(lastTf !== 'none')
                {
                    var temp = lastTf.split(',');
                    if(lastTf.indexOf('3d') === -1)
                    {
                        lastX = parseInt(temp[4]);
                        lastY = parseInt(temp[5]);
                    } else
                    {
                        lastX = parseInt(temp[12]);
                        lastY = parseInt(temp[13]);
                    }
                }
                var startX = e.pageX - lastX;
                var startY = e.pageY - lastY;

                $(document).on(dragEventsMap.mousemove, function(ev)
                {
                    var newX = ev.pageX - startX;
                    var newY = ev.pageY - startY;
                    //if(newY < 0)
                    //{
                    //    newY = 0;
                    //}
                    var lastTf = $this.css('transform') || 'none';
                    if(lastTf === 'none')
                    {
                        if(lastTf.indexOf('3d') === -1)
                        {
                            $this.css('transform', 'matrix(1, 0, 0, 1, ' + newX + ', ' + newY + ')');
                        } else
                        {
                            $this.css('transform', 'matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, ' + newX + ', ' + newY + ', 0, 1)');
                        }
                    } else
                    {
                        var matrix = lastTf.split(',');
                        if(lastTf.indexOf('3d') === -1)
                        {
                            matrix[4] = ' ' + newX;
                            matrix[5] = ' ' + newY + ')';
                        } else
                        {
                            matrix[12] = ' ' + newX;
                            matrix[13] = ' ' + newY;
                        }
                        $this.css('transform', matrix.join(','));
                    }
                });
            });
            $(document).on(dragEventsMap.mouseup, function(e)
            {
                var $chart = $('.family-tree');
                if($chart.data('panning'))
                {
                    $chart.css('cursor', 'default');
                    $(this).off('mousemove');
                    $chart.data('panning', false);
                }
            });

            me.familyTreeDragableInMobile();    //移动端拖拽
        },

        //移动端拖拽
        familyTreeDragableInMobile: function()
        {
            var $tree = $('.family-tree');
            var block = $tree[0];
            var oW, oH;

            $(document).on('touchstart', '.family-tree', function(e)
            {
                $tree = $('.family-tree');
                block = $tree[0];
                var touches = e.touches[0];
                oW = touches.pageX - block.offsetLeft;
                oH = touches.pageY - block.offsetTop;
                //阻止页面的滑动默认事件
                e && e.preventDefault();
                $(document).on('touchmove', '.family-tree', function(e)
                {
                    var touches = e.touches[0];
                    var oLeft = touches.pageX - oW;
                    var oTop = touches.pageY - oH;
                    block.style.left = oLeft + "px";
                    block.style.top = oTop + "px";
                });
            });
            //block.addEventListener("touchstart", function(e)
            //{
            //
            //}, false);


            $(document).on('touchend', '.family-tree', function(e)
            {
                e && e.preventDefault();
                $(document).off('touchmove', '.family-tree');
            });


        },

        hideMenu: function()
        {
            $('.node', '#control_family-tree .family-tree').removeClass('allowedDrop');
            $('.allowedDropTarget', '#control_family-tree .family-tree').removeClass('allowedDropTarget');
            $('#control_family-tree .second-menu').remove();
        },
        /****
         * 显示页面设计
         * @param id
         */
        showPageDesign: function(id)
        {
            var me = this;

            var loadUrl = oui.biz.Tool.getApiPathByController(me.FullName.replace('4Design', ''), 'loadPageDesign');
            oui.getData(me.data.loadPageDesignUrl || loadUrl, {
                id: id
            }, function(res)
            {
                var page = res.page || {};

                var controls = page.controls || [];
                controls = oui.parseJson(controls);
                page.controls = controls;
                page.style = oui.parseJson(page.style);
                page.innerStyle = oui.parseJson(page.innerStyle);
                //page.pageDesignType = 'normalForm';
                var treeNode = me.treeMap.findNode(id);
                var node = treeNode.node;
                node.parentId = treeNode.parentId;
                node.id = treeNode.id;
                oui.showPageDesign({
                    designTplUrl: oui.getContextPath() + 'res_engine/page_design/common/runtime/page-runtime-edit-tpl.html',
                    uploadUrl: oui.uploadURL,//跨静态页面传值，用于上传组件
                    /**必须参数*/
                    controls: [],//已有的控件列表
                    mainTemplate: '',//页面的业务属性面板模板获取方法
                    bizJs: [oui.getContextPath() + 'res_apps/menu/js/page-design/page-design-plugin.js'],
                    saveCallBack: 'saveCallBack',
                    /**非必须参数*/
                    viewType: 'openWindow',
                    buttons: "save", //按钮参数,如save,print 等按钮事件名
                    useControls: false,//使用传入控件列表作为 待设计控件区域,否则待选区域的控件全控件列表
                    page: page,//页面设计对象，如果是打印则是打印模板设计对象
                    scriptPkg: "com.oui.DesignBiz", //指定扩展脚本包,为了避免命名空间与页面设计的命名冲突，指定命名空间，实现业务与设计的代码命名隔离，api调用不冲突
                    bizCss: [],
                    canCloneControl: true,//是否可以复制控件
                    //其它业务参数调用传递,重要提示：cfg.params的参数最好使用简单map对象，不能包含任何dom对象或window对象等
                    params: {
                        nodeJson: oui.parseString(node),
                        saveUrl: me.data.savePageDesignUrl || oui.biz.Tool.getApiPathByController(me.FullName.replace('4Design', ''), 'savePageDesign')
                    }
                });
            }, '设计加载中...', function(res)
            {
                oui.getTop().oui.alert('由于网络原因，加载失败');
            });

        },
        /**
         * 根据节点 和树对象刷新某个节点和节点下面的所有节点
         * @node
         * @treeMap
         * ***/
        refreshByNodeId: function(nodeId, treeMap)
        {
            var view = oui.getById('family-tree');
            var html = view.getHtmlByTplId('menu-table-tpl', {
                treeMap: treeMap,
                nodeId: nodeId
            });
            var $table = $('[table-node-id=' + nodeId + ']', '#control_family-tree .family-tree');
            $table[0].outerHTML = html;
        },
        refreshByRoot: function(treeMap)
        {
            var view = oui.getById('family-tree');
            view.render();
        },
        newTreeMap: function(treeMapObj, idKey, parentIdKey, nameKey)
        {
            var me = this;
            treeMapObj.idKey = idKey || 'id';
            treeMapObj.parentIdKey = parentIdKey || 'parentId';
            treeMapObj.nameKey = nameKey || 'name';
            treeMapObj.clickName = oui.os.mobile ? 'tap' : 'click';
            var treeMap = com.oui.TreeMap.newTreeMap(treeMapObj);
            return treeMap;
        },
        array2orgTreeMap: function(arr, idKey, parentIdKey, nameKey)
        {
            var me = this;
            var treeMap = com.oui.TreeMap.array2treeMap(arr, idKey, parentIdKey, nameKey);
            return treeMap;
        },
        expandChildren: function(cfg)
        {
            cfg.e.stopPropagation && cfg.e.stopPropagation();
            var $node = $(cfg.el).closest('.node[node-id]');
            var nodeId = $node.attr('node-id');
            var me = this;
            var $table = $(('div>table[table-node-id=' + nodeId + ']'));
            var lastPos = $table.offset();
            me.treeMap.expandChildren(nodeId);
            var parentNode = me.treeMap.findParent(nodeId);
            if(parentNode && parentNode.id)
            {
                me.refreshByNodeId(parentNode.id, me.treeMap);
            } else
            {
                me.refreshByRoot(me.treeMap);
            }
            $table = $(('div>table[table-node-id=' + nodeId + ']'));
            var currPos = $table.offset();

            var width = $table.width();
            var $this = $('.family-tree');
            var leftFix = lastPos.left - currPos.left;

            var topFix = lastPos.top - currPos.top;

            //折叠后 向右偏移
            if(oui.os.mobile)
            {

            } else
            {

            }
            var dom = $(('div>table[table-node-id=' + nodeId + ']'))[0];

            me.scroll2dom(dom);


        },
        unExpandChildren: function(cfg)
        {
            cfg.e.stopPropagation && cfg.e.stopPropagation();
            var $node = $(cfg.el).closest('.node[node-id]');
            var nodeId = $node.attr('node-id');
            var me = this;
            var $table = $(('div>table[table-node-id=' + nodeId + ']'));
            var lastPos = $table.offset();
            me.treeMap.unExpandChildren(nodeId);
            var parentNode = me.treeMap.findParent(nodeId);
            if(parentNode && parentNode.id)
            {
                me.refreshByNodeId(parentNode.id, me.treeMap);
            } else
            {
                me.refreshByRoot(me.treeMap);
            }

            $table = $(('div>table[table-node-id=' + nodeId + ']'));
            var currPos = $table.offset();

            var width = $table.width();
            var $this = $('.family-tree');
            var leftFix = lastPos.left - currPos.left;
            var topFix = lastPos.top - currPos.top;

            //折叠后 向右偏移
            if(oui.os.mobile)
            {
            } else
            {
                //pc端偏移处理

            }
            var dom = $(('div>table[table-node-id=' + nodeId + ']'))[0];

            me.scroll2dom(dom);
        },
        scroll2dom:function(dom){
            ScrollToControl(dom);
        },
        /** 数据返回***/
        getData: function()
        {
            var me = this;
            return me.data;
        },
        /** 方法处理方法开始......******/

        /******
         * 拖拽结束后 两个节点 处理逻辑
         * @param nodeId
         * @param targetNodeId
         */
        dragEnd: function(nodeId, targetNodeId)
        {
            var me = this;
            if(!nodeId)
            {
                return;
            }
            if(!targetNodeId)
            {
                return;
            }

            //当前不提供 拖拽功能
            var hasMenus = false;
            if(!hasMenus)
            {
                me.hideMenu();
                return;
            }

            var currNode = me.treeMap.findNode(nodeId);
            var targetNode = me.treeMap.findNode(targetNodeId);
            var view = oui.getById('family-tree');
            var $node = $('.node[node-id=' + targetNodeId + ']', '.family-tree');


            var html = view.getHtmlByTplId('node-menu-dragend-tpl', {
                nodeId: nodeId,
                targetNodeId: targetNodeId,
                treeMap: me.treeMap
            });
            $('.second-menu').remove();
            $('.family-tree').closest('.family-tree-graph').append(html);
            oui.follow4fixed($node.find('.title')[0], $('.second-menu')[0], true);
        },

        /** 事件处理方法开始......******/

        event2showMenu: function(cfg)
        {
            var me = this;
            var nodeId = $(cfg.el).attr('node-id');
            var view = oui.getById('family-tree');
            $('.allowedDrop', '.family-tree').removeClass('allowedDrop');
            $('.second-menu').remove();
            var spouseId = $(cfg.el).attr('node-hw-id');
            var html = '';
            if(spouseId)
            { //爱人节点
                html = view.getHtmlByTplId('node-menu-tpl', {
                    nodeId: nodeId,
                    spouseId: spouseId,
                    treeMap: me.treeMap
                });
            } else
            {
                html = view.getHtmlByTplId('node-menu-tpl', {
                    nodeId: nodeId,
                    treeMap: me.treeMap
                });
            }

            $('.family-tree').closest('.family-tree-graph').append(html);
            oui.follow4fixed(cfg.el, $('.second-menu')[0], true);
        },
        event2menuAction: function(cfg)
        {
            cfg.e.stopPropagation && cfg.e.stopPropagation();
            var me = this;
            var menuId = $(cfg.el).attr('menu-action-id');
            if(!me.menuActionEnum[menuId])
            {
                return;
            }
            var spouseId = $(cfg.el).attr('node-hw-id');
            var nodeId = $(cfg.el).attr('node-id');
            var targetNodeId = $(cfg.el).attr('target-node-id');
            var treeMap = me.treeMap;
            var node = treeMap.findNode(nodeId);
            var targetNode = null;
            if(targetNodeId)
            {
                targetNode = treeMap.findNode(targetNodeId);
            }
            var currActionEnum = me.menuActionEnum[menuId];

            if(currActionEnum.before)
            {
                var flag = currActionEnum.before(node, treeMap, me, targetNode, spouseId);
                if(typeof flag == 'boolean')
                {
                    if(!flag)
                    {
                        return;
                    }
                }
            }
            var params = {
                menuId: me.data.menu.id,
                circleId: me.data.menu.circleId
            };
            if(currActionEnum.getApiParams)
            {
                params = $.extend(true, params, currActionEnum.getApiParams(node, treeMap, me, targetNode, spouseId));
            }

            if(currActionEnum.api)
            {
                var path = oui.biz.Tool.getApiPathByController(me.FullName.replace('4Design', ''), currActionEnum.api);
                var apiMap = me.data.apiMap;

                if(apiMap && apiMap[currActionEnum.api])
                {
                    oui.postData(apiMap[currActionEnum.api] || path, params, function(res)
                    {
                        if(res.success)
                        {
                            currActionEnum.action(node, treeMap, me, targetNode, params, res, spouseId);
                            //添加完成后，执行设计保存
                            me.saveDesign();
                        } else
                        {
                            oui.getTop().oui.alert(res.msg || ((currActionEnum.display || '') + '处理失败'));
                        }
                    }, function(res)
                    {
                        oui.getTop().oui.alert(res.msg || ('由于网络原因：' + (currActionEnum.display || '') + '处理失败'));
                    });
                } else
                {
                    currActionEnum.action(node, treeMap, me, targetNode, params, {}, spouseId);
                    //添加完成后，执行设计保存
                    me.saveDesign();
                }
            } else
            {
                currActionEnum.action(node, treeMap, me, targetNode, params, {}, spouseId);
                me.saveDesign();
            }
            $('.allowedDrop', '.family-tree').removeClass('allowedDrop');
            $('.second-menu').remove();
            return false;
        },
        event2editNodeName: function(cfg)
        {
            var nodeId = $(cfg.el).attr('node-id');
            var me = this;
            var view = oui.getById('family-tree');
            var html = view.getHtmlByTplId('node-name-edit-tpl', {
                nodeId: nodeId,
                treeMap: me.treeMap
            });
            $(cfg.el).attr('draggable', false);
            $(cfg.el).append(html);
            $(cfg.el).find('.title').hide();
            $(cfg.el).find('input').focus();
        },
        event2updateCurrNodeName: function(cfg)
        {
            var nodeId = $(cfg.el).attr('node-id');
            var me = this;
            if(me.data.menu.id == nodeId)
            {
                me.data.menu.name = $(cfg.el).val();
            }
            me.treeMap.updateNodeName(nodeId, $(cfg.el).val());
            me.refreshByNodeId(nodeId, me.treeMap);

        },
        //导出图片
        event2exportGraph: function(cfg)
        {
            var $chartContainer = $('.family-tree');
            var me = this;
            var treeMap = me.treeMap;
            if(!treeMap.direction)
            {
                treeMap.direction = '';
            }
            var name = me.treeMap.findRoot().node.name + '.png';
            com.oui.TreeMap.exportGraph(treeMap, $chartContainer, name, function()
            {
                $chartContainer.removeClass('canvasContainer');
            });

        },

        //保存系统
        event2saveGraph: function(cfg)
        {
            var me = this;
            var userInfo = me.userInfo;

            var nodeJSON = me.treeMap;
            var treeMapJSON = new Blob([oui.parseString(nodeJSON)], {"type": "text/json;charset=utf-8"});
            var formData = new FormData();

            formData.append('userId', userInfo.userId);
            formData.append('tokenId', userInfo.tokenId);
            formData.append('targetUserId', userInfo.userId);
            formData.append('file', treeMapJSON, 'treeMap.json');

            if(userInfo.familyCulturePath)
            {
                var tempPath = userInfo.familyCulturePath.replace(/\\/ig, '/');
                var fileId = tempPath.substring(tempPath.lastIndexOf('/'), tempPath.lastIndexOf('.'));
                formData.append('fileId', fileId);

            }

            _vm_.$indicator.open('保存中...');
            $.ajax({
                url: $Api.uploadFile,
                type: 'POST',
                mimeType: "multipart/form-data",
                cache: false,         //不设置缓存
                processData: false,  // 不处理数据
                dataType: 'json',
                contentType: false,   // 不设置内容类型
                data: formData,
                success: function(res)
                {
                    _vm_.$indicator.close();
                    console.log(res);
                    var path = res.data[0];

                    me.updateFamilyCulturePath(path); //更新自己的家族图路径

                },
                fail: function()
                {
                    _vm_.$indicator.close();
                }
            });


        },

        //查看家谱表格
        event2ViewFamilyTable: function()
        {
            //oui.router.push('/family_culture/user/familyTable.vue.html');
            oui.router.push('/family_culture/user/familyTable.html');
            $('#routerView').show();
        },

        //更新自己的家族图路径
        updateFamilyCulturePath: function(path)
        {
            var me = this;
            var userInfo = me.userInfo;
            debugger;
            var nodeJSON = me.treeMap;

            var jf = 0;
            var nodesLength = nodeJSON.ids.length;
            var leafNodesLength = 0;
            var leafNode = 0;
            var nodeJf = 0;

            //遍历节点，统计节点信息
            for(var i = 0; i < nodeJSON.ids.length; i++)
            {
                if(me.treeMap.hasChildren(nodeJSON.ids[i]))
                {
                    leafNode = me.treeMap.map[nodeJSON.ids[i]].childIds.length;
                    leafNodesLength += leafNode;
                }

                nodeJf = me.treeMap.map[nodeJSON.ids[i]].node.jf ? me.treeMap.map[nodeJSON.ids[i]].node.jf : 1;
                jf += nodeJf;
            }

            console.log(jf + " " + nodesLength + " " + leafNodesLength);

            _vm_.$indicator.open('更新中...');
            //更新节点信息
            $.ajax({
                url: $Api.updateFamilyCulturePath,
                data: {
                    userId: userInfo.userId,
                    tokenId: userInfo.tokenId,
                    targetUserId: userInfo.userId,
                    imagePath: path,
                    jf: jf, //积分
                    nodesLength: nodesLength,    //节点数
                    leafNodesLength: leafNodesLength,    //叶子节点数
                },
                success: function(o)
                {
                    me.saved = true;
                    _vm_.$indicator.close();
                    showTipInfo('保存成功');

                    userInfo.familyCulturePath = path;
                    oui.storage.set('userInfo', oui.parseString(userInfo));
                },
                fail: function()
                {
                    _vm_.$indicator.close();
                }
            });

        },

        /**
         * 获取独立的vue模板
         * @param treeNode
         * @returns {*}
         */
        findNodeHtml4vue: function(treeNode)
        {
            var me = this;
            var page = treeNode.node.page;
            var tempPage = oui.parseJson(oui.parseString(page));

            var content = tempPage.content;
            tempPage.content = '';
            tempPage.listContent = '';

            if(!me._tpl4vuehtml)
            {
                var tplText = oui.loadUrl(oui.getContextPath() + 'res_engine/page_design/pc/code-page-runtime-edit.art.html', 2);
                me._tpl4vuehtml = template.compile(tplText);
            }
            var html = me._tpl4vuehtml({
                contextPath: oui.getContextPath(),
                name: treeNode.node.name,
                enName: treeNode.node.enName || treeNode.id,
                content: content,
                pageJson: oui.parseString(tempPage)
            });
            return html;
        },
        //获取节点html代码
        findNodeHtml: function(treeNode)
        {
            var me = this;
            var page = treeNode.node.page;
            var tempPage4content = oui.parseJson(oui.parseString(page));
            tempPage4content.listContent = '';
            var content = tempPage4content.content || '';
            if(!me._tpl4html)
            {
                var tplText = oui.loadUrl(oui.getContextPath() + 'res_engine/page_design/pc/code-page-runtime.html', 2);
                me._tpl4html = template.compile(tplText);
            }
            var html = me._tpl4html({
                contextPath: oui.getContextPath(),
                name: treeNode.node.name,
                enName: treeNode.node.enName || treeNode.id,
                content: content,
                pageJson: oui.parseString(tempPage4content)
            });
            return html;
        },
        /***
         * 导出所有节点源代码
         * @param nodeId
         */
        appendNode2zip: function(nodeId, zip)
        {
            var me = this;
            var treeMap = me.treeMap;
            var nodes = treeMap.findChildren(nodeId) || [];
            oui.eachArray(nodes, function(item)
            {

                //创建目录
                var pids = treeMap.findParentIdsAll(item.id) || [];
                pids = pids.reverse();//反序
                var path = [];
                oui.eachArray(pids, function(pid)
                {
                    //var currNode = treeMap.findNode(pid);
                    path.push(treeMap.findNodeEnName(pid));
                });
                //创建源代码路径资源
                path.push(treeMap.findNodeEnName(item.id) + '.vue.html');
                var html4vue = me.findNodeHtml4vue(item); //根据页面设计获取页面vueHtml
                var html = me.findNodeHtml(item);
                var vuePath = path.join('/');
                var htmlPath = vuePath.replace('.vue.html', '.html');

                //构造文件
                zip.addFile(vuePath, html4vue); //添加 模板文件
                zip.addFile(htmlPath, html); //添加 模板文件


                me.appendNode2zip(item.id, zip);
            });
        },
        /**导出整个系统 */
        event2exportSys: function(cfg)
        {
            var me = this;
            // me.treeMap;
            console.log(me.treeMap);
            //根据树结构创建目录 和设计包
            //根据树结构创建目录，创建页面

            oui.require([
                oui.getContextPath() + "res_engine/tools/ouiAce/ace/ace.js",
                oui.getContextPath() + "res_engine/tools/ouiAce/ace/ext-language_tools.js",
                oui.getContextPath() + "res_engine/tools/ouiAce/ace/ext-statusbar.js",
                oui.getContextPath() + "res_engine/tools/ouiAce/ace/ext-static_highlight.js",
                oui.getContextPath() + "res_engine/tools/ouiAce/zip/zip.js",
                oui.getContextPath() + "res_engine/tools/ouiAce/zip/mime-types.js",
                oui.getContextPath() + "res_engine/tools/ouiAce/js/zip-tool.js",
                oui.getContextPath() + "res_engine/tools/ouiAce/js/beautify.js",
                oui.getContextPath() + "res_engine/tools/ouiAce/js/HTML-Beautify.js"
            ], function()
            {

                var z = new ZipArchive();
                me.appendNode2zip('root', z);
                var name = me.treeMap.findNodeName('root');
                oui.getTop().oui.showInputDialog('导出项目-输入项目名称', function(v)
                {
                    if(!v)
                    {
                        oui.getTop().oui.alert('项目名称不能为空');
                        return;
                    }
                    z.export(v);
                }, [{type: "text", value: name}]);

            });
        },
        saveDesign: function(success)
        {
            var me = this;

            var saveDesignApi = me.data.saveDesignUrl;
            var param = {
                menuId: me.data.menu.id,
                treeMap: me.treeMap
            };
            if(saveDesignApi)
            {
                oui.postData(saveDesignApi, param, function(res)
                {
                    if(res.success)
                    {
                        success && success();
                    } else
                    {
                        oui.getTop().oui.alert(res.msg);
                    }
                }, function(err)
                {
                    oui.getTop().oui.alert(err);
                }, '保存中...');

            } else
            {
                //本地存储
                //console.log('本地存储');
                //oui.db.sys_menu.saveOrUpdate(me.treeMap, function()
                //{
                //    console.log('保存成功');
                //    success && success();
                //}, function()
                //{
                //    console.log('保存失败');
                //});
            }
        },

        //
        hasSaveData: function()
        {
            return this.saved
        },

        /****
         * 保存 项目设计
         * @param cfg
         */
        event2saveDesign: function(cfg)
        {
            var me = this;
            me.saveDesign(function()
            {
                var d = oui.getTop().oui.alert('保存成功');
                setTimeout(function()
                {
                    d.hide();
                }, 800);
            });
        },

        load: function(param, callback)
        {
            var me = this;
            var path = oui.biz.Tool.getApiPathByController(me.FullName.replace('4Design', ''), 'loadDesign');
            var id = oui.getParam('id');
            path = me.urlParams.loadmenuDesignPath || path;
            oui.getData(path, param, function(res)
            {
                if(res.success)
                {
                    var menu = res.menu || {};
                    me.data.menu = menu;
                    if(res.designJson)
                    {
                        me.treeMap = me.newTreeMap(oui.parseJson(res.designJson), 'id', 'parentId', 'name');
                    } else
                    {
                        me.treeMap = me.array2orgTreeMap([{
                            id: menu.id,
                            name: menu.name,
                            parentId: null,
                            nodeType: me.nodeTypeEnum.root.name
                        }], 'id', 'parentId', 'name');
                    }
                    me.data.saveDesignUrl = res.saveDesignUrl;
                    me.data.treeMap = me.treeMap || {};
                    me.data.apiMap = res.apiMap || {};
                    me.data.loadLogicUrl = res.loadLogicUrl;
                    me.data.loadInteractionDesignUrl = res.loadInteractionDesignUrl;
                    me.data.loadPageDesignUrl = res.loadPageDesignUrl;
                    me.data.savePageDesignUrl = res.savePageDesignUrl;
                    callback && callback();
                } else
                {
                    oui.getTop().oui.alert(res.msg);
                }
            }, '加载中...');
        }
    };
    FamilyTree = oui.biz.Tool.crateOrUpdateClass(FamilyTree);
})(window, window.$$ || window.$);




