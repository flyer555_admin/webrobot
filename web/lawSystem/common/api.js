var _baseUrl_ = "https://www.startwe.net";

var $Api = {
    //获取短信验证码
    phoneMsgCode: _baseUrl_ + "/com.oursui.models.lbb.web.AccountController.getCellphoneMsgCode.biz",
    //注册
    register: _baseUrl_ + "/com.oursui.models.lbb.web.AccountController.register.biz",
    //登录
    login: _baseUrl_ + "/com.oursui.models.lbb.web.AccountController.login.biz",
    //获取登录信息
    getUser: _baseUrl_ + "/com.oursui.models.lbb.web.AccountController.getUser.biz",
    //更新密码
    updatePass: _baseUrl_ + "/com.oursui.models.lbb.web.AccountController.updatePass.biz",
    //找回密码
    findPass: _baseUrl_ + "/com.oursui.models.lbb.web.AccountController.findPass.biz",
    //根据支付回调的内容查看支付图片二维码接口
    alipayQrCode: _baseUrl_ + "/com.oursui.models.qrcode.web.QrcodeController.png.biz",
    //支付订单处理
    payOrder: _baseUrl_ + "/com.oursui.models.lbb.web.AccountController.pay.biz",
    //查询订单支付状态
    queryOrderState: _baseUrl_ + "/com.oursui.models.lbb.web.AccountController.queryOrderState.biz",

    //******* 后台管理 *******
    //保存消息
    saveCfgMsg: _baseUrl_ + "/com.oursui.models.lbb.web.ConfigController.saveCfgMsg.biz",
    //保存产品列表
    saveProducts: _baseUrl_ + "/com.oursui.models.lbb.web.ConfigController.saveProducts.biz",
    //查询用户列表
    queryUsers: _baseUrl_ + "/com.oursui.models.lbb.web.AccountMgrController.queryUsers.biz",
    //管理员添加用户
    addUser: _baseUrl_ + "/com.oursui.models.lbb.web.AccountMgrController.addUser.biz",
    //追加购买时长
    addTime: _baseUrl_ + "/com.oursui.models.lbb.web.AccountMgrController.addTime.biz",
    //赠送时长
    freeTime: _baseUrl_ + "/com.oursui.models.lbb.web.AccountMgrController.freeTime.biz",
    //修改用户角色
    editUserRole: _baseUrl_ + "/com.oursui.models.lbb.web.AccountMgrController.updatelimit.biz",
    //历史订单查询
    queryHistoryOrders: _baseUrl_ + "/com.oursui.models.lbb.web.OrderController.queryHistoryOrders.biz",
    //官网用户名密码批量导入
    importWebsiteUsers: _baseUrl_ + "/com.oursui.models.lbb.web.WebsiteUserController.importWebsiteUsers.biz",
    //官网用户名密码维护批量删除
    clearWebsiteUsers: _baseUrl_ + "/com.oursui.models.lbb.web.WebsiteUserController.clearWebsiteUsers.biz",
    //查询官网用户名和密码
    queryWebsiteUsers: _baseUrl_ + "/com.oursui.models.lbb.web.WebsiteUserController.queryWebsiteUsers.biz",
    //根据当前登录人 获取某个官网的可用用户名和密码
    queryWebsiteOne: _baseUrl_ + "/com.oursui.models.lbb.web.WebsiteUser4VipController.queryOne.biz"
};

//获取用户信息
oui.getUserInfo = function(callback)
{
    oui.storageApi({
        key: 'userInfo'
    }, function(userInfo)
    {
        userInfo = oui.parseJson(userInfo);

        if(userInfo.userId)
        {
            oui.ajax({
                url: $Api.getUser,
                postParam: {
                    userId: userInfo.userId,
                    cellphone: userInfo.cellphone
                },
                callback: function(res)
                {
                    if(res.success && res.data && res.data.user)
                    {
                        res.data.user.urls = {
                            getUser: $Api.getUser,
                            queryWebsiteOne: $Api.queryWebsiteOne
                        };

                        oui.storageApi({
                            key:'userInfo',
                            value: JSON.stringify(res.data.user)
                        }, function()
                        {
                            window.top.postMessage({
                                    cmd: 'cmd4updateUserInfo',
                                    param: res.data.user
                                }
                                , "*");

                            callback && callback(res.data.user);
                            oui.getUserInfoTimer();//开始定时任务
                        });


                    } else
                    {
                        callback && callback(null);
                    }
                }
            });
        } else
        {
            callback && callback(null);
        }

    });

};


oui._timer4getUser = null;
oui.clearTimer4getUser = function()
{
    if(oui._timer4getUser)
    {
        try
        {
            clearInterval(oui._timer4getUser);
        } catch(err)
        {
        }
    }
};

oui.getUserInfoTimer = function()
{
    oui.clearTimer4getUser();
    oui._timer4getUser = setInterval(function()
    {
        oui.getUserInfo();
    }, 30 * 60 * 1000);//30分钟刷新一次
};

//清除用户信息
oui.clearUserInfo = function(callback)
{
    oui.clearTimer4getUser();
    oui.storageApi({
        remove:true,
        key:'userInfo'
    }, function()
    {
        window.top.postMessage({
                cmd: 'cmd4updateUserInfo',
                param: {}
            }
            , "*");
    });
    setTimeout(function(){
        callback&&callback();
    },100);
};

/***
 * 获取插件url参数
 * @returns {{isIframeInclude: string, isChromeExt: string}}
 */
oui.getPluginUrlParam = function()
{
    var isIframeInclude = oui.getParam("isIframeInclude") || '';
    if(isIframeInclude == 'true')
    {
        isIframeInclude = true;
    } else
    {
        isIframeInclude = false;
    }
    var isChromeExt = oui.getParam('isChromeExt') || '';
    if(isChromeExt == 'true')
    {
        isChromeExt = true;
    } else
    {
        isChromeExt = false;
    }

    var urlParam = {
        isIframeInclude: isIframeInclude,
        isChromeExt: isChromeExt
    };
    return urlParam;
};
oui.getPluginUrlParamString = function()
{
    return $.param(oui.getPluginUrlParam());
};
oui.storageApi = function(param, callback)
{

    var urlParam = oui.getPluginUrlParam();
    if(urlParam.isChromeExt)
    {
        oui.storageByTopChromePlugin(param, callback);
    } else
    {
        var result = param;
        if(param.key && (typeof param.value != 'undefined'))
        { //set
            oui.storage.set(param.key, param.value);
        } else if(param.key)
        {//remove or get
            if(param.remove)
            {
                oui.storage.remove(param.key);
            } else
            {
                var v = oui.storage.get(param.key);
                result = v;
            }
        } else if(param.clear)
        {
            oui.storage.clear();
        }
        callback && callback(result);
    }
}