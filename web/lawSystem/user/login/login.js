var _vm_ = new Vue();
//登录
$('#log-btn').click(function()
{
    var cellphone = $.trim($('input[name=cellphone]').val());
    var password = $.trim($('input[name=password]').val());

    if(cellphone == '' || !/^[1][3,4,5,7,8,9][0-9]{9}$/.test(cellphone))
    {
        showTipInfo('请输入正确的手机号');
        return;
    } else if(password == '')
    {
        showTipInfo('请输入密码');
        return;
    }

    $('.log-btn').eq(1).addClass('active').siblings().removeClass('active');

    // isIframInclude=true&isChromeExt=true


    var urlParam = oui.getPluginUrlParam();
    var strUrlParam = $.param(urlParam);

    if(urlParam.isIframeInclude && urlParam.isChromeExt)
    {
        oui.loadUrl = oui.loadUrlByTopChromePlugin;
    }

    oui.ajax({
        url: $Api.login,
        postParam: {
            cellphone: cellphone,
            pwd: password
        },//post提交场景，post提交对象参数
        callback: function(res)
        {
            $('.log-btn').eq(0).addClass('active').siblings().removeClass('active');

            //console.log("fail: ", fail);

            if(res.success)
            {
                oui.storageApi({
                    key:'userInfo',
                    value: JSON.stringify(res.data.user)
                }, function()
                {
                    oui.getUserInfo(function(userInfo){
                        //重置
                        $('input[name=cellphone]').val('');
                        $('input[name=password]').val('');
                        //personRole 1普通用户，2管理员，3推广员，4超级管理员
                        var personRole = userInfo.personRole;
                        if([1, 3].includes(personRole))
                        {
                            location.href = '/lawSystem/user/index.html?' + strUrlParam;
                        } else if([2, 4].includes(personRole))
                        {
                            location.href = '/lawSystem/admin/index.html?' + strUrlParam;
                        }
                    });

                });

            } else
            {
                showTipInfo(res.msg);
            }
        }
    });


    /*  $.ajax({
          url: $Api.login,
          type: "GET",
          timeout: 10000,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: {
              cellphone: cellphone,
              pwd: password,
              /!*cellphone: '13808071246', //超级管理员
              cellphone: '13808078013',   //普通用户
              pwd: '123456'*!/
          },
          success: function(res)
          {
              if(res.success)
              {
                  localStorage.setItem('userInfo', JSON.stringify(res.data.user));

                  //重置
                  $('input[name=cellphone]').val('');
                  $('input[name=password]').val('');


                  //personRole 1普通用户，2管理员，3推广员，4超级管理员
                  var personRole = res.data.user.personRole;
                  if([1, 3].includes(personRole))
                  {
                      location.href = '/lawSystem/user/index.html';
                  } else if([2, 4].includes(personRole))
                  {
                      location.href = '/lawSystem/admin/index.html';
                  }
              } else
              {
                  showTipInfo(res.msg);
              }

          },
          complete: function()
          {
              $('.log-btn').eq(0).addClass('active').siblings().removeClass('active');
          }
      });*/

});

enterKeyDown();

function enterKeyDown()
{
    document.onkeydown = function(ev)
    {
        var event = ev || event;
        if(event.keyCode == 13)
        {
            $('#log-btn').trigger('click');
        }
    }
}

//忘记密码
$('#find-pwd-btn').click(function()
{
    location.href = '/lawSystem/user/findPwd.html';
});

//注册
$('#reg-btn').click(function()
{
    location.href = '/lawSystem/user/register.html';
});

function showTipInfo(text)
{
    _vm_.$message.closeAll();
    _vm_.$message({
        type: 'info',
        message: text,
        duration: 1500
    });
}