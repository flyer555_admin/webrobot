;(function (win)
{
    function SelectDialog(options)
    {
        options = options || {};
        this.options = {
            title: options.title || '请选择',
            area: options.area || '700px',
            content: options.content || '内容...',
            treeConfig: options.treeConfig || {},
            confirm: options.confirm
        };

        this.init();

    };

    SelectDialog.prototype.init = function ()
    {
        var that = this;

        //左侧tab切换
        $('.dialog-tabs .tab-item').click(function ()
        {
            var index = $(this).index();
            $(this).addClass('active').siblings().removeClass('active');
            var $content = that.options.content;
            $content.find('.dialog-right-inner').eq(index).addClass('active').siblings().removeClass('active');
        });

        this.renderTree();
    };

    //生成树结构
    SelectDialog.prototype.renderTree = function ()
    {
        var that = this;
        var treeConfig = this.options.treeConfig;
        for (var key in treeConfig)
        {
            (function (key)
            {
                var treeId = treeConfig[key].treeId;
                var treeData = treeConfig[key].data;
                treeConfig[key].checkedNodes = [];
                console.warn(treeConfig);

                var setting = {
                    check: {
                        //enable: true
                    },
                    view: {
                        //showIcon: false,
                        showLine: false,
                        showTitle: false
                    },
                    callback: {
                        onClick: function (e, treeId, treeNode)
                        {
                            var treeObj = $.fn.zTree.getZTreeObj(treeId);
                            var nodes = [treeNode];
                            console.warn(nodes);

                            if (nodes[0].children && nodes[0].children.length)
                            {
                                treeObj.expandNode(nodes[0], true, true, true);
                            } else
                            {
                                treeObj.checkNode(nodes[0], true, true);
                                that.filterCheckedNodes(key, nodes);
                                that.renderCheckedNode(key, treeConfig[key].checkedNodes);

                               /* treeObj.setting.view.fontCss["color"] = '#1E9FFF';
                                nodes = treeObj.transformToArray(nodes);
                                treeObj.updateNode(nodes[0]);*/
                            }


                        },
                        /*onCheck: function (e, treeId, treeNode)
                        {
                            var treeObj = $.fn.zTree.getZTreeObj(treeId);
                            //console.warn(treeId, treeNode);
                            var nodes = that.getNoLeafNodes([treeNode], []);    //获取节点下所有无子节点的子节点
                            //console.warn(nodes);

                            if (!treeNode.checked)
                            {//取消选择
                                that.filterCheckedNodesRemove(key, nodes);
                            } else
                            {
                                treeObj.expandNode(treeNode, true, true, true);
                                that.filterCheckedNodes(key, nodes);
                            }

                            that.renderCheckedNode(key, treeConfig[key].checkedNodes);

                        }*/
                    }
                };
                var zNodes = treeData;

                $.fn.zTree.init($("#" + treeId), setting, zNodes);

            })(key);
        }

        new  ZtreeSearch();
    };

    //遍历获取无子节点的节点
    SelectDialog.prototype.getNoLeafNodes = function (data, tempArr)
    {
        for (var i = 0; i < data.length; i++)
        {
            if(!data[i].children)
            {
                tempArr.push(data[i]);
            } else
            {
                this.getNoLeafNodes(data[i].children, tempArr);
            }
        }

        return tempArr;
    };

    //过滤选中节点，不存在就添加
    SelectDialog.prototype.filterCheckedNodes = function (key, data)
    {
        var treeConfig = this.options.treeConfig;
        var checkedNodes = treeConfig[key].checkedNodes;

        var isMatch = false;
        for (var i = 0; i < data.length; i++)
        {
            isMatch = false;

            for (var j = 0; j < checkedNodes.length; j++)
            {
                if (checkedNodes[j].id == data[i].id)
                {
                    isMatch = true;
                }
            }

            !isMatch && checkedNodes.push(data[i]);
        }

    };

    //移除选中节点，匹配就移除
    SelectDialog.prototype.filterCheckedNodesRemove = function (key, data)
    {
        var treeConfig = this.options.treeConfig;
        var checkedNodes = treeConfig[key].checkedNodes;

        for (var i = 0; i < data.length; i++)
        {
            for (var j = 0; j < checkedNodes.length; j++)
            {
                if (checkedNodes[j].id == data[i].id)
                {
                    checkedNodes.splice(j, 1);
                    break;
                }
            }
        }
    };

    //渲染选中的树节点
    SelectDialog.prototype.renderCheckedNode = function (key, data)
    {
        var that = this;
        var html = '';
        for (var i = 0; i < data.length; i++)
        {
            html += '<div class="checked-item">' +
                '<span>' + data[i].name + '</span>' +
                '<i class="item-close" data-id="' + data[i].id + '" data-index="' + i + '"></i>' +
                '</div>';
        }

        var treeConfig = this.options.treeConfig;
        var $checkedBox = $('#' + treeConfig[key].checkedBoxId);
        $checkedBox.html(html);

        var checkedNodes = treeConfig[key].checkedNodes;
        $checkedBox.find('.item-close').on('click', function ()
        {
            var id = $(this).data('id');
            var index = $(this).data('index');
            var treeObj = $.fn.zTree.getZTreeObj(treeConfig[key].treeId);
            var node = treeObj.getNodeByParam("id", id);
            treeObj.checkNode(node, false, true);
            checkedNodes.splice(index, 1);
            console.warn(index, checkedNodes);
            that.renderCheckedNode(key, checkedNodes);

           /* treeObj.setting.view.fontCss["color"] = '#333';
            treeObj.updateNode(node);*/

        });
    };

    //弹窗显示
    SelectDialog.prototype.show = function ()
    {
        var that = this;
        this.dialogIndex = layer.open({
            type: 1,
            title: this.options.title,
            area: this.options.area, //宽高
            move: false,
            btn: ['确定', '取消'],
            content: this.options.content,
            yes: function (index)
            {
                layer.close(index);
                if (that.options.confirm && typeof that.options.confirm == 'function')
                {
                    that.options.confirm.apply(that, []);
                }
            }
        });
    };

    //弹窗隐藏
    SelectDialog.prototype.hide = function ()
    {
        layer.close(this.dialogIndex);
    };

    //获取选中的结果
    SelectDialog.prototype.getResult = function ()
    {
        var treeConfig = this.options.treeConfig;
        var result = {};

        for (var key in treeConfig)
        {
            var treeObj = $.fn.zTree.getZTreeObj(treeConfig[key].treeId);
            var nodes = treeObj.getCheckedNodes(true);
            result[key] = nodes;
        }

        return result;
    };

    oui.selectDialog = SelectDialog;

})(window);

