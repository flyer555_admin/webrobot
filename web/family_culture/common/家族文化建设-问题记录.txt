功能修改说明：
1.在双亲树中，点击“祭祀”，判断当前节点的祭祀Id是否存在，如果没有，则调用后台接口 新增祭祀对象;然后跳转祭祀页面
	
 接口编号 33.com.oursui.models.fc.web.Sacrificial4VipController.saveOrUpdateSacrificial.biz
oui.ajax({
  url:'http://127.0.0.1:8083/com.oursui.models.fc.web.Sacrificial4VipController.saveOrUpdateSacrificial.biz',
  postParam:{ 
	id:'365922710980657152',//新增时为空，更新时必须传入
    userId:'365509614663368704',tokenId:'a0cacd9f7abf430594366c6eeb1361d3',
    nodeId:'node2',//祭祀对象在双亲图中的节点id
	name:'张**22',//祭祀对象姓名
	treePath:'***/111.json',//双亲图json路径
	imagePath:'xxx/***.png',//头像路径
	birthDate: '1987-11-04 23:40:00' ,//生辰（出生时间） 年-月-日 时:分:秒
	dateOfDeath: '2087-11-04 23:40:00' ,//忌日（去世时间） 年-月-日 时:分:秒
	age:99,//祭祀对象年龄,根据生辰和忌日计算
	sex:1//值为数字[显示为选择项]，性别 1,男 2,女
  },
  callback:function(o){console.log(o)} 
})


2.祭祀访问页面
  当祭祀对象没有 头像图片时，则将名字显示为竖着的在照片 占位的位置；
   
  需要调用祭祀对象接口进行回填
  为已故亲人创建祭祀活动，通过 双亲树节点菜单 【纪念亲人】操作，逻辑：先判断当前节点对象上 有没有祭祀id(sacrificial),如果没有则先执行 保存祭祀对象接口
	接口编号：34.com.oursui.models.fc.web.SacrificialController.getSacrificial.biz 查询祭祀对象
	//根据祭祀id查询
	http://127.0.0.1:8083/com.oursui.models.fc.web.SacrificialController.getSacrificial.biz?sacrificialId=365922710980657152
   
	
3.编辑信息包括
	name,//姓名
	treePath,//双亲树json路径
	avantar,//头像
	birthDate,//生辰 阴历生辰
	dateOrDeath, //忌日 阴历时间
	personState,//当前人状态【值为数字，显示为下拉选择】, 1.活着 2.去世 3.夭折 4.走失
	sex,//性别【值为数字,显示为下拉】,1.男 2.女
	age,//节点年龄：(如果当前人状态判断，如果为 1.活着 或者 4.走失 则为 当前时间减去生辰计算；如果为 2.去世 或者 3.夭折 则为 忌日时间减去生辰计算 ) 【根据当前时间，生辰，忌日 的计算】
	cd,//朝代，【古代记时】如：乾隆三年，
	lssj,//历史事件，对当时历史事件进行描述	
	bz, //备注，对人物进行介绍，年代，历史事件，父母，子女信息,对事业发展，对家族贡献，社会贡献进行描述等
	jdjf, //节点积分，每个节点编辑时计算和合计积分值;
		【编辑信息中的属性,name非默认值，非空】
		【备注内容，100到200字】
		积分规则【name:1分,anantar:2分,birthDate:1分,dateOfDeath:1分,personState:2分,sex:1分,cd:2分,lssj:3分,bz:3分】
	
4.保存双亲树时，合计所有节点的积分，存放到treeMap的根节点的node中的属性上 node.hjjf ;//合计积分

5.双亲树中节点显示
	当节点没有图片时，将节点的名字 竖着显示在图片的占位处【下方横着显示姓名隐藏】
    当节点有图片时，图片占位处显示头像【下方横着显示姓名】
6.登录注册页面 需要适配pc端
7.在这个中心中显示 双亲树菜单,菜单名“我的家谱”
8.双亲树页面，头部的图片去掉，需要有复古素材作为页面背景、页面边框
9.祭祀支付页面,移动端 1）需要生成一张另存为图片的的二维码 2）需要提供一个链接(提示 用手机自带浏览器打开,如果不支持访问，请下载二维码图片，通过识别二维码进行支付)，点击跳转到支付宝支付路径;
10.祭祀页面，支付成功后,刷新页面，或者过一天后进入逻辑
   1)先根据祭祀Id 获取祭祀对象，显示祭祀信息
   2)回调后，根据 查询订单接口 35.com.oursui.models.fc.web.SacrificialController.queryOrdersBySacrificial.biz根据祭祀对象查询订单列表
	oui.ajax({
	  url:'http://127.0.0.1:8083/com.oursui.models.fc.web.SacrificialController.queryOrdersBySacrificial.biz',
	  postParam:{ 
		sacrificialId:'365922710980657152',//祭祀id
		pageParam:{},//分页对象
		cellphone:''//根据支付者电话号码
	  },
	  callback:function(o){console.log(o)} 
	})
   3）如果已经支付的时间超过3天，则不回填已经购买的产品信息到祭祀详情 ；如果支付成功时间没有超过3天，则回填支付购买产品信息到祭祀详情；
11.家谱宣传页面设计【待设计的页面】
	1).追根寻祖，"我为我的家族代言"
	2).24孝文化介绍
	3).家族文化传承
	4).拉近亲人关系
	5).长辈生日备忘录
	6).已故亲人纪念
	7).同辈亲戚常联系
	8).祖先祭奠不用愁
    9).维护祖先长青树，沉淀家族贡献荣誉；
	10).颁发家族文化传承证书
	11).永久有效的24孝积分政策，积分对换礼品：酒、鸡蛋、蛋糕、蔬菜、水果、猪肉、衣服等	
12.家族文化排行榜
	1).根据姓氏查询家族贡献积分,显示为家族排行列表
	2).就是显示 姓氏、节点数、分支数、积分
13.管理员查看用户列表中，需要提供家族图查看功能(能进行维护并保存)	
	
14. 接口【29，31】【管理员更新家谱树，和普通用户更新自己的家谱树 都要这样处理】 更新家谱路径的接口(在双亲树上传后，都需要调用该接口，新增，更新都要调用该接口)
	提交参数中扩展
	jf,//积分
	nodesLength,//节点数
	leafNodesLength //叶子节点数(分支数)
15.提供一个管理员pc端访问的登录入口

16.首页默认显示为用户家族树积分排行榜



