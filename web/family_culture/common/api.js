var _baseUrl_ = "http://startwe.net";

var $Api = {
    //获取短信验证码
    phoneMsgCode: _baseUrl_ + "/com.oursui.models.fc.web.AccountController.getCellphoneMsgCode.biz",
    //注册
    register: _baseUrl_ + "/com.oursui.models.fc.web.AccountController.register.biz",
    //登录
    login: _baseUrl_ + "/com.oursui.models.fc.web.AccountController.login.biz",
    //获取登录信息
    getUser: _baseUrl_ +"/com.oursui.models.fc.web.AccountController.getUser.biz",
    //更新密码
    updatePass: _baseUrl_ + "/com.oursui.models.fc.web.AccountController.updatePass.biz",
    //找回密码
    findPass: _baseUrl_ + "/com.oursui.models.fc.web.AccountController.findPass.biz",
    //根据支付回调的内容查看支付图片二维码接口
    alipayQrCode: _baseUrl_ + "/com.oursui.models.qrcode.web.QrcodeController.png.biz",
    //支付订单处理
    payOrder: _baseUrl_ + "/com.oursui.models.fc.web.AccountController.pay.biz",
    //查询订单支付状态
    queryOrderState: _baseUrl_ + "/com.oursui.models.fc.web.AccountController.queryOrderState.biz",

    //*** 祭祀详情 ***
    //多产品支付
    payList: _baseUrl_ + "/com.oursui.models.fc.web.AccountController.payList.biz",
    //上传图片
    uploadFile: _baseUrl_ + "/com.oursui.models.file.web.FileController.upload.biz",
    //查看图片
    viewImage: _baseUrl_ + "/com.oursui.models.file.web.FileController.image.biz",
    //用户自己更新自己的家族图路径
    updateFamilyCulturePath: _baseUrl_ + "/com.oursui.models.fc.web.AccountController.updateFamilyCulturePath.biz",
    //更新自己的家族图路径
    updateFamilyCulturePath: _baseUrl_ + "/com.oursui.models.fc.web.AccountController.updateFamilyCulturePath.biz",
    //查看文件内容
    readFile: _baseUrl_ + "/com.oursui.models.file.web.FileController.readFile.biz",
    //用户体验反馈
    feedback: _baseUrl_ + "/com.oursui.models.fc.web.AccountController.question.biz",
    queryUserJf: _baseUrl_ + "/com.oursui.models.fc.web.AccountController.queryUserJf.biz",

    //******* 后台管理 *******
    //保存消息
    saveCfgMsg: _baseUrl_ + "/com.oursui.models.fc.web.ConfigController.saveCfgMsg.biz",
    //保存产品列表
    saveProducts: _baseUrl_ + "/com.oursui.models.fc.web.ConfigController.saveProducts.biz",
    //查询用户列表
    queryUsers: _baseUrl_ + "/com.oursui.models.fc.web.AccountMgrController.queryUsers.biz",
    //管理员添加用户
    addUser: _baseUrl_ + "/com.oursui.models.fc.web.AccountMgrController.addUser.biz",
    //追加购买时长
    addTime: _baseUrl_ + "/com.oursui.models.fc.web.AccountMgrController.addTime.biz",
    //赠送时长
    freeTime: _baseUrl_ + "/com.oursui.models.fc.web.AccountMgrController.freeTime.biz",
    //修改用户角色
    editUserRole:_baseUrl_ + "/com.oursui.models.fc.web.AccountMgrController.updatelimit.biz",
    //历史订单查询
    queryHistoryOrders: _baseUrl_ + "/com.oursui.models.fc.web.OrderController.queryHistoryOrders.biz",
    //官网用户名密码批量导入
    importWebsiteUsers: _baseUrl_ + "/com.oursui.models.fc.web.WebsiteUserController.importWebsiteUsers.biz",
    //官网用户名密码维护批量删除
    clearWebsiteUsers: _baseUrl_ + "/com.oursui.models.fc.web.WebsiteUserController.clearWebsiteUsers.biz",
    //查询官网用户名和密码
    queryWebsiteUsers: _baseUrl_ + "/com.oursui.models.fc.web.WebsiteUserController.queryWebsiteUsers.biz",
    //根据当前登录人 获取某个官网的可用用户名和密码
    queryWebsiteOne: _baseUrl_ + "/com.oursui.models.fc.web.WebsiteUser4VipController.queryOne.biz"
};
oui = window['oui']||{};
if(!oui.storage){
    oui.storage = {
        get: function (key) {
            return window.localStorage.getItem(key);
        },
        save: function (key, value) {
            window.localStorage.setItem(key, value);
        },
        set: function (key, value) {
            window.localStorage.setItem(key, value);
        },
        remove: function (key) {
            window.localStorage.removeItem(key);
        },
        clear: function () {
            window.localStorage.clear();
        }
    };

//将对象转JSON字符串
    oui.parseString = function (obj) {
        if(typeof obj =='string'){
            return obj;
        }
        return JSON.stringify(obj);
    };
//将字符串转换成JSON对象
    oui.parseJson = function (str) {

        if (typeof str == 'undefined') {
            return {};
        }
        if (!str) {
            return {};
        }
        if (typeof str == 'object') {
            return str;
        }
        /* eval 作用域处理****/
        if (str.indexOf('[') == 0) {
            return eval.call(window,str);
        }
        return eval.call(window,"(" + str + ")");
    };


    oui.setParam = function (url, paramName, paramValue,backUrl) {
        var cfg = oui.getParamByUrl(url);
        cfg[paramName] = paramValue;
        var first;
        if(url.indexOf("?")>-1){
            first = url.substring(0,url.indexOf("?"));
        }else{
            first = url;
        }
        var paramStr = $.param(cfg);
        url = first+'?'+paramStr;

        if (backUrl) {
            url = oui.setBackUrl(url, backUrl);
        }

        return url;
    };
    oui.getParamByUrl = function(url,key){
        var cfg = {};
        if(!url){
            return cfg;
        }
        if (url.indexOf("?") != -1) {
            var str = url.substr(url.indexOf("?")+1);
            var strs = str.split("&");
            for (var i = 0; i < strs.length; i++) {
                if(strs[i].split("=")[0]){
                    cfg[strs[i].split("=")[0]] = decodeURIComponent(strs[i].split("=")[1]);
                }
            }
        }
        if(key){
            return cfg[key]||"";
        }
        return cfg;
    };
}
//获取用户信息
oui.getUserInfo = function(callback){
    var userInfo = oui.parseJson(oui.storage.get("userInfo")||{});
    if(userInfo.userId){
        $.ajax({
            url: $Api.getUser,
            type: "post",
            data: {ouiData: JSON.stringify({
                userId:userInfo.userId,
                cellphone:userInfo.cellphone
            })},
            timeout: 10000000,
            cache: false,
            //contentType : 'text/json',
            dataType: "json",
            error: function(ret)
            {
                callback(null);
            },
            success: function(text)
            {
                var res = text;
                if(res.success&&res.data&&res.data.user){
                    oui.storage.set("userInfo",oui.parseString(res.data.user));
                    callback&&callback(res.data.user);
                    oui.getUserInfoTimer();//开始定时任务
                }else{
                    callback&&callback(null);
                }
            }
        });
    }else{
        callback&&callback(null);
    }
};


oui._timer4getUser = null;
oui.clearTimer4getUser=function(){
    if(oui._timer4getUser){
        try{
            clearInterval(oui._timer4getUser);
        }catch(err){}
    }
};

oui.getUserInfoTimer = function (){
    oui.clearTimer4getUser();
    oui._timer4getUser = setInterval(function(){
        oui.getUserInfo();
    },30*60*1000);//30分钟刷新一次
};
//清除用户信息
oui.clearUserInfo = function(){
    oui.clearTimer4getUser();
    oui.storage.remove('userInfo');

};
